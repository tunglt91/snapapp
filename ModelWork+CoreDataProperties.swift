//
//  ModelWork+CoreDataProperties.swift
//  
//
//  Created by Tu Ngo on 10/26/18.
//
//

import Foundation
import CoreData


extension ModelWork {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ModelWork> {
        return NSFetchRequest<ModelWork>(entityName: "ModelWork")
    }

    @NSManaged public var date_current: String?
    @NSManaged public var date: String?
    @NSManaged public var doc_rec: [String?]
    @NSManaged public var id: Int16
    @NSManaged public var lot_num: String?
    @NSManaged public var name_of_ster: String?
    @NSManaged public var results_rec_ac: [String?]
    @NSManaged public var results_rec_eo: [String?]
    @NSManaged public var results_rec_hydro: [String?]
    @NSManaged public var results_rec_formalin: [String?]
    @NSManaged public var check_condition_ac: [String?]
    @NSManaged public var check_condition_eo: [String?]
    @NSManaged public var sterilization: SterilizationModel?

}
