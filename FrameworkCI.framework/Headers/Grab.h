//
//  Grab.h
//  SampleDOM
//
//  Created by Fumio KARASAWA on 2015/10/14.
//  Copyright (c) 2015年 Fumio KARASAWA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Grab : UIViewController

//-(void)getRGBave:(CGImageRef)image touchPoint:(CGPoint)location scale:(float)scaleX;
-(NSMutableArray*)getRGBave:(CGImageRef)image touchPoint:(CGPoint)location captureSize:(int)captureSize;

@end
