//
//  RGB2Lab.h
//  SampleDOM
//
//  Created by Fumio KARASAWA on 2015/10/14.
//  Copyright (c) 2015年 Fumio KARASAWA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RGB2Lab : UIViewController

-(NSMutableArray*)getLab:(int)sR sG:(int)sG sB:(int)sB;

@end
