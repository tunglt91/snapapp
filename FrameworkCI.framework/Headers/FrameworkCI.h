//
//  FrameworkCI.h
//  FrameworkCI
//
//  Created by Fumio KARASAWA on 2018/08/20.
//  Copyright © 2018年 experimental. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FrameworkCI.
FOUNDATION_EXPORT double FrameworkCIVersionNumber;

//! Project version string for FrameworkCI.
FOUNDATION_EXPORT const unsigned char FrameworkCIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FrameworkCI/PublicHeader.h>

#import "Acquire.h"
#import "Grab.h"
#import "MagnifierView.h"
#import "RGB2Lab.h"
