//
//  ModelDevice+CoreDataProperties.swift
//  
//
//  Created by Tu Ngo on 10/26/18.
//
//

import Foundation
import CoreData


extension ModelDevice {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ModelDevice> {
        return NSFetchRequest<ModelDevice>(entityName: "ModelDevice")
    }

    @NSManaged public var part: String?
    @NSManaged public var technical_equipment: String?
    @NSManaged public var amount: String?

}
