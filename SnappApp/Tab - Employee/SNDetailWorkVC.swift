//
//  SNDetailWorkVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import VisualEffectView

protocol SNDetailWorkDelegate: class {
    func reloadListWork()
}

class SNDetailWorkVC: UIViewController {
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var viewCopy: UIView!
    @IBOutlet weak var btnCloseUp: UIButton!
    @IBOutlet weak var collectionDocument: UICollectionView!
    @IBOutlet weak var collectionTestResult: UICollectionView!
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var viewContent: UIView!
    
    var arrayImgDocument:UIImage = UIImage()
    var dataModel:ModelWork = ModelWork()
    
    // arrayData Image
    var dataImgDocument:[Any] = []
    var dataImgAC:[Any] = []
    var dataImgEO:[Any] = []
    var dataImgHydro:[Any] = []
    var dataImgFormalin:[Any] = []
    
    //arrayData Condition
    var dataConditionAC:[Any] = []
    var dataConditionEO:[Any] = []
    
    // arrayCell Nil
    var arrayDocNil:[String] = ["滅菌作業記録","滅菌器運転記録", "器材記録1", "器材記録2", "器材記録3", "器材記録4", "器材記録5"]
    var arrayACNil:[String] = ["AC \n BDテスト", "AC \n CIテスト", "AC \n BIテスト"]
    var arrayEONil:[String] = ["EO \n CIテスト", "EO \n BIテスト"]
    var arrayHydroNil:[String] = ["過酸化水素 \n CIテスト", "過酸化水素 \n BIテスト"]
    var arrayFormalinNil:[String] = ["ホルマリン \n CIテスト", "ホルマリン \n BIテスト"]
    var arrayScan:[String] = ["AC \n BDテスト", "AC \n CIテスト", "AC \n BIテスト", "EO \n CIテスト", "EO \n BIテスト", "過酸化水素 \n CIテスト", "過酸化水素 \n BIテスト", "ホルマリン \n CIテスト", "ホルマリン \n BIテスト"]
    
    var fromListJobVC:Bool = false
    var check1stLoad:Bool = true
    
    
    // Drag
    var customImgView = UIImageView()
    var startFrame = CGRect()
    var isReachedDest = Bool()
    var isPanRunning = Bool()
    var isPanAllowed = Bool()
    var indexPathForItem:IndexPath?
    var indexRemove:Int = 0
    var oldImg:String = ""
    
    var motherView:SNAddJobSuccessVC!
    var delegate:SNDetailWorkDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
        self.addGesturesForCollectionDoc()
        self.addGesturesForCollectionTest()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if check1stLoad {
            check1stLoad = false
            dataImgDocument = dataModel.value(forKey: "doc_rec") as! [Any]
            dataImgAC = dataModel.value(forKey: "results_rec_ac") as! [Any]
            dataImgEO = dataModel.value(forKey: "results_rec_eo") as! [Any]
            dataImgHydro = dataModel.value(forKey: "results_rec_hydro") as! [Any]
            dataImgFormalin = dataModel.value(forKey: "results_rec_formalin") as! [Any]
            dataConditionAC = dataModel.value(forKey: "check_condition_ac") as! [Any]
            dataConditionEO = dataModel.value(forKey: "check_condition_eo") as! [Any]
            //print((dataModel.value(forKey: "sterilization") as! SterilizationModel).curInfo.entrant)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.setupGirdView()
    }
        
    //MARK: - IBAction
    @IBAction func closeUpPress(_ sender: Any) {
        self.delegate?.reloadListWork()
        if fromListJobVC {
            //self.navigationController?.popToRootViewController(animated: true)
            self.dismiss()
        }else {
            self.motherView.dismiss()
            self.dismiss()
        }
    }
    
    //MARK: - Func
    func setupView() {
        self.btnCloseUp.layer.cornerRadius = 30
        self.btnCloseUp.layer.masksToBounds = true
        
        self.viewCopy.layer.cornerRadius = 10
        self.viewCopy.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 10, scale: true)
        
        self.collectionDocument.delegate = self
        self.collectionDocument.dataSource = self
//        self.collectionDocument.dragDelegate = self
//        self.collectionDocument.dragInteractionEnabled = true
        
        self.collectionTestResult.delegate = self
        self.collectionTestResult.dataSource = self
        
        let nib = UINib(nibName: "DetailWorkCell", bundle: nil)
        
        self.collectionDocument.register(nib, forCellWithReuseIdentifier: "DetailWorkCell")
        self.collectionTestResult.register(nib, forCellWithReuseIdentifier: "DetailWorkCell")
        
//        if fromListJobVC {
//            self.viewAlpha.backgroundColor = UIColor.white
//            self.viewAlpha.alpha = 1.0
//            self.viewContent.backgroundColor = UIColor.white
//        }
        
        self.viewAlpha.isHidden = true
        let visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        
        // Configure the view with tint color, blur radius, etc
        visualEffectView.colorTint = UIColor.white
        visualEffectView.colorTintAlpha = 0.1
        visualEffectView.blurRadius = 10
        visualEffectView.scale = 1
        
        self.view.addSubview(visualEffectView)
        self.view.addSubview(viewContent)
    }
    
    func setupGirdView() {
        let numberOfColumns: CGFloat = 6
        
        //let width: CGFloat = (self.collectionDocument.frame.width / 5) - 26
        let width: CGFloat = 100
        
        let flowDocument = collectionDocument?.collectionViewLayout as? UICollectionViewFlowLayout
        flowDocument?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowDocument?.itemSize = CGSize(width: width, height: width)
        flowDocument?.minimumLineSpacing = 22
        flowDocument?.minimumInteritemSpacing = 26
//        flowDocument?.scrollDirection = .vertical
        collectionDocument?.isPagingEnabled = false
        
        let flowTestResult = collectionTestResult?.collectionViewLayout as? UICollectionViewFlowLayout
        flowTestResult?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowTestResult?.itemSize = CGSize(width: width, height: width)
        flowTestResult?.minimumLineSpacing = 22
        flowTestResult?.minimumInteritemSpacing = 26
        flowTestResult?.scrollDirection = .vertical
        collectionTestResult?.isPagingEnabled = false
    }
    
    func showInVC(vc: UIViewController){
        vc.addChildViewController(self)
        vc.view.addSubview(self.view)
        self.didMove(toParentViewController: vc)
    }
    
    func dismiss() {
        self.willMove(toParentViewController: nil)
        self.view .removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func showCameraDocumentVC(listDataImage: [Any], indexArray: Int) {
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let cameraDocumentVC = storyboard.instantiateViewController(withIdentifier: "SNCameraDocumentVC") as? SNCameraDocumentVC
        cameraDocumentVC?.delegate = self
        cameraDocumentVC?.arrayImgDocument = listDataImage as! [String]
        cameraDocumentVC?.dataWork = dataModel
        cameraDocumentVC?.indexArray = indexArray
        self.navigationController?.pushViewController(cameraDocumentVC!, animated: true)
    }
    
    func showCameraTestResultVC(listDataImage: [Any], indexArray: Int, selectionFlag:Int) {
        /*
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let cameraTestResultVC = storyboard.instantiateViewController(withIdentifier: "SNCameraTestResultVC") as? SNCameraTestResultVC
        cameraTestResultVC?.delegate = self
        cameraTestResultVC?.arrayImg = listDataImage as! [String]
        cameraTestResultVC?.indexArray = indexArray
        cameraTestResultVC?.dataWork = dataModel
        cameraTestResultVC?.selectionFlag = selectionFlag
        self.navigationController?.pushViewController(cameraTestResultVC!, animated: true)
        */
        
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let cameraScanVC = storyboard.instantiateViewController(withIdentifier: "SNCameraScanVC") as? SNCameraScanVC
        cameraScanVC?.delegate = self
        cameraScanVC?.arrayImg = listDataImage
        cameraScanVC?.indexArray = indexArray
        cameraScanVC?.dataWork = dataModel
        cameraScanVC?.targetType = selectionFlag;
        self.navigationController?.pushViewController(cameraScanVC!, animated: true)
    }
    
    func deleteImgDocument() {
        // lưu tên ảnh vào coredata
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        oldImg = dataImgDocument[0] as! String
        
        if indexRemove == 0 {
            dataImgDocument.remove(at: indexRemove)
            dataImgDocument.insert("noImg", at: indexRemove)
        }else {
            dataImgDocument.remove(at: indexRemove)
            // nếu data.count = 0 thì phải khởi tạo lại từ đầu tránh crash ở didSelect
            if dataImgDocument.count == 1 {
                dataImgDocument = [oldImg,"","","","",""]
            }
        }
        /*
        dataImgDocument.remove(at: indexRemove)
        // nếu data.count = 0 thì phải khởi tạo lại từ đầu tránh crash ở didSelect
        if dataImgDocument.count == 0 {
            dataImgDocument = ["","","","","",""]
        }
         */
        
        dataModel.setValue(dataImgDocument, forKey: "doc_rec")
        
        do {
            try context.save()
            self.collectionDocument.reloadData()
        } catch {
            print("Failed saving")
        }
    }
    
    func deleteImgTest(index: Int) {
        // lưu tên ảnh vào coredata
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        if index == 0 {
            // indexRemove default
            dataImgAC.remove(at: indexRemove)
            dataImgAC.insert("noImg", at: indexRemove)
            /*
            // nếu data.count = 0 thì phải khởi tạo lại từ đầu tránh crash ở didSelect
            if dataImgAC.count == 0 {
                dataImgAC = ["noImg","noImg","noImg"]
            }
             */
            dataModel.setValue(dataImgAC, forKey: "results_rec_ac")
            
            dataConditionAC.remove(at: indexRemove)
            dataConditionAC.insert(0, at: indexRemove)
            dataModel.setValue(dataConditionAC, forKey: "check_condition_ac")
        }
        else if index == 1 {
            indexRemove = indexRemove - 1
            dataImgEO.remove(at: indexRemove)
            dataImgEO.insert("noImg", at: indexRemove)
            /*
             // nếu data.count = 0 thì phải khởi tạo lại từ đầu tránh crash ở didSelect
             if dataImgEO.count == 0 {
             dataImgEO = ["",""]
             }
             */
            dataModel.setValue(dataImgEO, forKey: "results_rec_eo")
            
            dataConditionEO.remove(at: indexRemove)
            dataConditionEO.insert(0, at: indexRemove)
            dataModel.setValue(dataConditionEO, forKey: "check_condition_eo")
        }
            /*
        else if index == 3 || index == 4 {
            indexRemove = indexRemove - 3
            dataImgEO.remove(at: indexRemove)
            dataImgEO.insert("noImg", at: indexRemove)
            /*
            // nếu data.count = 0 thì phải khởi tạo lại từ đầu tránh crash ở didSelect
            if dataImgEO.count == 0 {
                dataImgEO = ["",""]
            }
             */
            dataModel.setValue(dataImgEO, forKey: "results_rec_eo")
        }
         */
        
        do {
            try context.save()
            self.collectionTestResult.reloadData()
        } catch {
            print("Failed saving")
        }
    }

}
