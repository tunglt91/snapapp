//
//  SNScanVC.h
//  SnappApp
//
//  Created by Tu Ngo on 9/11/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Grab.h"

#import "Acquire.h"

//#import "AppDelegate.h"

#import "RGB2Lab.h"

//fk20180523
#import "MagnifierView.h"

//fk20180823
#import <FrameworkCI/FrameworkCI.h>
@class SNConfirmImageVC;
@class ModelWork;
@class InteractiveView;

@protocol SNScanDelegate <NSObject>

- (void)reloadDataTestResult:(NSArray *)array selectionFlag:(NSInteger)selectionFlag dataCheckCondition:(NSArray *)dataCheckCondition;

@end

@interface SNScanVC : UIViewController<UIScrollViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

{
    //AppDelegate *appDelegate;
}

@property(nonatomic, weak) id<SNScanDelegate> delegate;

@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic, strong) UIImage *shrinkedImage;

@property (nonatomic) ModelWork *dataWork;
@property (nonatomic) id arrayImg;
@property (nonatomic) NSInteger indexArray;
@property (nonatomic) NSInteger selectionFlag;
@property (nonatomic) InteractiveView *firstView;
@property (nonatomic) CGFloat widthViewScan;
@property (nonatomic) CGFloat heightViewScan;
@property (nonatomic) NSInteger typeNotification;

@property (weak, nonatomic) IBOutlet UIButton *btnJudge;
//fk20180731
- (IBAction)showImagePicker:(id)sender;
//@property (weak, nonatomic) IBOutlet UIButton *showImagePicker;
@property (weak, nonatomic) IBOutlet UIView *viewImage;
@property (weak, nonatomic) IBOutlet UILabel *lbNoti;

@property (weak, nonatomic) IBOutlet UIView *viewTool;
@property (weak, nonatomic) IBOutlet UILabel *currentScale;
@property (weak, nonatomic) IBOutlet UIStepper *scale;
- (IBAction)scale:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *scroll;
- (IBAction)viewScroll:(id)sender;

//fk20180809
@property (weak, nonatomic) IBOutlet UILabel *userCIvalue;
@property (weak, nonatomic) IBOutlet UILabel *userRGBValue;

//fk20180518
- (IBAction)typeSelect:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeSelected;
@property (weak, nonatomic) IBOutlet UILabel *rectData;

//fk20180521
@property (weak, nonatomic) IBOutlet UILabel *LabData;
- (IBAction)sizeSelect:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sizeSelected;
- (IBAction)screenSave:(id)sender;

//fk20180522
- (IBAction)searchSizeSelect:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchSizeSelected;

//fk20180523
@property (weak, nonatomic) IBOutlet UISwitch *loupe;
- (IBAction)loupeSetting:(id)sender;

//fk20180713
@property (weak, nonatomic) IBOutlet UILabel *overlap;

//fk20180717
@property (weak, nonatomic) IBOutlet UILabel *y0h0y1;

//fk20180727
@property (weak, nonatomic) IBOutlet UILabel *sRGBLightBottom;

//fk20180802
@property (weak, nonatomic) IBOutlet UILabel *backgroundColor;
//@property (weak, nonatomic) IBOutlet UISwitch *background;
- (IBAction)backgroundSetting:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *LabBottom;

//fk20180813
@property (weak, nonatomic) IBOutlet UISwitch *centerHisto;
- (IBAction)centerHistoSetting:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *histoSetting;

@end


@interface InteractiveView : UIView{
    
    //fk20180523
    NSTimer *touchTimer;
    //fk20180523
    //    MagnifierView *loop;
    MagnifierView *loupe;
    
    //AppDelegate *appDelegate;           //fk20150209
    
    UINavigationController *navi;
}

//fk20180523
@property (nonatomic, retain) NSTimer *touchTimer;
@property (nonatomic) NSInteger selectionFlag;

//fk20180523
//- (void)addLoop;
- (void)addLoupe;

- (void)handleAction:(id)timerObj;

@end
