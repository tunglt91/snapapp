//
//  SNAddJobSuccessVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import VisualEffectView

protocol SNAddJobSuccessDelegate: class {
    func reloadListWorkFromSuccessVC()
}

class SNAddJobSuccessVC: UIViewController {
    @IBOutlet weak var btnDetailOfWork: UIButton!
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbLotNumber: UILabel!
    @IBOutlet weak var lbNameOfSterilizer: UILabel!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var heightNewCell: NSLayoutConstraint!
    @IBOutlet weak var viewNewCell: UIView!
    
    var strDate = ""
    var strLotNumber = ""
    var strNameOfSterilizer = ""
    var positionYCell:CGFloat = 0
    
    var delegate:SNAddJobSuccessDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }

    //MARK: -IBAction
    @IBAction func detailOfWorkPress(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SNDetailWorkVC") as? SNDetailWorkVC
        vc?.delegate = self
        vc?.motherView = self
        vc?.dataModel = LocalData.sharedInstance.dataWork()[LocalData.sharedInstance.dataWork().count - 1]
        vc?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        vc?.showInVC(vc: self)
    }
    
    //MARK: - Func
    func setupView() {
        //self.viewNewCell.frame.origin.y = self.positionYCell
        let width: CGFloat = ((UIScreen.main.bounds.width - 343) / 5)
        let heightRow = (width * 3) + 46
        self.heightNewCell.constant = heightRow
        
        self.viewAlpha.isHidden = true
        
        let visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        
        // Configure the view with tint color, blur radius, etc
        visualEffectView.colorTint = UIColor.white
        visualEffectView.colorTintAlpha = 0.1
        visualEffectView.blurRadius = 10
        visualEffectView.scale = 1
        
        self.view.addSubview(visualEffectView)
        self.view.addSubview(viewContent)
        
        self.btnDetailOfWork.layer.cornerRadius = 30
        self.btnDetailOfWork.layer.masksToBounds = true
        
        self.lbDate.text = strDate
        self.lbLotNumber.text = strLotNumber
        self.lbNameOfSterilizer.text = strNameOfSterilizer
    }
    
    func showInVC(vc: UIViewController){
        vc.addChildViewController(self)
        vc.view.addSubview(self.view)
        self.didMove(toParentViewController: vc)
    }
    
    func dismiss() {
        self.willMove(toParentViewController: nil)
        self.view .removeFromSuperview()
        self.removeFromParentViewController()
    }
}

//MARK: Delegate
extension SNAddJobSuccessVC: SNDetailWorkDelegate {
    func reloadListWork() {
        self.delegate?.reloadListWorkFromSuccessVC()
    }
}
