//
//  SNListSterilization_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import CoreData

extension SNListSterilizationVC: DateSettingDelegate, CustomSettingDelegate, TotalBottomCellDelegate, ListSterilizationCell3Delegate {
    //MARK: - DateSettingDelegate
    func dateChangeButtonDidToggle(strDate: String, strYear: Int, strMonth: Int, strDay: Int, strHour: Int, strMinute: String) {
        if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell4 {
            if curIndexPath!.section == 0 {
                if curIndexPath!.row == 2 {
                    self.dateSave = "\(strYear)-\(strMonth)-\(strDay)"
                    cell.lbContent.text = strDate
                    curInfo.dateOfWork = cell.lbContent.text!
                }
                else if curIndexPath!.row == 6 {
                    cell.lbContent.text = "\(strHour):\(strMinute)"
                    curInfo.startTime = cell.lbContent.text!
                }
                else if curIndexPath!.row == 7 {
                    cell.lbContent.text = "\(strHour):\(strMinute)"
                    curInfo.endTime = cell.lbContent.text!
                }
            }
            else if curIndexPath!.section == 3 {
                if curIndexPath!.row == 3 {
                    cell.lbContent.text = "\(strHour):\(strMinute)"
                    curBiologica.cultivation_start_time = cell.lbContent.text!
                }
                else if curIndexPath!.row == 4 {
                    cell.lbContent.text = "\(strHour):\(strMinute)"
                    curBiologica.result_check_time = cell.lbContent.text!
                }
            }
        }
        view.endEditing(true)
    }
    
    //MARK: - CustomSettingDelegate
    func customChangeButtonDidToggle(strContent: String) {
        if curIndexPath!.section < 4 {
            if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell4 {
                cell.lbContent.text = strContent
                if curIndexPath!.section == 0 {
                    if curIndexPath!.row == 3 {
                        curInfo.useSterilizer = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 5 {
                        curInfo.operationFrequency = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 8 {
                        curInfo.ci_indicator_check = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 11 {
                        curInfo.bi_indicator_check = cell.lbContent.text!
                    }
                }
                else if curIndexPath!.section == 1 {
                    if curIndexPath!.row == 0 {
                        curPhysical.vapor_pressure = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 1 {
                        curPhysical.sterilization_temperature = cell.lbContent.text!
                    }
                    else {
                        curPhysical.sterilization_time = cell.lbContent.text!
                    }
                }
                else if curIndexPath!.section == 2 {
                    if curIndexPath!.row == 0 {
                        curChemical.bd_test = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 2 {
                        curChemical.indicator = cell.lbContent.text!
                    }
                }
                else if curIndexPath!.section == 3 {
                    if curIndexPath!.row == 0 {
                        curBiologica.positive_control = cell.lbContent.text!
                    }
                    else if curIndexPath!.row == 2 {
                        curBiologica.judgment = cell.lbContent.text!
                    }
                }
            }
        }else {
            let deviceChange = curListDevice[curIndexPath!.row - 2]
            if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell3 {
                if cell.tfPart.tag == tagTfCell3 {
                    cell.tfPart.text = strContent
                    deviceChange.part = cell.tfPart.text!
                }
                else if cell.tfTechnicalEquipment.tag == tagTfCell3 {
                    cell.tfTechnicalEquipment.text = strContent
                    deviceChange.technical_equipment = cell.tfTechnicalEquipment.text!
                }
            }
        }
        view.endEditing(true)
        print(curInfo.bi_indicator_check)
    }
    
    //MARK: - TotalBottomCellDelegate
    func backButtonDidToggle(cell: TotalBottomCell) {
        /*
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            //Do your post-animation work here
            self.delegate?.reloadAfterCreateNewWork(strDate: "", strLotNumber: "", strNameOfSterilizer: "")
        }
        _ = self.navigationController?.popToRootViewController(animated: false)
        CATransaction.commit()
         */
        curSterilization = SterilizationModel.init(info: curInfo, physical: curPhysical, chemical: curChemical, biological: curBiologica, listDevice: curListDevice)
        //print(curSterilization.curInfo.entrant)
        self.createWork()
    }
    
    //MARK: - ListSterilizationCell3Delegate
    func addButtonDidToggle() {
        let newDevice = DeviceModel()
        curListDevice.append(newDevice)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: curListDevice.count + 1, section: 4)], with: .automatic)
        tableView.endUpdates()
    }
}

//MARK: - Func
extension SNListSterilizationVC {
    func createWork() {
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        switch typeFromVC {
        case .ListJobVC:
            let entity = NSEntityDescription.entity(forEntityName: "ModelWork", in: context)
            let newWork = NSManagedObject(entity: entity!, insertInto: context)
            
            newWork.setValue(["noImg","","","","",""], forKey: "doc_rec")
            newWork.setValue(["noImg","noImg","noImg"], forKey: "results_rec_ac")
            newWork.setValue(["noImg","noImg"], forKey: "results_rec_eo")
            newWork.setValue(["noImg","noImg"], forKey: "results_rec_hydro")
            newWork.setValue(["noImg","noImg"], forKey: "results_rec_formalin")
            newWork.setValue(self.dateSave, forKey: "date")
            newWork.setValue(curInfo.loadNo, forKey: "lot_num")
            newWork.setValue(curInfo.useSterilizer, forKey: "name_of_ster")
            //        newWork.setValue(self.dateCurrent, forKey: "date_current")
            newWork.setValue([0,0,0], forKey: "check_condition_ac")
            newWork.setValue([0,0], forKey: "check_condition_eo")
            newWork.setValue(self.curSterilization, forKey: "sterilization")
        case .DetailWorkVC:
            let dateStr = curInfo.dateOfWork
            var editTypeDate = dateStr.replacingOccurrences(of: "年", with: "-")
            editTypeDate = editTypeDate.replacingOccurrences(of: "月", with: "-")
            editTypeDate = editTypeDate.replacingOccurrences(of: "日", with: " ")
            
            self.dataModel.setValue(self.curSterilization, forKey: "sterilization")
            self.dataModel.setValue(editTypeDate, forKey: "date")
            self.dataModel.setValue(curInfo.loadNo, forKey: "lot_num")
            self.dataModel.setValue(curInfo.useSterilizer, forKey: "name_of_ster")
        }

        do {
            try context.save()
            
            CATransaction.begin()
            CATransaction.setCompletionBlock {
                //Do your post-animation work here
            self.delegate?.reloadAfterCreateNewWork(strDate: self.curInfo.dateOfWork, strLotNumber: self.curInfo.loadNo, strNameOfSterilizer: self.curInfo.useSterilizer)
            }
            _ = self.navigationController?.popToRootViewController(animated: false)
            CATransaction.commit()
            //self.showSuccessVC()
        } catch {
            print("Failed saving")
        }
    }
}
