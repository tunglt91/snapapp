//
//  SNListSterilizationVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

enum TypeFromVC {
    case ListJobVC
    case DetailWorkVC
}

class SNListSterilizationVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    lazy var keyboardSupportView:UIView = {
        let viewX = UIView.init()
        viewX.backgroundColor = UIColor.clear
        viewX.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(viewX)
        viewX.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        viewX.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        viewX.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        viewX.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector (self.handleTapViewX(_:)))
        viewX.addGestureRecognizer(tapGesture)
        
        return viewX
    }()
    
    var delegate:SNAddJobDelegate?
    
    var arrSection:[String] = ["滅菌作業記録", "1 : 物理学的モニタリング", "2 : 化学的 モニタリング", "3 : 生物学的モニタリング", "器材管理"]
    var arrSec1:[String] = ["記入者", "責任者", "作業年月日", "使用滅菌器", "滅菌器番号", "運転回数", "滅菌開始時間", "滅菌終了時間", "CIインジケーター確認", "CI (BDテスト)  Lot番号", "CI (カード)  Lot番号", "BIインジケーター確認", "Load No."]
    var arrSec2:[String] = ["蒸気圧", "滅菌温度", "滅菌時間"]
    var arrSec4:[String] = ["陽性コントロール", "挿入位置", "判定", "培養開始時間", "結果確認時間"]
    
    lazy var arrType:[String] = ["","高圧蒸気滅菌", "EOガス", "過酸化水素", "ホルムアルデヒド"]
    lazy var arrayNumber:[String] = ["0","1","2","3","4","5","6","7","8","9","10"]
    lazy var arrTemperature:[String] = ["","121℃", "126℃", "132℃", "134℃", "135℃"]
    lazy var arrPart:[String] = ["","呼吸外科", "外科", "心臓外科", "泌尿器", "形成外科", "歯科", "耳鼻科", "婦人科", "整形外科", "脳外", "眼科", "病棟", "その他"]
    
    var curIndexPath:IndexPath?
    lazy var dateView:DateSettingView = DateSettingView()
    lazy var customSettingView:CustomSettingView = CustomSettingView()
    //var arrayDevice:[ModelDevice] = []
    lazy var curInfo:InfoModel = InfoModel()
    lazy var curPhysical:PhysicalModel = PhysicalModel()
    lazy var curChemical:ChemicalModel = ChemicalModel()
    lazy var curBiologica:BiologicalModel = BiologicalModel()
    lazy var curListDevice:[DeviceModel] = []
    var curSterilization:SterilizationModel!
    var dateSave:String = ""
    var typeFromVC:TypeFromVC = .ListJobVC
    var dataModel:ModelWork = ModelWork()
    var tagTfCell3:Int = 0
    
    var dataImgDocument:[Any] = []
    var dataImgAC:[Any] = []
    var dataImgEO:[Any] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        switch typeFromVC {
        case .ListJobVC:
            break
        case .DetailWorkVC:
            dataImgDocument = dataModel.value(forKey: "doc_rec") as! [Any]
            dataImgAC = dataModel.value(forKey: "results_rec_ac") as! [Any]
            dataImgEO = dataModel.value(forKey: "results_rec_eo") as! [Any]
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Func
    func setupView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.init(hexString: "F0F0F0")
        
        let headerNib = UINib(nibName: "ListJobSectionCell", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "ListJobSectionCell")
        
        let nib1 = UINib(nibName: "ListSterilizationCell1", bundle: nil)
        self.tableView.register(nib1, forCellReuseIdentifier: "ListSterilizationCell1")
        
        let nib2 = UINib(nibName: "ListSterilizationCell2", bundle: nil)
        self.tableView.register(nib2, forCellReuseIdentifier: "ListSterilizationCell2")
        
        let nib3 = UINib(nibName: "TotalBottomCell", bundle: nil)
        self.tableView.register(nib3, forCellReuseIdentifier: "TotalBottomCell")
        
        let nib4 = UINib(nibName: "ListSterilizationCell3", bundle: nil)
        self.tableView.register(nib4, forCellReuseIdentifier: "ListSterilizationCell3")
        
        let nib5 = UINib(nibName: "ListSterilizationCell4", bundle: nil)
        self.tableView.register(nib5, forCellReuseIdentifier: "ListSterilizationCell4")
        
        let nib6 = UINib(nibName: "ListSterilizationCell5", bundle: nil)
        self.tableView.register(nib6, forCellReuseIdentifier: "ListSterilizationCell5")
        
        self.dateSave = currentDate()
        
        if typeFromVC == .DetailWorkVC {
            self.curInfo = self.curSterilization.curInfo
            self.curPhysical = self.curSterilization.curPhysical
            self.curChemical = self.curSterilization.curChemical
            self.curBiologica = self.curSterilization.curBiological
            self.curListDevice = self.curSterilization.curListDevice
        }
    }
    
    func currentDate() -> String {
        //Current Date
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return "\(year)-\(month)-\(day)"
    }
    
    //MARK: - Notification Keyboard
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            print(userInfo)
            //let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let isKeyboardShowing = notification.name == .UIKeyboardWillShow
            
            if isKeyboardShowing {
                self.view.bringSubview(toFront: self.keyboardSupportView)
                self.keyboardSupportView.isHidden = false
            }
            else {
                self.keyboardSupportView.isHidden = true
            }
            
        }
        
    }
    
    //MARK: - IBAction
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTapViewX(_ sender: UITapGestureRecognizer) {
        if Int((curIndexPath!.section)) < 4 {
            print("IndexPath: ",curIndexPath!)
            if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell1 {
                if curIndexPath == [0,0] || curIndexPath == [0,1] || curIndexPath == [0,4] || curIndexPath == [0,9] || curIndexPath == [0,10] || curIndexPath == [0,12] {
                    cell.tfContent.resignFirstResponder()
                }
            } else if curIndexPath == [0, 3] || curIndexPath == [0, 5] || curIndexPath == [0, 8] || curIndexPath == [0, 11] {
                UIView.animate(withDuration: 0.2, animations: {
                    self.customSettingView.frame.origin.y += 300
                }) { (bool) in
                    self.customSettingView.removeFromSuperview()
                }
            } else if curIndexPath == [0, 2] || curIndexPath == [0, 6] || curIndexPath == [0, 7] {
                UIView.animate(withDuration: 0.2, animations: {
                    self.dateView.frame.origin.y += 300
                }) { (bool) in
                    self.dateView.removeFromSuperview()
                }
            }  else if curIndexPath == [1, 0] || curIndexPath == [1, 1] || curIndexPath == [1, 2] || curIndexPath == [2, 0] || curIndexPath == [2, 2] || curIndexPath == [3,0] || curIndexPath == [3, 2]{
                UIView.animate(withDuration: 0.2, animations: {
                    self.customSettingView.frame.origin.y += 300
                }) { (bool) in
                    self.customSettingView.removeFromSuperview()
                }
            } else if curIndexPath == [3, 3] || curIndexPath == [3, 4] {
                UIView.animate(withDuration: 0.2, animations: {
                    self.dateView.frame.origin.y += 300
                }) { (bool) in
                    self.dateView.removeFromSuperview()
                }
            }
            
        }else {
            if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell3 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.customSettingView.frame.origin.y += 300
                }) { (bool) in
                    self.customSettingView.removeFromSuperview()
                }
                
                cell.tfPart.resignFirstResponder()
                cell.tfTechnicalEquipment.resignFirstResponder()
                cell.tfAmount.resignFirstResponder()
            }
        }

        view.endEditing(true)
        self.keyboardSupportView.isHidden = true
    }
    
}

extension SNListSterilizationVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let buttonPosition:CGPoint = textField.convert(CGPoint.zero, to:self.tableView)
        let indexPathSelect = self.tableView.indexPathForRow(at: buttonPosition)
        curIndexPath = indexPathSelect
        print(curIndexPath!)
        if curIndexPath!.section < 4 {
            if let cell = self.tableView.cellForRow(at: indexPathSelect!) as? ListSterilizationCell1 {
                // Section 0
                if indexPathSelect == [0, 0] || indexPathSelect == [0, 1] || indexPathSelect == [0, 4] || indexPathSelect == [0, 9] || indexPathSelect == [0, 10] || indexPathSelect == [0, 12] {
                    if indexPathSelect == [0, 0] || indexPathSelect == [0, 1] || indexPathSelect == [0,4] || indexPathSelect == [0, 9] || indexPathSelect == [0, 10] {
//                            cell.tfContent.text = ""
                    }
                    cell.tfContent.becomeFirstResponder()
                }
                else if indexPathSelect == [0, 2]{
//                    view.endEditing(true)
                    cell.tfContent.resignFirstResponder()
                    self.showDateSettingView(modePicker: .date)
                }
                else if indexPathSelect == [0, 3] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: arrType, type: .TypeScan, content: cell.tfContent.text!)
                }
                else if indexPathSelect == [0, 5] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: arrayNumber, type: .Number, content: cell.tfContent.text!)
                }
                else if indexPathSelect == [0, 6] || indexPathSelect == [0, 7] {
                    cell.tfContent.resignFirstResponder()
                    self.showDateSettingView(modePicker: .time)
                }
                else if indexPathSelect == [0, 8] || indexPathSelect == [0, 11] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: ["","あり", "なし"], type: .YesOrNo, content: cell.tfContent.text!)
                }
                    // Section 1
                else if indexPathSelect == [1, 0] {
                    cell.tfContent.resignFirstResponder()
                    var arrVaporPressure:[String] = []
                    let queue = DispatchQueue(label: "queue")
                    queue.sync {
                        /*
                        for i in 0..<24 {
                            let doubleVapor = "0.\(i)"
                            arrVaporPressure.append(doubleVapor)
                        }
                         */
                        for i in stride(from: 0.00, to: 0.24, by: 0.01) {
                            arrVaporPressure.append("\(i)")
                        }
                        arrVaporPressure.insert("", at: 0)
                    }
                    self.showCustomSettingView(array: arrVaporPressure, type: .VaporPressure, content: cell.tfContent.text!)
                }
                else if indexPathSelect == [1, 1] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: arrTemperature, type: .Temperature, content: cell.tfContent.text!)
                }
                else if indexPathSelect == [1, 2] {
                    cell.tfContent.resignFirstResponder()
                    var arrSterilizationTime:[String] = []
                    let queue = DispatchQueue(label: "queue")
                    queue.sync {
                        for i in 3..<31 {
                            arrSterilizationTime.append("\(i)")
                        }
                        arrSterilizationTime.insert("", at: 0)
                    }
                    self.showCustomSettingView(array: arrSterilizationTime, type: .SterilizationTime, content: cell.tfContent.text!)
                }
                    // Section 2
                else if indexPathSelect == [2, 0] || indexPathSelect == [2, 2] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: ["","良", "不良"], type: .GoodOrBad, content: cell.tfContent.text!)
                }
                    // Section 3
                else if indexPathSelect == [3, 0] || indexPathSelect == [3, 2] {
                    cell.tfContent.resignFirstResponder()
                    self.showCustomSettingView(array: ["","+", "-"], type: .GoodOrBad, content: cell.tfContent.text!)
                }
                else if indexPathSelect == [3, 1] {
//                    cell.tfContent.text = ""
                    cell.tfContent.becomeFirstResponder()
                }
                else if indexPathSelect == [3, 3] || indexPathSelect == [3, 4] {
                    cell.tfContent.resignFirstResponder()
                    self.showDateSettingView(modePicker: .time)
                }
            }
        }else {
            tagTfCell3 = textField.tag
            if let cell = self.tableView.cellForRow(at: indexPathSelect!) as? ListSterilizationCell3 {
                if textField.tag == 1 {
                    cell.tfPart.resignFirstResponder()
                    self.showCustomSettingView(array: arrPart, type: .Part, content: cell.tfPart.text!)
                }
                else if textField.tag == 2 {
                    cell.tfTechnicalEquipment.resignFirstResponder()
                    
                    if let filepath = Bundle.main.path(forResource: "TechnicalEquipment", ofType: "txt") {
                        do {
                            let contents = try String(contentsOfFile: filepath)
                            let arrayTechnicalEquipment = contents.components(separatedBy: "\n")
                            self.showCustomSettingView(array: arrayTechnicalEquipment, type: .TechnicalEquipment, content: cell.tfTechnicalEquipment.text!)
                        } catch {
                            // contents could not be loaded
                        }
                    } else {
                        // example.txt not found!
                    }
                }
            }
        }
    }
        
    func textFieldDidEndEditing(_ textField: UITextField) {
        if curIndexPath!.section < 4 {
            if curIndexPath!.section == 0 {
                if textField.tag == 0 {
                    curInfo.entrant = textField.text!
                }
                else if textField.tag == 1 {
                    curInfo.responsiblePerson = textField.text!
                }
                else if textField.tag == 4 {
                    curInfo.sterilizerNumber = textField.text!
                }
                else if textField.tag == 9 {
                    curInfo.ci_BDtest = textField.text!
                }
                else if textField.tag == 10 {
                    curInfo.ci_Card = textField.text!
                }
                else if textField.tag == 12 {
                    curInfo.loadNo = textField.text!
                }
            }
            else if curIndexPath!.section == 3 {
                if textField.tag == 1 {
                    curBiologica.insertion_position = textField.text!
                }
            }
        }else {
            let deviceChange = curListDevice[curIndexPath!.row - 2]
            /*
            if textField.tag == 1 {
                deviceChange.part = textField.text!
            }
            else if textField.tag == 2 {
                deviceChange.technical_equipment = textField.text!
            }
             */
            if textField.tag == 3 {
                deviceChange.amount = textField.text!
            }
        }
        print(curInfo.entrant)
    }
    
    func showDateSettingView(modePicker: UIDatePickerMode) {
        keyboardSupportView.isHidden = false
        dateView = DateSettingView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300))
        dateView.datePicker.datePickerMode = modePicker
        UIView.animate(withDuration: 0.2, animations: {
            self.dateView.frame.origin.y -= 300
        }, completion: nil)
        dateView.delegate = self
        self.view.addSubview(dateView)
    }
    
    func showCustomSettingView(array: [String], type: TypeCustom, content: String) {
        keyboardSupportView.isHidden = false
        customSettingView = CustomSettingView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300))
        customSettingView.delegate = self
        customSettingView.typeCustom = type
        customSettingView.arrayPicker = array
        switch type {
        case .Number:
            for i in 0..<array.count {
                if content.elementsEqual("\(array[i])回目") {
                    customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                }
            }
            break
        case .VaporPressure:
            for i in 0..<array.count {
                if array[i] == "" {
                    if content.elementsEqual(array[i]) {
                        customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                    }
                } else {
                    if content.elementsEqual("\(array[i]) MPa") {
                        customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                    }
                }
            }
            break
        case .SterilizationTime:
            for i in 0..<array.count {
                if array[i] == "" {
                    if content.elementsEqual(array[i]) {
                        customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                    }
                } else {
                    if content.elementsEqual("\(array[i]) 分") {
                        customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                    }
                }
            }
            break
        default:
            for i in 0..<array.count {
                if content.elementsEqual(array[i]) {
                    customSettingView.pickerView.selectRow(i, inComponent: 0, animated: true)
                }
            }
            break
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.customSettingView.frame.origin.y -= 300
        }, completion: nil)
        self.view.addSubview(customSettingView)
    }
}

extension SNListSterilizationVC: ButtonDelegate {
    func Click(str: String, row: Int, section: Int) {
        curIndexPath = IndexPath(row: row, section: section)
        if let cell = self.tableView.cellForRow(at: curIndexPath!) as? ListSterilizationCell4{
            if curIndexPath == [0,2] {
                self.showDateSettingView(modePicker: .date)
            }
            else if curIndexPath == [0,3] {
                self.showCustomSettingView(array: arrType, type: .TypeScan, content: cell.lbContent.text!)
            }
            else if curIndexPath == [0, 5] {
                self.showCustomSettingView(array: arrayNumber, type: .Number, content: cell.lbContent.text!)
            }
            else if curIndexPath == [0, 6] || curIndexPath == [0, 7] {
                self.showDateSettingView(modePicker: .time)
            }
            else if curIndexPath == [0, 8] || curIndexPath == [0, 11] {
                self.showCustomSettingView(array: ["","あり", "なし"], type: .YesOrNo, content: cell.lbContent.text!)
            }
            else if curIndexPath == [1,0] {
                var arrVaporPressure:[String] = []
                let queue = DispatchQueue(label: "queue")
                queue.sync {
                    for i in stride(from: 0.00, to: 0.24, by: 0.01) {
                        arrVaporPressure.append("\(i)")
                    }
                    arrVaporPressure.insert("", at: 0)
                }
                self.showCustomSettingView(array: arrVaporPressure, type: .VaporPressure, content: cell.lbContent.text!)
            }
            else if curIndexPath == [1,1] {
                self.showCustomSettingView(array: arrTemperature, type: .Temperature, content: cell.lbContent.text!)
            }
            else if curIndexPath == [1,2] {
                var arrSterilizationTime:[String] = []
                let queue = DispatchQueue(label: "queue")
                queue.sync {
                    for i in 3..<31 {
                        arrSterilizationTime.append("\(i)")
                    }
                    arrSterilizationTime.insert("", at: 0)
                }
                self.showCustomSettingView(array: arrSterilizationTime, type: .SterilizationTime, content: cell.lbContent.text!)
            }
            else if curIndexPath == [2,0] || curIndexPath == [2,2] {
                self.showCustomSettingView(array: ["","良", "不良"], type: .GoodOrBad, content: cell.lbContent.text!)
            }
            else if curIndexPath == [3,0] || curIndexPath == [3,2] {
                self.showCustomSettingView(array: ["","+", "-"], type: .GoodOrBad, content: cell.lbContent.text!)
            }
            else if curIndexPath == [3,3] || curIndexPath == [3,4] {
                self.showDateSettingView(modePicker: .time)
            }
        }
    }
}
