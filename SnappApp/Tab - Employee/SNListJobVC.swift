//
//  SNListJobVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/21/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

fileprivate func parseDate(_ str : String) -> Date {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "yyyy-MM-dd"
    return dateFormat.date(from: str)!
}

fileprivate func firstDayOfMonth(date : Date) -> Date {
    
    let calendar = Calendar.current
    let components = calendar.dateComponents([.year, .month, .day], from: date)
    return calendar.date(from: components)!
    
}

enum TabType {
    case Tab1
    case Tab2
    case Tab3
}

class SNListJobVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnShowAll: UIButton!
    @IBOutlet weak var btnShowLatestWeek: UIButton!
    @IBOutlet weak var btnShowOmission: UIButton!
    @IBOutlet weak var btnAddJob: UIButton!
    @IBOutlet weak var viewAddJob: UIView!
    @IBOutlet weak var lineTab1: UIView!
    @IBOutlet weak var lineTab2: UIView!
    @IBOutlet weak var lineTab3: UIView!
    
    let attributeSelect : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 20),
        NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.black,
        NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) : NSUnderlineStyle.styleSingle.rawValue]
    
    let attributeNotSelect : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 20),
        NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.lightGray]
    
    var arrayWork:[ModelWork] = []
    var newWork:[ModelWork] = []
    
    var sections = [TableSection<Date, ModelWork>]()
    var tabType:TabType = .Tab1
    
    var storedOffsets = [Int: CGFloat]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.reloadDataWork()
    }
    
    //MARK: - Action Segment
    @IBAction func segmentedControlValueChanged(_ sender: UIButton) {
        if sender.tag == 0 {
            tabType = .Tab1
            
            self.lineTab1.isHidden = false
            self.lineTab2.isHidden = true
            self.lineTab3.isHidden = true
            self.btnShowAll.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            
            self.btnShowAll.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            self.btnShowLatestWeek.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            self.btnShowOmission.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        else if sender.tag == 1 {
            tabType = .Tab2
            
            self.lineTab1.isHidden = true
            self.lineTab2.isHidden = false
            self.lineTab3.isHidden = true
            self.btnShowAll.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            self.btnShowAll.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            self.btnShowLatestWeek.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            self.btnShowOmission.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeNotSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        else {
            tabType = .Tab3
            
            self.lineTab1.isHidden = true
            self.lineTab2.isHidden = true
            self.lineTab3.isHidden = false
            self.btnShowAll.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            
            self.btnShowAll.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            self.btnShowLatestWeek.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
            self.btnShowOmission.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeNotSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        self.tableView.reloadData()
    }
    
    //MARK: - IBAction
    @IBAction func addJobPress(_ sender: Any) {
        /*
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let addJobVC = storyboard.instantiateViewController(withIdentifier: "SNAddJobVC") as? SNAddJobVC
        addJobVC?.delegate = self
        self.navigationController?.pushViewController(addJobVC!, animated: true)
         */
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let listSterilizationVC = storyboard.instantiateViewController(withIdentifier: "SNListSterilizationVC") as? SNListSterilizationVC
        listSterilizationVC?.delegate = self
        listSterilizationVC?.typeFromVC = .ListJobVC
        self.navigationController?.pushViewController(listSterilizationVC!, animated: true)
    }
    
    @IBAction func showMenuAdminPress(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let menuAdminVC = storyboard.instantiateViewController(withIdentifier: "SNTopMenuAdminVC") as? SNTopMenuAdminVC
        self.navigationController?.pushViewController(menuAdminVC!, animated: true)
    }
    
    
    //MARK: - Func
    func setupView() {
        self.viewAddJob.layer.cornerRadius = 8
        self.viewAddJob.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        let headerNib = UINib(nibName: "ListJobSectionCell", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "ListJobSectionCell")
        
        let nib = UINib(nibName: "ListJobCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ListJobCell")
        
        self.lineTab1.isHidden = false
        self.lineTab2.isHidden = true
        self.lineTab3.isHidden = true
        self.btnShowAll.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
        self.btnShowLatestWeek.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
        self.btnShowOmission.setTitleColor(UIColor.init(hexString: "bcbcbc"), for: .normal)
        
        /*
        let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeSelect)
        self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
        
        let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
        self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
        
        let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
        self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
         */
    }
    
    func reloadDataWork() {
        arrayWork.removeAll()
        newWork.removeAll()
        self.sections.removeAll()
        arrayWork = LocalData.sharedInstance.dataWork()
        
        if arrayWork.count > 0 {
            //newWork
            newWork.append(arrayWork[arrayWork.count - 1])
            //xoa công việc mới tạo ở phần ở dưới
            arrayWork.remove(at: arrayWork.count - 1)
        }
        
        self.sections = TableSection.group(rowItems: self.arrayWork, by: { (headline) in
            return firstDayOfMonth(date: parseDate(headline.date!))
        })
        
        self.tableView.reloadData()
    }
}

extension SNListJobVC: SNAddJobDelegate, SNDetailWorkDelegate, SNAddJobSuccessDelegate{
    //MARK: -SNAddJobDelegate
    func reloadAfterCreateNewWork(strDate: String, strLotNumber: String, strNameOfSterilizer: String) {
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SNAddJobSuccessVC") as? SNAddJobSuccessVC
        vc?.delegate = self
        vc?.strDate = strDate
        vc?.strLotNumber = strLotNumber
        vc?.strNameOfSterilizer = strNameOfSterilizer
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ListJobCell {
            vc?.positionYCell = cell.frame.origin.y
        }
        vc?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        vc?.showInVC(vc: self)
    }
    
    //MARK: -SNDetailWorkDelegate
    func reloadListWork() {
        self.reloadDataWork()
    }
    
    //MARK: -SNAddJobSuccessDelegate
    func reloadListWorkFromSuccessVC() {
        self.reloadDataWork()
    }
    
}
