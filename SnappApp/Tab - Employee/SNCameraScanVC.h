//
//  SNCameraScanVC.h
//  SnappApp
//
//  Created by Tu Ngo on 9/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "Target.h"
#import "SNScanVC.h"
@class ModelWork;
//fk20180914
//#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SNCameraScanDelegate <NSObject>

- (void)reloadDataTestResult:(NSArray *)array selectionFlag:(NSInteger)selectionFlag dataCheckCondition:(NSArray *)dataCheckCondition;

@end

@interface SNCameraScanVC : UIViewController <AVCapturePhotoCaptureDelegate>

@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureDeviceInput *videoInput;


//fk20180815
@property (strong, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
//@property (strong, nonatomic) AVCapturePhotoOutput *avCaptureOutput;
//@property (strong, nonatomic) AVCapturePhotoSettings *avSettings;


- (IBAction)capture:(id)sender;

@property (strong, nonatomic) UIImageView *uv;

@property (nonatomic, strong) AVCaptureSession* session;

//@property (weak, nonatomic) IBOutlet UISlider *slider;
//- (IBAction)sliderChanged:(id)sender;

@property (strong, nonatomic) AVCaptureDevice *videoCaptureDevice;

//fk20180814
//@property (weak, nonatomic) IBOutlet UILabel *actualZoomFactor;

//fk20180816
//@property (weak, nonatomic) IBOutlet UISwitch *pics;
//- (IBAction)picsSelected:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *pics1233;
@property (weak, nonatomic) IBOutlet UIImageView *pics1250;

@property(nonatomic, weak) id<SNCameraScanDelegate> delegate;

@property (nonatomic) NSInteger targetType;
@property (nonatomic) id arrayImg;
@property (nonatomic) NSInteger indexArray;
@property (nonatomic) ModelWork *dataWork;

@end


NS_ASSUME_NONNULL_END
