//
//  SNAddJobVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import CoreData

enum SettingType {
    case Date
    case LotOfNumber
    case NameOfSterilizer
}

protocol SNAddJobDelegate: class {
    func reloadAfterCreateNewWork(strDate: String, strLotNumber: String, strNameOfSterilizer: String)
}

class SNAddJobVC: UIViewController {
    @IBOutlet weak var viewSetting: UIView!
    @IBOutlet weak var btnChangeDate: UIButton!
    @IBOutlet weak var btnChangeLotNumber: UIButton!
    @IBOutlet weak var btnChangeNameOfSterilizer: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var tfLotOfNumber: UITextField!
    @IBOutlet weak var tfNameOfSterilizer: UITextField!
    
    var delegate:SNAddJobDelegate?
    
    lazy var keyboardSupportView:UIView = {
        let viewX = UIView.init()
        viewX.backgroundColor = UIColor.clear
        viewX.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(viewX)
        viewX.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        viewX.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        viewX.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        viewX.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        
         let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector (self.handleTapViewX(_:)))
         viewX.addGestureRecognizer(tapGesture)
        
        return viewX
    }()
    
    var settingType:SettingType = .Date
    var dateView:DateSettingView = DateSettingView()
    var dateSave:String = ""
    var dateCurrent:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    //MARK: - IBAction
    @IBAction func dateSettingPress(_ sender: Any) {
        settingType = .Date
        keyboardSupportView.isHidden = false
        dateView = DateSettingView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300))
        UIView.animate(withDuration: 0.2, animations: {
            self.dateView.frame.origin.y -= 300
        }, completion: nil)
        dateView.delegate = self
        self.view.addSubview(dateView)
    }
    
    @IBAction func lotNumberPress(_ sender: Any) {
        settingType = .LotOfNumber
        self.tfLotOfNumber.isUserInteractionEnabled = true
        self.tfLotOfNumber.becomeFirstResponder()
    }
    
    @IBAction func nameOfSterilizerPress(_ sender: Any) {
        settingType = .NameOfSterilizer
        self.tfNameOfSterilizer.isUserInteractionEnabled = true
        self.tfNameOfSterilizer.becomeFirstResponder()
    }
    
    @IBAction func signUpPress(_ sender: Any) {
        self.createWork()
    }
    
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTapViewX(_ sender: UITapGestureRecognizer) {
        switch settingType {
        case .Date:
            UIView.animate(withDuration: 0.2, animations: {
                self.dateView.frame.origin.y += 300
            }) { (bool) in
                self.dateView.removeFromSuperview()
            }
        case .LotOfNumber:
            self.tfLotOfNumber.resignFirstResponder()
            self.tfLotOfNumber.isUserInteractionEnabled = false
            break
        case .NameOfSterilizer:
            self.tfNameOfSterilizer.resignFirstResponder()
            self.tfNameOfSterilizer.isUserInteractionEnabled = false
            break
        }
        
        self.keyboardSupportView.isHidden = true
    }
    
    //MARK: -Func
    func setupView() {
        self.btnChangeDate.layer.cornerRadius = 16
        self.btnChangeDate.layer.masksToBounds = true
        
        self.btnChangeLotNumber.layer.cornerRadius = 16
        self.btnChangeLotNumber.layer.masksToBounds = true
        
        self.btnChangeNameOfSterilizer.layer.cornerRadius = 16
        self.btnChangeNameOfSterilizer.layer.masksToBounds = true
        
        self.btnSignUp.layer.cornerRadius = 30
        self.btnSignUp.layer.masksToBounds = true
        
        self.viewSetting.layer.cornerRadius = 8
        self.viewSetting.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewSetting.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.viewSetting.layer.shadowOpacity = 1
        self.viewSetting.layer.shadowRadius = 1
        self.viewSetting.layer.masksToBounds = false
        
        keyboardSupportView.isHidden = true
        
        //Current Date
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        self.dateSave = "\(year)-\(month)-\(day)"
        self.dateCurrent = "\(year)-\(month)-\(day) \(hour):\(minute):\(second)"
        self.lbDate.text = "\(year)年\(month)月\(day)日"
        //Disable Keyboard
        self.tfLotOfNumber.isUserInteractionEnabled = false
        self.tfNameOfSterilizer.isUserInteractionEnabled = false
    }
    
    func createWork() {
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
            
        let entity = NSEntityDescription.entity(forEntityName: "ModelWork", in: context)
        let newWork = NSManagedObject(entity: entity!, insertInto: context)
        
        newWork.setValue(["noImg","","","","",""], forKey: "doc_rec")
        newWork.setValue(["noImg","noImg","noImg"], forKey: "results_rec_ac")
        newWork.setValue(["noImg","noImg"], forKey: "results_rec_eo")
        newWork.setValue(["noImg","noImg"], forKey: "results_rec_hydro")
        newWork.setValue(["noImg","noImg"], forKey: "results_rec_formalin")
        newWork.setValue(self.dateSave, forKey: "date")
        newWork.setValue(self.tfLotOfNumber.text, forKey: "lot_num")
        newWork.setValue(self.tfNameOfSterilizer.text, forKey: "name_of_ster")
        newWork.setValue(self.dateCurrent, forKey: "date_current")
        newWork.setValue([0,0,0], forKey: "check_condition_ac")
        newWork.setValue([0,0], forKey: "check_condition_eo")
        
        do {
            try context.save()
            
            CATransaction.begin()
            CATransaction.setCompletionBlock {
                //Do your post-animation work here
                self.delegate?.reloadAfterCreateNewWork(strDate: self.lbDate.text!, strLotNumber: self.tfLotOfNumber.text!, strNameOfSterilizer: self.tfNameOfSterilizer.text!)
            }
            _ = self.navigationController?.popToRootViewController(animated: false)
            CATransaction.commit()
            //self.showSuccessVC()
        } catch {
            print("Failed saving")
        }
    }
    
    func showSuccessVC() {
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SNAddJobSuccessVC") as? SNAddJobSuccessVC
        vc?.strDate = self.lbDate.text!
        vc?.strLotNumber = self.tfLotOfNumber.text!
        vc?.strNameOfSterilizer = self.tfNameOfSterilizer.text!
        vc?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        vc?.showInVC(vc: self)
    }
    
    //MARK: - Notification Keyboard
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            print(userInfo)
            //let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let isKeyboardShowing = notification.name == .UIKeyboardWillShow
            
            if isKeyboardShowing {
                self.view.bringSubview(toFront: self.keyboardSupportView)
                self.keyboardSupportView.isHidden = false
            }
            else {
                self.keyboardSupportView.isHidden = true
            }
            
        }
        
    }
    
}
