//
//  SNDetailWorkDrag_Extension.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/10/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNDetailWorkVC {
    func addGesturesForCollectionDoc() {
        
        let longPressGest = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGestureActionDoc))
        longPressGest.delegate = self
        longPressGest.minimumPressDuration = 0.2
        self.collectionDocument.addGestureRecognizer(longPressGest)
        
        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(panGestureActionDoc))
        panGesture.delaysTouchesBegan = true
        panGesture.delegate = self
        self.collectionDocument.addGestureRecognizer(panGesture)
    }
    
    func addGesturesForCollectionTest() {
        
        let longPressGest = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGestureActionTest))
        longPressGest.delegate = self
        longPressGest.minimumPressDuration = 0.2
        self.collectionTestResult.addGestureRecognizer(longPressGest)
        
        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(panGestureActionTest))
        panGesture.delaysTouchesBegan = true
        panGesture.delegate = self
        self.collectionTestResult.addGestureRecognizer(panGesture)
    }
}

extension SNDetailWorkVC:UIGestureRecognizerDelegate {
    
    // MARK: - Gesture Delegate Methods
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc fileprivate func panGestureActionDoc (_ gestureRecognizer : UIPanGestureRecognizer) {
        
        if !isPanAllowed || !customImgView.isDescendant(of: self.view) {
            collectionDocument.isScrollEnabled = true
            return
        }
        
        let translation = gestureRecognizer.translation(in: self.view)
        collectionDocument.isScrollEnabled = false
        
        if gestureRecognizer.state == .began  {
            startFrame = customImgView.frame
        }
        else if gestureRecognizer.state == .changed {
            
            checkWhetherProductMovedDestinationDoc()
            customImgView.center = CGPoint(x: customImgView.center.x + translation.x, y: customImgView.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            
        }
        else if gestureRecognizer.state == .ended {
            caseMethodForPanCompletionWithDoc(gestureRecognizer)
        }
    }
    
    private func caseMethodForPanCompletionWithDoc(_ gestureRecognizer:UIPanGestureRecognizer) {
        
        
        if indexPathForItem == nil || !customImgView.isDescendant(of: self.view) {
            return
        }
        
        let velocity = gestureRecognizer.velocity(in: self.view)
        
        let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
        let slideMultiplier = magnitude / 200
        print("magnitude: \(magnitude), slideMultiplier: \(slideMultiplier)")
        
        let slideFactor = 0.1 * slideMultiplier     //Increase for more of a slide
        
        var finalPoint = CGPoint(x:customImgView.center.x + (velocity.x * slideFactor),
                                 y:customImgView.center.y + (velocity.y * slideFactor))
        finalPoint.x = min(max(finalPoint.x, 0), self.view.bounds.size.width)
        finalPoint.y = min(max(finalPoint.y, 0), self.view.bounds.size.height)
        
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: { [weak self] in
                        self?.customImgView.center = finalPoint
                        self?.checkWhetherProductMovedDestinationDoc()
            },
                       completion: { [weak self] _ in
                        
                        self?.panCompletionMethodDoc()
        })
        collectionDocument.isScrollEnabled = true
    }
    
    fileprivate func panCompletionMethodDoc() {
        
        if self.isReachedDest {
            UIView.animate(withDuration: 0.2, animations: { [weak weakSelf = self] in
                let destionViewCenterWithRespectMain = weakSelf?.getTheMainViewBasedCenterForDestinationDoc()
                weakSelf?.customImgView.frame = CGRect(x: (destionViewCenterWithRespectMain?.x)! - 5, y: (destionViewCenterWithRespectMain?.y)! - 5, width: 10.0, height: 10.0)
                }, completion:{  [weak weakSelf = self] _ in
                    
                    weakSelf?.doSomeThingsAfterPanCompletionDoc()
                    self.deleteImgDocument()
                })
        }
        else {
            UIView.animate(withDuration: 0.2, animations: { [weak weakSelf = self] in
                weakSelf?.customImgView.frame = (weakSelf?.constructFrameForIndexDoc(indexPath: (weakSelf?.indexPathForItem)!))!
                }, completion:{  [weak weakSelf = self] _ in
                    weakSelf?.doSomeThingsAfterPanCompletionDoc()})
        }
        
    }
    
    fileprivate func checkWhetherProductMovedDestinationDoc() {
        isPanRunning = true
        
        let draggingViewX = customImgView.frame.origin.x + customImgView.frame.size.width
        let draggingViewY = customImgView.frame.origin.y
        
        let destionViewCenterWithRespectMain = getTheMainViewBasedCenterForDestinationDoc()
        
        let requiredDestFrame = CGRect(x: destionViewCenterWithRespectMain.x - deleteView.frame.width/2, y: destionViewCenterWithRespectMain.y - deleteView.frame.height/2, width: deleteView.frame.size.width, height: deleteView.frame.size.height)
        
        isReachedDest = false
        if (draggingViewY <= requiredDestFrame.origin.y + deleteView.frame.size.height && draggingViewX >= requiredDestFrame.origin.x)  {
            isReachedDest = true
        }
    }
    
    fileprivate func getTheMainViewBasedCenterForDestinationDoc() -> CGPoint {
        var destionViewCenterWithRespectMain = deleteView.center
        
        if var destinationSuperView = deleteView.superview {
            destionViewCenterWithRespectMain = CGPoint(x: destionViewCenterWithRespectMain.x + destinationSuperView.frame.origin.x, y: destionViewCenterWithRespectMain.y + destinationSuperView.frame.origin.y)
            while let anotherSuperView = destinationSuperView.superview {
                destionViewCenterWithRespectMain = CGPoint(x: destionViewCenterWithRespectMain.x + anotherSuperView.frame.origin.x, y: destionViewCenterWithRespectMain.y + anotherSuperView.frame.origin.y)
                destinationSuperView = anotherSuperView
            }
        }
        return destionViewCenterWithRespectMain
    }
    
    @objc fileprivate func longPressGestureActionDoc(_ gestureRecognizer : UILongPressGestureRecognizer) {
        
        if isPanRunning {
            return
        }
        
        if gestureRecognizer.state == .began {
            
            let point = gestureRecognizer.location(in: collectionDocument)
            print(point)
            
            guard let indexPathToCheck = collectionDocument.indexPathForItem(at: point) else {
                return
            }
            if customImgView.isDescendant(of: self.view) {
                customImgView.removeFromSuperview()
            }
            isPanAllowed = true
            
            print("IndexPath : \(indexPathToCheck)")
            indexRemove = indexPathToCheck.row - 1
            print(indexPathToCheck.row)
            
            addCustomViewAtDoc(point)
        }
        else if gestureRecognizer.state == .ended {
            
            isPanAllowed = false
            
            if customImgView.isDescendant(of: view) {
                customImgView.removeFromSuperview()
            }
            collectionDocument.isScrollEnabled = true
        }
    }
    
    
    fileprivate func addCustomViewAtDoc(_ point:CGPoint) {
        
        guard let indexPath = collectionDocument.indexPathForItem(at: point) else {return}
        
        if customImgView.isDescendant(of: self.view) {
            print("Nil Indexpath or already view available>>>>>>> Returned")
            return
        }
        
        indexPathForItem = indexPath as IndexPath
        let checkData = dataImgDocument[0] as! String
        if checkData != "" && indexPath.row - 1 < dataImgDocument.count && indexPath.row != 0 {
            customImgView = UIImageView.init(frame:constructFrameForIndexDoc(indexPath: indexPath))
            customImgView.contentMode = .scaleToFill
            
            let itemAtIndexPath = dataImgDocument[(indexPath.row - 1)]
            //customImgView.image = UIImage.init(named: itemAtIndexPath.imageName)
            SNCommon.getImageDirectory(imgView: customImgView, nameImg: itemAtIndexPath)
            customImgView.backgroundColor = UIColor.clear
            
            startFrame = customImgView.frame
            self.view.addSubview(customImgView)
        }
    }
    
    
    //MARK: - All The Gesture Methods
    
    fileprivate func doSomeThingsAfterPanCompletionDoc() {
        
        if customImgView.isDescendant(of: view) {
            customImgView.removeFromSuperview()
        }
        isReachedDest = false
        isPanRunning = false
        isPanAllowed = false
    }
        
    fileprivate func constructFrameForIndexDoc(indexPath:IndexPath)->CGRect {
        
        let attributes = collectionDocument.layoutAttributesForItem(at: indexPath as IndexPath)
        let sizeRect = attributes?.frame
        
        let tempView = UIView.init(frame: CGRect(x: 0, y: 0, width: (sizeRect?.size.width)! + 20.0, height: (sizeRect?.size.height)! + 20.0))
        tempView.center = CGPoint(x: (attributes?.center.x)! + collectionDocument.frame.origin.x - collectionDocument.contentOffset.x , y: (attributes?.center.y)! + collectionDocument.frame.origin.y - collectionDocument.contentOffset.y)
        
        if var theSuperView = collectionDocument.superview {
            tempView.center = CGPoint(x: tempView.center.x + theSuperView.frame.origin.x, y: tempView.center.y + theSuperView.frame.origin.y)
            while let anotherSuperView = theSuperView.superview {
                tempView.center = CGPoint(x: tempView.center.x + anotherSuperView.frame.origin.x, y: tempView.center.y + anotherSuperView.frame.origin.y)
                theSuperView = anotherSuperView
            }
        }
        
        return tempView.frame
    }
}


extension SNDetailWorkVC  {
    @objc fileprivate func panGestureActionTest (_ gestureRecognizer : UIPanGestureRecognizer) {
        
        if !isPanAllowed || !customImgView.isDescendant(of: self.view) {
            collectionTestResult.isScrollEnabled = true
            return
        }
        
        let translation = gestureRecognizer.translation(in: self.view)
        collectionTestResult.isScrollEnabled = false
        
        if gestureRecognizer.state == .began  {
            startFrame = customImgView.frame
        }
        else if gestureRecognizer.state == .changed {
            
            checkWhetherProductMovedDestinationTest()
            customImgView.center = CGPoint(x: customImgView.center.x + translation.x, y: customImgView.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            
        }
        else if gestureRecognizer.state == .ended {
            caseMethodForPanCompletionWithTest(gestureRecognizer)
        }
    }
    
    private func caseMethodForPanCompletionWithTest(_ gestureRecognizer:UIPanGestureRecognizer) {
        
        
        if indexPathForItem == nil || !customImgView.isDescendant(of: self.view) {
            return
        }
        
        let velocity = gestureRecognizer.velocity(in: self.view)
        
        let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
        let slideMultiplier = magnitude / 200
        print("magnitude: \(magnitude), slideMultiplier: \(slideMultiplier)")
        
        let slideFactor = 0.1 * slideMultiplier     //Increase for more of a slide
        
        var finalPoint = CGPoint(x:customImgView.center.x + (velocity.x * slideFactor),
                                 y:customImgView.center.y + (velocity.y * slideFactor))
        finalPoint.x = min(max(finalPoint.x, 0), self.view.bounds.size.width)
        finalPoint.y = min(max(finalPoint.y, 0), self.view.bounds.size.height)
        
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: { [weak self] in
                        self?.customImgView.center = finalPoint
                        self?.checkWhetherProductMovedDestinationTest()
            },
                       completion: { [weak self] _ in
                        
                        self?.panCompletionMethodTest()
        })
        collectionTestResult.isScrollEnabled = true
    }
    
    fileprivate func panCompletionMethodTest() {
        
        if self.isReachedDest {
            UIView.animate(withDuration: 0.2, animations: { [weak weakSelf = self] in
                let destionViewCenterWithRespectMain = weakSelf?.getTheMainViewBasedCenterForDestinationTest()
                weakSelf?.customImgView.frame = CGRect(x: (destionViewCenterWithRespectMain?.x)! - 5, y: (destionViewCenterWithRespectMain?.y)! - 5, width: 10.0, height: 10.0)
                }, completion:{  [weak weakSelf = self] _ in
                    
                    weakSelf?.doSomeThingsAfterPanCompletionTest()
                    self.deleteImgTest(index: self.indexRemove)
            })
        }
        else {
            UIView.animate(withDuration: 0.2, animations: { [weak weakSelf = self] in
                weakSelf?.customImgView.frame = (weakSelf?.constructFrameForIndexTest(indexPath: (weakSelf?.indexPathForItem)!))!
                }, completion:{  [weak weakSelf = self] _ in
                    weakSelf?.doSomeThingsAfterPanCompletionTest()})
        }
        
    }
    
    fileprivate func checkWhetherProductMovedDestinationTest() {
        isPanRunning = true
        
        let draggingViewX = customImgView.frame.origin.x + customImgView.frame.size.width
        let draggingViewY = customImgView.frame.origin.y
        
        let destionViewCenterWithRespectMain = getTheMainViewBasedCenterForDestinationTest()
        
        let requiredDestFrame = CGRect(x: destionViewCenterWithRespectMain.x - deleteView.frame.width/2, y: destionViewCenterWithRespectMain.y - deleteView.frame.height/2, width: deleteView.frame.size.width, height: deleteView.frame.size.height)
        
        isReachedDest = false
        if (draggingViewY <= requiredDestFrame.origin.y + deleteView.frame.size.height && draggingViewX >= requiredDestFrame.origin.x)  {
            isReachedDest = true
        }
    }
    
    fileprivate func getTheMainViewBasedCenterForDestinationTest() -> CGPoint {
        var destionViewCenterWithRespectMain = deleteView.center
        
        if var destinationSuperView = deleteView.superview {
            destionViewCenterWithRespectMain = CGPoint(x: destionViewCenterWithRespectMain.x + destinationSuperView.frame.origin.x, y: destionViewCenterWithRespectMain.y + destinationSuperView.frame.origin.y)
            while let anotherSuperView = destinationSuperView.superview {
                destionViewCenterWithRespectMain = CGPoint(x: destionViewCenterWithRespectMain.x + anotherSuperView.frame.origin.x, y: destionViewCenterWithRespectMain.y + anotherSuperView.frame.origin.y)
                destinationSuperView = anotherSuperView
            }
        }
        return destionViewCenterWithRespectMain
    }
    
    @objc fileprivate func longPressGestureActionTest(_ gestureRecognizer : UILongPressGestureRecognizer) {
        
        if isPanRunning {
            return
        }
        
        if gestureRecognizer.state == .began {
            
            let point = gestureRecognizer.location(in: collectionTestResult)
            print(point)
            
            guard let indexPathToCheck = collectionTestResult.indexPathForItem(at: point) else {
                return
            }
            if customImgView.isDescendant(of: self.view) {
                customImgView.removeFromSuperview()
            }
            isPanAllowed = true
            
            print("IndexPath : \(indexPathToCheck)")
            indexRemove = indexPathToCheck.row
            print(indexPathToCheck.row)
            
            addCustomViewAtTest(point)
        }
        else if gestureRecognizer.state == .ended {
            
            isPanAllowed = false
            
            if customImgView.isDescendant(of: view) {
                customImgView.removeFromSuperview()
            }
            collectionTestResult.isScrollEnabled = true
        }
    }
    
    
    fileprivate func addCustomViewAtTest(_ point:CGPoint) {
        
        guard let indexPath = collectionTestResult.indexPathForItem(at: point) else {return}
        
        if customImgView.isDescendant(of: self.view) {
            print("Nil Indexpath or already view available>>>>>>> Returned")
            return
        }
        
        indexPathForItem = indexPath as IndexPath
        
        var newArray:[Any] = []
        var newIndex:Int = 0
        var checkData = ""
        if indexPath.row == 0 {
            newArray = dataImgAC
            newIndex = indexPath.row
            checkData = newArray[0] as! String
        }
        else if indexPath.row == 1 {
            newArray = dataImgEO
            newIndex = indexPath.row - 1
            checkData = newArray[0] as! String
        }
        /*
        else if indexPath.row == 3 || indexPath.row == 4 {
            newArray = dataImgEO
            newIndex = indexPath.row - 3
            checkData = newArray[0] as! String
        }
         */
        
        if checkData != "" && newIndex < newArray.count {
            customImgView = UIImageView.init(frame:constructFrameForIndexTest(indexPath: indexPath))
            customImgView.contentMode = .scaleToFill
            
            let itemAtIndexPath = newArray[(newIndex)]
            //customImgView.image = UIImage.init(named: itemAtIndexPath.imageName)
            SNCommon.getImageDirectory(imgView: customImgView, nameImg: itemAtIndexPath)
            customImgView.backgroundColor = UIColor.clear
            
            startFrame = customImgView.frame
            self.view.addSubview(customImgView)
        }
    }
    
    
    //MARK: - All The Gesture Methods
    
    fileprivate func doSomeThingsAfterPanCompletionTest() {
        
        if customImgView.isDescendant(of: view) {
            customImgView.removeFromSuperview()
        }
        isReachedDest = false
        isPanRunning = false
        isPanAllowed = false
    }
    
    fileprivate func constructFrameForIndexTest(indexPath:IndexPath)->CGRect {
        
        let attributes = collectionTestResult.layoutAttributesForItem(at: indexPath as IndexPath)
        let sizeRect = attributes?.frame
        
        let tempView = UIView.init(frame: CGRect(x: 0, y: 0, width: (sizeRect?.size.width)! + 20.0, height: (sizeRect?.size.height)! + 20.0))
        tempView.center = CGPoint(x: (attributes?.center.x)! + collectionTestResult.frame.origin.x - collectionTestResult.contentOffset.x , y: (attributes?.center.y)! + collectionTestResult.frame.origin.y - collectionTestResult.contentOffset.y)
        
        if var theSuperView = collectionTestResult.superview {
            tempView.center = CGPoint(x: tempView.center.x + theSuperView.frame.origin.x, y: tempView.center.y + theSuperView.frame.origin.y)
            while let anotherSuperView = theSuperView.superview {
                tempView.center = CGPoint(x: tempView.center.x + anotherSuperView.frame.origin.x, y: tempView.center.y + anotherSuperView.frame.origin.y)
                theSuperView = anotherSuperView
            }
        }
        
        return tempView.frame
    }
}
