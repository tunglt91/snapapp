//
//  SNCameraDocumentVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreData

protocol SNCameraDocumentDelegate: class {
    func cameraButtonDidToggle(arrayData: [Any])
}

class SNCameraDocumentVC: UIViewController {
    @IBOutlet weak var cameraView: UIView!
    
    var delegate:SNCameraDocumentDelegate?
    
    var arrayImgDocument:[Any] = []
    var dataWork:ModelWork = ModelWork()
    var imgCapture:UIImage = UIImage()
    var indexArray:Int = 0
    var oldImgFirst:String = ""
    
    
    // Capture
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //tên ảnh cũ vị trí số 0
        oldImgFirst = arrayImgDocument[0] as! String
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
    }
    
    //MARK: - IBAction
    @IBAction func cameraPress(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Func
    func setupView() {
        self.setupCapture(cameraView: cameraView)
    }
    
    func saveImageToDocument() {
        // lưu ảnh vào folder device
        let date :Date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'_'HH_mm_ss"
        //        dateFormatter.timeZone = NSTimeZone(name: "GMT")! as TimeZone
        
        let imageName = "TL_\(dateFormatter.string(from: date)).jpg"
        print(imageName)
        
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("/Snapp Album/\(imageName)")
        //let image = sceneView.snapshot()
        let image = self.imgCapture
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        
        // lưu tên ảnh vào coredata
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        if indexArray == 0 {
            arrayImgDocument.remove(at: indexArray)
            arrayImgDocument.insert(imageName, at: indexArray)
        }else {
            let checkData = arrayImgDocument[1] as! String
            if checkData.isEmpty {
                arrayImgDocument.removeAll()
                arrayImgDocument = [oldImgFirst]
            }
            arrayImgDocument.append(imageName)
        }
        
        dataWork.setValue(arrayImgDocument, forKey: "doc_rec")
        
        do {
            try context.save()
            self.delegate?.cameraButtonDidToggle(arrayData: arrayImgDocument)
            self.navigationController?.popViewController(animated: true)
        } catch {
            print("Failed saving")
        }
    }
    
}

extension SNCameraDocumentVC: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation() {
            self.imgCapture = UIImage(data: imageData)!
            self.saveImageToDocument()
        }
    }
}

