//
//  CustomSettingView.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

enum TypeCustom {
    case TypeScan
    case Number
    case YesOrNo
    case VaporPressure
    case Temperature
    case SterilizationTime
    case GoodOrBad
    case IncreaseOrReduction
    case Part
    case TechnicalEquipment
}

protocol CustomSettingDelegate: class {
    func customChangeButtonDidToggle(strContent: String)
}

class CustomSettingView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var delegate:CustomSettingDelegate?
    
    var arrayPicker:[String] = []
    var typeCustom:TypeCustom = .TypeScan
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomSettingView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }

}

//MARK: PickerView Delegate
extension CustomSettingView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayPicker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch typeCustom {
        case .TypeScan:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .Number:
            self.delegate?.customChangeButtonDidToggle(strContent: "\(arrayPicker[row])回目")
            break
        case .YesOrNo:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .VaporPressure:
            if arrayPicker[row] == "" {
                self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            } else {
                self.delegate?.customChangeButtonDidToggle(strContent: "\(arrayPicker[row]) MPa")
            }
            break
        case .Temperature:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .SterilizationTime:
            if arrayPicker[row] == "" {
                self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            } else {
                self.delegate?.customChangeButtonDidToggle(strContent: "\(arrayPicker[row]) 分")
            }
            break
        case .GoodOrBad:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .IncreaseOrReduction:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .Part:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        case .TechnicalEquipment:
            self.delegate?.customChangeButtonDidToggle(strContent: arrayPicker[row])
            break
        }
    }
}
