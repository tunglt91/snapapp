//
//  DateSettingView.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/7/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol DateSettingDelegate: class {
    func dateChangeButtonDidToggle(strDate: String, strYear: Int, strMonth: Int, strDay: Int, strHour: Int, strMinute: String)
}


class DateSettingView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var delegate:DateSettingDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DateSettingView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: datePicker.date)
        let month = calendar.component(.month, from: datePicker.date)
        let day = calendar.component(.day, from: datePicker.date)
        let hour = calendar.component(.hour, from: datePicker.date)
        let minute = calendar.component(.minute, from: datePicker.date)
        let minuteFormat = String(format: "%02d", minute)
        
        let dateJapan = "\(year)年\(month)月\(day)日"
        
        self.delegate?.dateChangeButtonDidToggle(strDate: dateJapan, strYear: year, strMonth: month, strDay: day, strHour: hour, strMinute: minuteFormat)
    }
    

}
