//
//  SNDetailWorkCollection_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UICollectionViewDataSource
extension SNDetailWorkVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionDocument {
            return 7
        }else {
            return 9
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionDocument {
            let cell:DetailWorkCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailWorkCell", for: indexPath) as! DetailWorkCell
            
            cell.imgDetail.contentMode = .scaleAspectFit
            cell.tag = indexPath.row
            cell.lbTitle.text = arrayDocNil[indexPath.row]
            
            if indexPath.row == 0 {
                cell.imgCamera.image = UIImage.init(named: "documentnew")
            }
            else
            {
                cell.imgCamera.image = UIImage.init(named: "cameranew")
            }
            
            if indexPath.row == 0 {
                //cell.imgCamera.image = UIImage.init(named: "documentnew")
                cell.imgDetail.image = nil
                cell.viewImgNil.isHidden = false
            }else {
                if indexPath.row - 1 >= dataImgDocument.count {
                    cell.imgDetail.image = nil
                    cell.viewImgNil.isHidden = false
                }else {
                    let imgName = dataImgDocument[indexPath.row - 1]
                    print(imgName)
                    
                    if indexPath.row == 1 {
                        if (imgName as! String) == "noImg" {
                            cell.viewImgNil.isHidden = false
                        }else {
                            cell.viewImgNil.isHidden = true
                        }
                    }else {
                        if (imgName as! String) == "" {
                            cell.viewImgNil.isHidden = false
                        }else {
                            cell.viewImgNil.isHidden = true
                        }
                    }
                    
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                }
            }

            cell.viewCondition.isHidden = true
        
            return cell
        }else {
            let cell:DetailWorkCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailWorkCell", for: indexPath) as! DetailWorkCell
            cell.constraintTopImgCamera.constant = 13
            cell.lbTitle.text = arrayScan[indexPath.row]
            if indexPath.row == 0 {
                cell.tag = indexPath.row
                //cell.lbTitle.text = arrayACNil[indexPath.row]
                
                if indexPath.row >= dataImgAC.count {
                    cell.imgDetail.image = nil
                }else {
                    cell.imgDetail.contentMode = .scaleAspectFit
                    let imgName = dataImgAC[indexPath.row]
                    
                    if (imgName as! String) == "noImg" {
                        cell.viewImgNil.isHidden = false
                    }else {
                        cell.viewImgNil.isHidden = true
                    }
                    
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                    
                    let checkCondition = dataConditionAC[indexPath.row] as! Int
                    if checkCondition == 0 {
                        cell.viewCondition.isHidden = true
                    }else {
                        cell.viewCondition.isHidden = false
                    }
                }
            }
            else if indexPath.row == 1 {
                cell.tag = indexPath.row - 1
                //cell.lbTitle.text = arrayEONil[indexPath.row - 1]
                
                if indexPath.row - 1 >= dataImgEO.count {
                    cell.imgDetail.image = nil
                }else {
                    cell.imgDetail.contentMode = .scaleAspectFit
                    
                    let imgName = dataImgEO[indexPath.row - 1]
                    
                    if (imgName as! String) == "noImg" {
                        cell.viewImgNil.isHidden = false
                    }else {
                        cell.viewImgNil.isHidden = true
                    }
                    
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                    
                    let checkCondition = dataConditionEO[indexPath.row - 1] as! Int
                    if checkCondition == 0 {
                        cell.viewCondition.isHidden = true
                    }else {
                        cell.viewCondition.isHidden = false
                    }
                }
            }
            else {
                cell.imgDetail.image = nil
                cell.viewCondition.isHidden = true
                cell.viewImgNil.isHidden = false
            }
                /*
            else if indexPath.row == 3 || indexPath.row == 4 {
                cell.tag = indexPath.row - 3
                cell.lbTitle.text = arrayEONil[indexPath.row - 3]
                
                if indexPath.row - 3 >= dataImgEO.count {
                    cell.imgDetail.image = nil
                }else {
                    let imgName = dataImgEO[indexPath.row - 3]
                    
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                }
            }
            else if indexPath.row == 5 || indexPath.row == 6 {
                cell.imgDetail.image = nil
                cell.lbTitle.text = arrayHydroNil[indexPath.row - 5]
            }
            else if indexPath.row == 7 || indexPath.row == 8 {
                cell.imgDetail.image = nil
                cell.lbTitle.text = arrayFormalinNil[indexPath.row - 7]
            }
             */
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == collectionDocument {
            if indexPath.row == 0 {
                /*
                if (dataImgDocument[0] as! String) == "noImg" {
                    self.showCameraDocumentVC(listDataImage: dataImgDocument, indexArray: indexPath.row)
                }else {
                    self.showZoomImg(nameImg: dataImgDocument[indexPath.row], zoomType: .Doc)
                }
                 */
                let storyboard = UIStoryboard(name: "Employee", bundle: nil)
                let listSterilizationVC = storyboard.instantiateViewController(withIdentifier: "SNListSterilizationVC") as? SNListSterilizationVC
                listSterilizationVC?.typeFromVC = .DetailWorkVC
                listSterilizationVC?.curSterilization = (dataModel.value(forKey: "sterilization") as! SterilizationModel)
                listSterilizationVC?.dataModel = self.dataModel
                self.navigationController?.pushViewController(listSterilizationVC!, animated: true)
            }
            else if indexPath.row == 1 {
                if (dataImgDocument[0] as! String) == "noImg" {
                    self.showCameraDocumentVC(listDataImage: dataImgDocument, indexArray: indexPath.row - 1)
                }else {
                    self.showZoomImg(nameImg: dataImgDocument[indexPath.row - 1], zoomType: .Doc)
                }
            }
            else {
                //Check Data có phải mới khởi tạo không
                let checkData = dataImgDocument[1] as! String
                if checkData.isEmpty || indexPath.row - 1 >= dataImgDocument.count {
                    self.showCameraDocumentVC(listDataImage: dataImgDocument, indexArray: indexPath.row - 1)
                }else {
                    self.showZoomImg(nameImg: dataImgDocument[indexPath.row - 1], zoomType: .Doc)
                }
            }
        }else {
            if indexPath.row == 0 {
                /*
                //Check Data có phải mới khởi tạo không
                let checkData = dataImgAC[0] as! String
                if checkData.isEmpty || indexPath.row >= dataImgAC.count {
                    self.showCameraTestResultVC(listDataImage: dataImgAC, indexArray: indexPath.row)
                }
                 */
                let checkData = dataImgAC[indexPath.row] as! String
                if checkData == "noImg" {
                    self.showCameraTestResultVC(listDataImage: dataImgAC, indexArray: indexPath.row, selectionFlag: 0)
                }else {
                    self.showZoomImg(nameImg: dataImgAC[indexPath.row], zoomType: .AO)
                }
            }else if indexPath.row == 1 {
                /*
                //Check Data có phải mới khởi tạo không
                let checkData = dataImgEO[0] as! String
                if checkData.isEmpty || indexPath.row - 3 >= dataImgEO.count {
                    self.showCameraTestResultVC(listDataImage: dataImgEO, indexArray: indexPath.row - 3)
                }
                 */
                let checkData = dataImgEO[indexPath.row - 1] as! String
                if checkData == "noImg" {
                    self.showCameraTestResultVC(listDataImage: dataImgEO, indexArray: indexPath.row - 1, selectionFlag: 1)
                }else {
                    self.showZoomImg(nameImg: dataImgEO[indexPath.row - 1], zoomType: .EO)
                }
            }
        }
    }
    
    func showZoomImg(nameImg: Any, zoomType: ZoomScanType) {
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let zoomVC = storyboard.instantiateViewController(withIdentifier: "SNZoomImgVC") as? SNZoomImgVC
        zoomVC?.nameImg = nameImg
        zoomVC?.zoomType = zoomType
        self.navigationController?.pushViewController(zoomVC!, animated: true)
    }
}


/*
extension SNDetailWorkVC: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if indexPath.row >= dataImgDocument.count {
            return []
        }else {
            let dragItem = self.dragItem(forPhotoAt: indexPath)
            return [dragItem]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        if indexPath.row >= dataImgDocument.count {
            return []
        }else {
            let dragItem = self.dragItem(forPhotoAt: indexPath)
            return [dragItem]
        }
    }
    
    /// Helper method
    private func dragItem(forPhotoAt indexPath: IndexPath) -> UIDragItem {        
            let imageName = dataImgDocument[indexPath.row]
            let itemProvider = NSItemProvider(object: imageName as! NSItemProviderWriting)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = imageName
            return dragItem
    }
}
 */
