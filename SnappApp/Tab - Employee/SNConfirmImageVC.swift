//
//  SNConfirmImageVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

 enum ScanType {
    case Flag_1233
    case Flag_1250
}

@objc protocol SNConfirmImageDelegate: class {
    func dismissButtonDidToggle()
    func closeUpButtonDidToggle(arrayData: [Any], selectionFlag: Int, dataCheckCondition: [Any])
}

class SNConfirmImageVC: UIViewController {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewTouch: UIView!
    @IBOutlet weak var btnCloseUp: UIButton!
    @IBOutlet weak var lbNoti: UILabel!
    @IBOutlet weak var imgNoti: UIImageView!
    
    @objc var delegate:SNConfirmImageDelegate?
    var scanType:ScanType = .Flag_1233
    @objc var selectionFlag:Int = 0
    
    @objc var snapShot:UIImage = UIImage()
    @objc var arrayImg:[Any] = []
    @objc var indexArray:Int = 0
    @objc var typeNoti:Int = 1
    /*
    @objc var arrayImgEO:[Any] = []
    var arrayImgHydro:[Any] = []
    var arrayImgFormalin:[Any] = []
     */
    @objc var dataWork:ModelWork = ModelWork()
    var dataCheckCondition:[Any] = []
    var checkCondition:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
        
        /*
        //Check Data có phải mới khởi tạo không
            let checkData = arrayImg[0] as! String
            if checkData.isEmpty {
                arrayImg.removeAll()
                arrayImg = []
            }
         */
    }

    //MARK: - IBAction
    @IBAction func backPress(_ sender: Any) {
        self.dismiss()
    }
    
    @IBAction func closeUpPress(_ sender: Any) {
        if typeNoti == 3 {
            self.delegate?.dismissButtonDidToggle()
        }else {
             self.saveImageToDocument()
        }
    }
    
    /*
    @objc func touchViewPress(_ sender:UITapGestureRecognizer){
        self.viewTouch.isHidden = false
    }
     */
    
    //MARK: - Func
    func setupView() {
        self.btnCloseUp.layer.cornerRadius = 30
        self.btnCloseUp.layer.masksToBounds = true
        
        /*
        self.viewTouch.isHidden = true
        
        let gestureTouch = UITapGestureRecognizer(target: self, action:  #selector (self.touchViewPress (_:)))
        self.viewContent.addGestureRecognizer(gestureTouch)
         */
        
        if selectionFlag == 0 {
            dataCheckCondition = dataWork.value(forKey: "check_condition_ac") as! [Any]
        }else {
            dataCheckCondition = dataWork.value(forKey: "check_condition_eo") as! [Any]
        }
        
        if self.typeNoti == 1 {
            self.lbNoti.text = "合格"
            self.lbNoti.textColor = UIColor.init(hexString: "448DE2")
            checkCondition = 1
        }
        else if self.typeNoti == 2 {
            self.lbNoti.text = "不合格"
            self.lbNoti.textColor = UIColor.red
            self.imgNoti.image = UIImage.init(named: "ico_X")
            checkCondition = 0
        }
        else if self.typeNoti == 3 {
            self.lbNoti.text = "再度試してください。"
            self.lbNoti.textColor = UIColor.red
            self.imgNoti.image = UIImage.init(named: "ico_X")
            self.btnCloseUp.setTitle("撮影する", for: .normal)
            checkCondition = 0
        }
    }
    
    @objc func showInVC(vc: UIViewController){
        vc.addChildViewController(self)
        vc.view.addSubview(self.view)
        self.didMove(toParentViewController: vc)
    }
    
    func dismiss() {
        self.willMove(toParentViewController: nil)
        self.view .removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func saveImageToDocument() {
        // lưu ảnh vào folder device
        let date :Date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'_'HH_mm_ss"
        
        var imageName = ""
        if selectionFlag == 0 {
            imageName = "AC_\(dateFormatter.string(from: date)).jpg"
        }else {
            imageName = "EO_\(dateFormatter.string(from: date)).jpg"
        }
        
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("/Snapp Album/\(imageName)")
        let image = snapShot
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        
        // lưu tên ảnh vào coredata
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        //arrayImg.append(imageName)
        arrayImg.remove(at: indexArray)
        arrayImg.insert(imageName, at: indexArray)
        
        //Save dieu kien
        dataCheckCondition.remove(at: indexArray)
        dataCheckCondition.insert(checkCondition, at: indexArray)
        
        // Flag 1233
        if selectionFlag == 0 {
            dataWork.setValue(arrayImg, forKey: "results_rec_ac")
            dataWork.setValue(dataCheckCondition, forKey: "check_condition_ac")
        // Flag 1250
        }else {
            dataWork.setValue(arrayImg, forKey: "results_rec_eo")
            dataWork.setValue(dataCheckCondition, forKey: "check_condition_eo")
        }
        
       
        do {
            try context.save()
            self.delegate?.closeUpButtonDidToggle(arrayData: arrayImg, selectionFlag: selectionFlag, dataCheckCondition: dataCheckCondition)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        } catch {
            print("Failed saving")
        }
    }

}
