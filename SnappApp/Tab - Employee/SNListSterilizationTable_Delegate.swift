//
//  SNListSterilizationTable_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation

extension SNListSterilizationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ListJobSectionCell") as! ListJobSectionCell
        headerView.constraintLeadingLine.constant = 38
        headerView.constraintTrailingLine.constant = 38
        headerView.lbSection.text = self.arrSection[section]
        if section == 0 {
            headerView.lbSection.font = UIFont.systemFont(ofSize: 28, weight: .regular)
            headerView.lineView.isHidden = true
        }
        else {
            headerView.lbSection.font = UIFont.systemFont(ofSize: 20, weight: .regular)
            headerView.lineView.isHidden = false
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSec1.count + 1
        }
        else if section == 1 {
            return arrSec2.count
        }
        else if section == 2 {
            return 4
        }
        else if section == 3 {
            return arrSec4.count + 1
        }
        else {
            return curListDevice.count + 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 {
            if indexPath.row < arrSec1.count {
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 9 || indexPath.row == 10 || indexPath.row == 12 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell1") as! ListSterilizationCell1
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    cell.tag = indexPath.row
                    cell.tfContent.delegate = self
                    cell.tfContent.tag = indexPath.row
                    cell.lbTitle.text = arrSec1[indexPath.row]
                    cell.curInfo = self.curInfo
                    return cell
                } else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 11 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell4") as! ListSterilizationCell4
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    cell.tag = indexPath.row
                    cell.delegate = self
                    cell.lbContent.tag = indexPath.row
                    cell.row = indexPath.row
                    cell.section = indexPath.section
                    cell.lbTitle.text = arrSec1[indexPath.row]
                    cell.curInfo = self.curInfo
                    return cell
                }
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell2") as! ListSterilizationCell2
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.lbTitle.text = "滅菌記録紙"
                cell.lbTitleCamera.text = "滅菌器記録紙\n画像"
                
                if dataImgDocument.count > 0 {
                    let imgName = dataImgDocument[0]
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                    cell.imgDetail.isHidden = false
                    if imgName as! String != "noImg" {
                        cell.viewCamera.isHidden = true
                    }
                }
                return cell
            }
            return UITableViewCell()
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell4") as! ListSterilizationCell4
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.tag = indexPath.row
            cell.row = indexPath.row
            cell.section = indexPath.section
            cell.delegate = self
            cell.lbContent.tag = indexPath.row
            cell.lbTitle.text = arrSec2[indexPath.row]
            cell.curPhysical = self.curPhysical
            return cell
        }
        else if indexPath.section == 2 {
            if indexPath.row == 0 || indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell4") as! ListSterilizationCell4
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tag = indexPath.row
                cell.row = indexPath.row
                cell.section = indexPath.section
                cell.delegate = self
                cell.lbContent.tag = indexPath.row
                if indexPath.row == 0 {
                    cell.lbTitle.text = "BDテスト"
                }else {
                    cell.lbTitle.text = "インジケーター"
                }
                cell.curChemical = self.curChemical
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell2") as! ListSterilizationCell2
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.lbTitleCamera.text = "インジケーター\n画像"
                if indexPath.row == 1 {
                    cell.lbTitle.text = "BDテスト"
                    if dataImgAC.count > 0 {
                        let imgName = dataImgAC[0]
                        SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                        cell.imgDetail.isHidden = false
                        if imgName as! String != "noImg" {
                            cell.viewCamera.isHidden = true
                        }
                    }
                }else {
                    cell.lbTitle.text = "化学的インジケーター"
                    if dataImgEO.count > 0 {
                        let imgName = dataImgEO[0]
                        SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                        cell.imgDetail.isHidden = false
                        if imgName as! String != "noImg" {
                            cell.viewCamera.isHidden = true
                        }
                    }
                }
                
                return cell
            }
        }
        else if indexPath.section == 3 {           
            if indexPath.row < arrSec4.count{
                if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell4") as! ListSterilizationCell4
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    cell.tag = indexPath.row
                    cell.delegate = self
                    cell.lbContent.tag = indexPath.row
                    cell.row = indexPath.row
                    cell.section = indexPath.section
                    cell.lbTitle.text = arrSec4[indexPath.row]
                    cell.curBiologica = self.curBiologica
                    return cell
                } else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell1") as! ListSterilizationCell1
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    cell.tag = indexPath.row
                    cell.tfContent.delegate = self
                    cell.tfContent.tag = indexPath.row
                    cell.lbTitle.text = arrSec4[indexPath.row]
                    cell.curBiologica = self.curBiologica
                    return cell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell2") as! ListSterilizationCell2
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.lbTitle.text = "生物学的モニタリング"
                cell.lbTitleCamera.text = "インジケーター\n画像"
                cell.viewCamera.isHidden = false
                cell.imgDetail.isHidden = true
                return cell
            }
            return UITableViewCell()
        }else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell5") as! ListSterilizationCell5
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                if dataImgDocument.count > 0 {
                    var data = dataImgDocument
                    data.remove(at: 0)
                    cell.arrayImg = data
                }
                return cell
            }
            else if indexPath.row == curListDevice.count + 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TotalBottomCell") as! TotalBottomCell
                cell.delegate = self
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                switch typeFromVC {
                case .ListJobVC:
                    cell.btnCloseUp.setTitle("登録", for: .normal)
                case .DetailWorkVC:
                    cell.btnCloseUp.setTitle("閉じる", for: .normal)
                }
                cell.btnCloseUp.backgroundColor = UIColor.init(hexString: "448DE2")
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListSterilizationCell3", for: indexPath) as! ListSterilizationCell3
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.tfPart.delegate = self
                cell.tfTechnicalEquipment.delegate = self
                cell.tfAmount.delegate = self
                cell.delegate = self
                if indexPath.row == 1 {
                    cell.lineHoz1.isHidden = true
                    cell.lineHoz2.isHidden = true
                    cell.buttonAdd.isHidden = true
                    cell.tfPart.isUserInteractionEnabled = false
                    cell.tfTechnicalEquipment.isUserInteractionEnabled = false
                    cell.tfAmount.isUserInteractionEnabled = false
                    cell.tfPart.text = "部署"
                    cell.tfTechnicalEquipment.text = "使用器材"
                    cell.tfAmount.text = "数量"
                    cell.tfPart.isHidden = false
                    cell.tfTechnicalEquipment.isHidden = false
                    cell.tfAmount.isHidden = false
//                    cell.tfPart.textColor = UIColor(named: "#848484")
                }
                else if indexPath.row == curListDevice.count + 2 {
                    cell.lineHoz1.isHidden = true
                    cell.lineHoz2.isHidden = true
                    cell.buttonAdd.isHidden = false
                    cell.tfTechnicalEquipment.isHidden = true
                    cell.tfAmount.isHidden = true
                    cell.tfPart.isHidden = false
                    cell.tfPart.text = "追加"
                    cell.tfPart.textColor = UIColor.lightGray
                    cell.tfPart.isUserInteractionEnabled = false
                }
                else {
                    cell.buttonAdd.isHidden = true
                    let device = curListDevice[indexPath.row - 2]
                    cell.tfPart.text = device.part
                    cell.tfTechnicalEquipment.text = device.technical_equipment
                    cell.tfAmount.text = device.amount
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                curListDevice.remove(at: indexPath.row - 2)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if indexPath.section == 4 {
            if indexPath.row > 1 && indexPath.row < curListDevice.count + 2 {
                return .delete
            }else {
                return .none
            }
        }else {
            return .none
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row < arrSec1.count {
                return 55
            }else {
                return 174
            }
        }
        else if indexPath.section == 2 {
            if indexPath.row == 0 || indexPath.row == 2 {
                return 55
            }else {
                return 174
            }
        }
        else if indexPath.section == 3 {
            if indexPath.row < arrSec4.count {
                return 55
            }else {
                return 174
            }
        }
        else if indexPath.section == 4 {
            if indexPath.row == 0 {
                return 230
            }
            else if indexPath.row == curListDevice.count + 3 {
                return 150
            }
            else {
                return 55
            }
        }
        else {
            return 55
        }
    }
}
