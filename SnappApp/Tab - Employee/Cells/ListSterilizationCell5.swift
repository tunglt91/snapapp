//
//  ListSterilizationCell5.swift
//  SnappApp
//
//  Created by Tu Ngo on 11/17/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class ListSterilizationCell5: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrayImg:[Any] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let nib = UINib(nibName: "DetailWorkCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "DetailWorkCell")
        self.setupGirdView()
    }
    
    func setupGirdView() {
        //let width: CGFloat = (self.collectionDocument.frame.width / 5) - 26
        let width: CGFloat = 134
        
        let flowDocument = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        flowDocument?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowDocument?.itemSize = CGSize(width: width, height: width)
        flowDocument?.minimumLineSpacing = 8
        flowDocument?.minimumInteritemSpacing = 8
        //        flowDocument?.scrollDirection = .vertical
        collectionView?.isPagingEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}


//MARK: - Collection Delegate, datesoure
extension ListSterilizationCell5: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:DetailWorkCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailWorkCell", for: indexPath) as! DetailWorkCell
        cell.imgDetail.contentMode = .scaleAspectFit
        cell.viewCondition.isHidden = true
        cell.viewImgNil.backgroundColor = UIColor.clear
        cell.lbTitle.text = "器材記録\(indexPath.row + 1)"
        
        if indexPath.row >= arrayImg.count {
            
        }else {
            SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: arrayImg[indexPath.row])
        }
        return cell
    }
}

