//
//  ListJobSectionCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/21/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class ListJobSectionCell: UITableViewHeaderFooterView {
    @IBOutlet weak var lbSection: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var constraintTrailingLine: NSLayoutConstraint!
    @IBOutlet weak var constraintLeadingLine: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lbSection.textColor = UIColor.init(hexString: "848484")
    }

}
