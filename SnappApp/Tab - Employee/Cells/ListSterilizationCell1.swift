//
//  ListSterilizationCell1.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class ListSterilizationCell1: UITableViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfContent: UITextField!
    
    var curInfo:InfoModel? {
        didSet {
            self.updateInfoData()
        }
    }
    
    var curPhysical:PhysicalModel? {
        didSet {
            self.updatePhysicalData()
        }
    }
    
    var curChemical:ChemicalModel? {
        didSet {
            self.updateChemicalData()
        }
    }
    
    var curBiologica:BiologicalModel? {
        didSet {
            self.updateBiologicaData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateInfoData() {
        if self.tag == 0 {
            self.tfContent.text = curInfo?.entrant
        }
        else if self.tag == 1 {
            self.tfContent.text = curInfo?.responsiblePerson
        }
        else if self.tag == 2 {
            self.tfContent.text = curInfo?.dateOfWork
        }
        else if self.tag == 3 {
            self.tfContent.text = curInfo?.useSterilizer
        }
        else if self.tag == 4 {
            self.tfContent.text = "\(curInfo!.sterilizerNumber)"
        }
        else if self.tag == 5 {
            self.tfContent.text = "\(curInfo!.operationFrequency)"
        }
        else if self.tag == 6 {
            self.tfContent.text = curInfo?.startTime
        }
        else if self.tag == 7 {
            self.tfContent.text = curInfo?.endTime
        }
        else if self.tag == 8 {
            self.tfContent.text = curInfo?.ci_indicator_check
        }
        else if self.tag == 9 {
            self.tfContent.text = curInfo?.ci_BDtest
        }
        else if self.tag == 10 {
            self.tfContent.text = curInfo?.ci_Card
        }
        else if self.tag == 11 {
            self.tfContent.text = curInfo?.bi_indicator_check
        }
        else if self.tag == 12 {
            self.tfContent.text = curInfo?.loadNo
        }
    }
    
    func updatePhysicalData() {
        if self.tag == 0 {
            self.tfContent.text = "\(curPhysical!.vapor_pressure)"
        }
        else if self.tag == 1 {
            self.tfContent.text = curPhysical?.sterilization_temperature
        }
        else {
            self.tfContent.text = "\(curPhysical!.sterilization_time)"
        }
    }
    
    func updateChemicalData() {
        if self.tag == 0 {
            self.tfContent.text = curChemical?.bd_test
        }
        else if self.tag == 2 {
            self.tfContent.text = curChemical?.indicator
        }
    }
    
    func updateBiologicaData() {
        if self.tag == 0 {
            self.tfContent.text = curBiologica?.positive_control
        }
        else if self.tag == 1 {
            self.tfContent.text = curBiologica?.insertion_position
        }
        else if self.tag == 2 {
            self.tfContent.text = curBiologica?.judgment
        }
        else if self.tag == 3 {
            self.tfContent.text = curBiologica?.cultivation_start_time
        }
        else {
            self.tfContent.text = curBiologica?.result_check_time
        }
    }
    
}
