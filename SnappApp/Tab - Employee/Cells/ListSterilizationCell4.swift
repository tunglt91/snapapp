//
//  ListSterilization4.swift
//  SnappApp
//
//  Created by HoangTrungKien on 11/6/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

protocol ButtonDelegate: class {
    func Click (str: String, row: Int, section: Int)
}

class ListSterilizationCell4: UITableViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    var row = 0
    var section = 0
    var delegate: ButtonDelegate?
    
    @IBAction func btn_Click(_ sender: Any) {
        self.delegate?.Click(str: lbContent.text!, row: row, section: section)
    }
    
    
    var curInfo:InfoModel? {
        didSet {
            self.updateInfoData()
        }
    }
    
    var curPhysical:PhysicalModel? {
        didSet {
            self.updatePhysicalData()
        }
    }
    
    var curChemical:ChemicalModel? {
        didSet {
            self.updateChemicalData()
        }
    }
    
    var curBiologica:BiologicalModel? {
        didSet {
            self.updateBiologicaData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateInfoData() {
        if self.tag == 2 {
            self.lbContent.text = curInfo?.dateOfWork
        }
        else if self.tag == 3 {
            self.lbContent.text = curInfo?.useSterilizer
        }
        else if self.tag == 5 {
            self.lbContent.text = "\(curInfo!.operationFrequency)"
        }
        else if self.tag == 6 {
            self.lbContent.text = curInfo?.startTime
        }
        else if self.tag == 7 {
            self.lbContent.text = curInfo?.endTime
        }
        else if self.tag == 8 {
            self.lbContent.text = curInfo?.ci_indicator_check
        }
        else if self.tag == 11 {
            self.lbContent.text = curInfo?.bi_indicator_check
        }
    }
    
    func updatePhysicalData() {
        if self.tag == 0 {
            self.lbContent.text = "\(curPhysical!.vapor_pressure)"
        }
        else if self.tag == 1 {
            self.lbContent.text = curPhysical?.sterilization_temperature
        }
        else {
            self.lbContent.text = "\(curPhysical!.sterilization_time)"
        }
    }
    
    func updateChemicalData() {
        if self.tag == 0 {
            self.lbContent.text = curChemical?.bd_test
        }
        else if self.tag == 2 {
            self.lbContent.text = curChemical?.indicator
        }
    }
    
    func updateBiologicaData() {
        if self.tag == 0 {
            self.lbContent.text = curBiologica?.positive_control
        }
        else if self.tag == 2 {
            self.lbContent.text = curBiologica?.judgment
        }
        else if self.tag == 3 {
            self.lbContent.text = curBiologica?.cultivation_start_time
        }
        else if self.tag == 4{
            self.lbContent.text = curBiologica?.result_check_time
        }
    }
    
}
