//
//  ListJobCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/21/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class ListJobCell: UITableViewCell {
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbLotNumber: UILabel!
    @IBOutlet weak var lbNameOfSterilizer: UILabel!
    @IBOutlet weak var imgNew: UIImageView!
    
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataImgDocument:[Any] = []
    var dataImgAC:[Any] = []
    var dataImgEO:[Any] = []
    var dataImgHydro:[Any] = []
    var dataImgFormalin:[Any] = []
    var dataCheckConditionAC:[Any] = []
    var dataCheckConditionEO:[Any] = []
    
    // arrayCell Nil
    var arrayDocNil:[String] = ["滅菌器運転記録", "器材記録1", "器材記録2", "器材記録3", "器材記録4", "器材記録5"]
    var arrayACNil:[String] = ["AC \n BDテスト", "AC \n CIテスト", "AC \n BIテスト"]
    var arrayEONil:[String] = ["EO \n CIテスト", "EO \n BIテスト"]
    var arrayHydroNil:[String] = ["過酸化水素 \n CIテスト", "過酸化水素 \n BIテスト"]
    var arrayFormalinNil:[String] = ["ホルマリン \n CIテスト", "ホルマリン \n BIテスト"]
    
    var data:ModelWork? {
        didSet {
            dataImgDocument = data?.value(forKey: "doc_rec") as! [Any]
            dataImgAC = data?.value(forKey: "results_rec_ac") as! [Any]
            dataImgEO = data?.value(forKey: "results_rec_eo") as! [Any]
            dataImgHydro = data?.value(forKey: "results_rec_hydro") as! [Any]
            dataImgFormalin = data?.value(forKey: "results_rec_formalin") as! [Any]
            dataCheckConditionAC = data?.value(forKey: "check_condition_ac") as! [Any]
            dataCheckConditionEO = data?.value(forKey: "check_condition_eo") as! [Any]
            self.updateView()
        }
    }
    
    var tabType:TabType = .Tab1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let nib = UINib(nibName: "DetailWorkCell", bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: "DetailWorkCell")
            }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.collectionView.collectionViewLayout.invalidateLayout()
        self.cornerView.layer.cornerRadius = 6
        self.cornerView.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 6, scale: true)
        
        self.setupGirdView()
    }
    
    func setupGirdView() {
        let numberOfColumns: CGFloat = 5
        
        let width: CGFloat = ((self.collectionView.frame.width) / 5) - 8
        print(width)
        
        let flow = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        flow?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flow?.itemSize = CGSize(width: width, height: width)
        flow?.minimumLineSpacing = 8
        flow?.minimumInteritemSpacing = 8
        flow?.scrollDirection = .vertical
        collectionView?.isPagingEnabled = false
    }
    
    func updateView() {
        let strDate = data?.value(forKey: "date") as? String
        let date = parseDate(strDate!)
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)

        self.lbDate.text = "\(year)年\(month)月\(day)日"
        self.lbLotNumber.text = data?.value(forKey: "lot_num") as? String
        self.lbNameOfSterilizer.text = data?.value(forKey: "name_of_ster") as? String
    }
    
    func parseDate(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.date(from: str)!
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ListJobCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell:DetailWorkCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailWorkCell", for: indexPath) as! DetailWorkCell
        
        cell.imgCamera.isHidden = true
        cell.imgDetail.contentMode = .scaleAspectFit
        cell.constraintTopLbTitle.constant = -32
        // data ImgDocument
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 {
            cell.viewCondition.isHidden = true
            cell.lbTitle.text = arrayDocNil[indexPath.row]
            if indexPath.row >= dataImgDocument.count {
                cell.imgDetail.image = nil
                cell.viewImgNil.isHidden = false
            }else {
                let imgName = dataImgDocument[indexPath.row]
                print(imgName)
                
                if indexPath.row == 0 {
                    if (imgName as! String) == "noImg" {
                        cell.viewImgNil.isHidden = false
                    }else {
                        cell.viewImgNil.isHidden = true
                    }
                }else {
                    if (imgName as! String) == "" {
                        cell.viewImgNil.isHidden = false
                    }else {
                        cell.viewImgNil.isHidden = true
                    }
                }
                
                SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
            }
            self.checkTypeTab(cell: cell, data: dataImgDocument)
        }
        // data ImgAC
        else if indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 {
            cell.lbTitle.text = arrayACNil[indexPath.row - 6]
            if indexPath.row - 6 >= dataImgAC.count {
                cell.imgDetail.image = nil
            }else {
                if indexPath.row == 6 {
                    let imgName = dataImgAC[indexPath.row - 6]
                    print(imgName)
                    
                    if (imgName as! String) == "noImg" {
                        cell.viewImgNil.isHidden = false
                    }else {
                        cell.viewImgNil.isHidden = true
                    }
                    
                    SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                    
                    let checkCondition = dataCheckConditionAC[indexPath.row - 6] as! Int
                    print(checkCondition)
                    if checkCondition == 0 {
                        NSLog("index_row(0) : %d", indexPath.row)
                        cell.viewCondition.isHidden = true
                    }else {
                        NSLog("index_row(1) : %d", indexPath.row)
                        cell.viewCondition.isHidden = false
                    }
                }
                else if indexPath.row == 7 {
                    if indexPath.row - 7 >= dataImgEO.count {
                        cell.imgDetail.image = nil
                    }else {
                        let imgName = dataImgEO[indexPath.row - 7]
                        print(imgName)
                        
                        if (imgName as! String) == "noImg" {
                            cell.viewImgNil.isHidden = false
                        }else {
                            cell.viewImgNil.isHidden = true
                        }
                        
                        SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                        
                        let checkCondition = dataCheckConditionEO[indexPath.row - 7] as! Int
                        if checkCondition == 0 {
                            NSLog("index_row(0) : %d", indexPath.row)
                            cell.viewCondition.isHidden = true
                        }else {
                            NSLog("index_row(1) : %d", indexPath.row)
                            cell.viewCondition.isHidden = false
                        }
                    }
                }
                if indexPath.row == 8 {
                    cell.viewImgNil.isHidden = false
                    cell.viewCondition.isHidden = true
                }
            }
            self.checkTypeTab(cell: cell, data: dataImgAC)
        }
        // data ImgEO
        else if indexPath.row == 9 || indexPath.row == 10 {
            cell.viewImgNil.isHidden = false
            cell.viewCondition.isHidden = true
            cell.lbTitle.text = arrayEONil[indexPath.row - 9]
            /*
            if indexPath.row - 9 >= dataImgEO.count {
                cell.imgDetail.image = nil
            }else {
                let imgName = dataImgEO[indexPath.row - 9]
                print(imgName)
                
                if (imgName as! String) == "noImg" {
                    cell.viewImgNil.isHidden = false
                }else {
                    cell.viewImgNil.isHidden = true
                }
                
                SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
                
                let checkCondition = dataCheckConditionEO[indexPath.row - 9] as! Int
                if checkCondition == 0 {
                    cell.viewCondition.isHidden = true
                }else {
                    cell.viewCondition.isHidden = false
                }
            }
             */
            self.checkTypeTab(cell: cell, data: dataImgEO)
        }
        // data ImgHydro
        else if indexPath.row == 11 || indexPath.row == 12 {
            cell.viewImgNil.isHidden = false
            cell.viewCondition.isHidden = true
            cell.lbTitle.text = arrayHydroNil[indexPath.row - 11]
            if indexPath.row - 11 >= dataImgHydro.count {
                cell.imgDetail.image = nil
            }else {
                let imgName = dataImgHydro[indexPath.row - 11]
                print(imgName)
                
                SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
            }
            self.checkTypeTab(cell: cell, data: dataImgHydro)
        }
        // data ImgFormalin
        else if indexPath.row == 13 || indexPath.row == 14 {
            cell.viewImgNil.isHidden = false
            cell.viewCondition.isHidden = true
            cell.lbTitle.text = arrayFormalinNil[indexPath.row - 13]
            if indexPath.row - 13 >= dataImgFormalin.count {
                cell.imgDetail.image = nil
            }else {
                let imgName = dataImgFormalin[indexPath.row - 13]
                print(imgName)
                
                SNCommon.getImageDirectory(imgView: cell.imgDetail, nameImg: imgName)
            }
            self.checkTypeTab(cell: cell, data: dataImgFormalin)
        }
            return cell
 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func checkTypeTab(cell: DetailWorkCell, data:[Any]) {
        if tabType == .Tab1 || tabType == .Tab2 {
            //cell.backgroundColor = nil
            cell.viewImgNil.isHidden = true
        }else if tabType == .Tab3 {
            if cell.imgDetail.image == nil || data.count == 0 {
                //cell.backgroundColor = UIColor.init(hexString: "848484")
                cell.viewImgNil.isHidden = false
                cell.viewImgNil.backgroundColor = UIColor.init(hexString: "448DE2")
                cell.lbTitle.textColor = UIColor.white
            }
        }
    }
}
