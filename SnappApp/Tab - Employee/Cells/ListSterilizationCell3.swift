//
//  ListSterilizationCell3.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/26/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol ListSterilizationCell3Delegate: class {
    func addButtonDidToggle()
}

class ListSterilizationCell3: UITableViewCell {
    @IBOutlet weak var tfPart: UITextField!
    @IBOutlet weak var tfTechnicalEquipment: UITextField!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var lineHoz1: UIView!
    @IBOutlet weak var lineHoz2: UIView!
    
    var delegate:ListSterilizationCell3Delegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - IBAction
    @IBAction func addPress(_ sender: Any) {
        self.delegate?.addButtonDidToggle()
    }
    
    
}
