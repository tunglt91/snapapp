//
//  ListSterilizationCell2.swift
//  SnappApp
//
//  Created by Tu Ngo on 10/25/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol ListSterilizationCell2Delegate {
    func goToCamera()
}

class ListSterilizationCell2: UITableViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTitleCamera: UILabel!
    @IBOutlet weak var viewCamera: UIView!
    @IBOutlet weak var imgDetail: UIImageView!
    
    var delegate:ListSterilizationCell2Delegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.viewCamera.layer.borderWidth = 1.0
//        self.viewCamera.layer.borderColor = UIColor.lightGray.cgColor
        
        let viewBorder = UIView.init(frame: self.viewCamera.frame)
        viewBorder.backgroundColor = UIColor.clear
        viewBorder.layer.borderWidth = 1.0
        viewBorder.layer.borderColor = UIColor.lightGray.cgColor
        self.addSubview(viewBorder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - IBAction
    @IBAction func cameraPress(_ sender: Any) {
        self.delegate?.goToCamera()
    }
}
