//
//  DetailWorkCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class DetailWorkCell: UICollectionViewCell {
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewImgNil: UIView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var viewCondition: UIView!
    @IBOutlet weak var constraintTopImgCamera: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLbTitle: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
