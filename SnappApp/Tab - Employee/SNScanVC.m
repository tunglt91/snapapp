//
//  SNScanVC.m
//  SnappApp
//
//  Created by Tu Ngo on 9/11/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

#import "SNScanVC.h"
#import "SnappApp-Swift.h"

//　拡大／縮小された表示用グラフィックスコンテキストを作る
float w, h;
float X0, Y0;

//　拡大／縮小されたビットマップ画像のC構造体へのポインターとピクセル情報
CGImageRef  cgImage;

//　表示ウインドーの大きさと、拡大率
//fk20150501
//float scaleX = 1.0;
float scaleX = 2.8;

//fk20150215
float panelWidth = 320.0;
float panelHeight = 320.0;
//double panelWidth = 320.0;
//double panelHeight = 320.0;

//　タッチ点の順番設定
int i = 0;
float locationX0 = 0;
float locationY0 = 0;
float locationX1 = 0;
float locationY1 = 0;

//fk20150214
//float adjLocationX = 0;
//float adjLocationY = 0;
float adjLocationX = 0.0;
float adjLocationY = 0.0;

// 画面全平均
int aveR = 118;                    //fk20131120
int aveG = 118;
int aveB = 118;

//fk20141114
//Lab値受け渡し用
float sRGBL = 0.0;
float sRGBa = 0.0;
float sRGBb = 0.0;

//fk20150209
int captureSize = 9;
int lowerLimit = 4;
int upperLimit = 5;

//fk20150204
int scrollOnOff = 1;

//fk20180809
//fk20150317
//int avR = 118;
//int avG = 118;
//int avB = 118;

//fk20151008
NSMutableArray* colorSet;

//fk20150317
float avX = -20.0;
float avY = -20.0;
CGRect acquiredRectAV;

//fk20150320
int maxAreaTemp = 128;
int maxToleranceTemp = 25;

//fk20150215
int resetFlag = 0;

//fk20180518
//int selectionFlag = 0;
int R9x9 = 128;
int G9x9 = 128;
int B9x9 = 128;
int targetWidth = 1;
int targetHeight = 1;

//fk20180521
float L9x9 = 50.0;
float a9x9 = 0.0;
float b9x9 = 0.0;
int pixelSize = 9;      //タッチした周りの初期平均を求めるサイズを、3x3、9x9、27x27、と選択できる様にするため
int sizeFlag = 1;

//fk20180521
int searchSize = 32;
int searchSizeFlag = 1;

//fk20180523
int loupeFlagSetting = 0;
int loupeFlag = 0;
CGPoint touchPointForMag;

//fk20180713
int overlapFlag = 0;    //0:Non Overlapped 1:Overlapped

//fk20180717
float y0h0 = 0.0;
float y1only = 0.0;

//fk20180727
int sRLightBottom = 0;
int sGLightBottom = 0;
int sBLightBottom = 0;

//fk20180802
int backgroundFlagSetting = 1;      //1:gray 0:white
float LLightBottom = 0.0;
float aLightBottom = 0.0;
float bLightBottom = 0.0;
float overlapAmount = 0.0;

//fk20180813
int histoSettingFlag = 1;

//----------------------------------------------------------------------//
#pragma mark -- 上に重ねる図形類の処理（ユーザーインターフェースとピクセル情報取得） --
//----------------------------------------------------------------------//

@implementation InteractiveView

//　拡大／縮小された画像のタッチポインター位置
CGPoint location;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}


//　四角形を描く処理
- (void)drawRect:(CGRect)rect
{
    NSLog(@"%tu", searchSize);
    
    //fk20150214
    if (!cgImage){                  //fk20140422 ファイルが選ばれる前のタッチの問題の回避
        return;
    }
    //fk20151009　計算の初期化処理
    if (resetFlag == 1) {
        
        //fk20180809
        //fk20150317
        //        avR = 118;
        //        avG = 118;
        //        avB = 118;
        
        avX = -20.0;
        avY = -20.0;
        
        resetFlag = 0;
        
        return;
        
    }
    
    
    //fk20150311
    @autoreleasepool {
        
        
        //----------------------------------------------------------------------//
#pragma mark -- タッチ位置の検出と範囲外探索エラーの補正、Defaultの色取得、範囲取得 --
        //----------------------------------------------------------------------//
        //  計算に必要な画僧データの取得とその部分の表示処理
        //  描画先情報を得る
        CGContextRef context = UIGraphicsGetCurrentContext();
        //  タッチ位置に9x9のマゼンタの正方形を描く、等の準備開始
        
        //fk20180717
        //        CGContextSetLineWidth(context, 2.0);
        CGContextSetLineWidth(context, 1.0);
        
        //　xのタッチ位置が画像の端からlowerLimit以下ならlowerLimitにする補正
        if ( location.x < lowerLimit ){
            adjLocationX = lowerLimit;
        }else if ( location.x > (w - lowerLimit)){
            adjLocationX = w - lowerLimit;
        }else{
            adjLocationX = location.x;
        }
        
        //　yのタッチ位置が画像の端からlowerLimit以下ならlowerLimitにする補正
        if ( location.y < lowerLimit ){
            adjLocationY = lowerLimit;
        }else if (location.y >= (h - lowerLimit)){
            adjLocationY = h - lowerLimit;
        }else{
            adjLocationY = location.y;
        }
        
        //　補正したタッチ位置の決定
        
        avX = adjLocationX;
        avY = adjLocationY;
        
        CGPoint avPoint = {avX, avY};
        
        //　近似色の外接四角形の取得準備
        Acquire *instanceAcq = [[Acquire alloc] init];
        
        //　パラメーターの取り込みの準備
        //fk20180523
        //        appDelegate = [[UIApplication sharedApplication] delegate];
        //appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        //　探索のための最大取得範囲と最大許容範囲の取得と外接四角形の取得
        
        //fk20180522
        //        maxAreaTemp = appDelegate.globalMaxArea;
        maxAreaTemp = searchSize;
        
        //maxToleranceTemp = appDelegate.globalMaxTolerance;
        maxToleranceTemp = 5;
        
        //fk20180801 タッチ点（中心点）周りのマゼンタ内と同じ色の範囲のシアンの四角を書くための準備（大きさは、設定で変わる）初期化の意味もある
        //        CGRect acquireRectAV = [instanceAcq acquire:cgImage touchPoint:avPoint scale:scaleX];
        CGRect acquireRectAV = [instanceAcq acquireD:cgImage touchPoint:avPoint scale:scaleX maxArea:maxAreaTemp maxTolerance:maxToleranceTemp];
        
        //fk20180821
        //　タッチ点の周りの9x9(設定により、pixelSize * pixelSize or 3x3 or 9x9 or 27x27)ピクセルの平均色値の取得
        Grab *instanceGrab = [[Grab alloc] init];
        
        //fk20180521
        //        NSMutableArray* averaged9x9RGBav = [instanceGrab getRGBave:cgImage touchPoint:avPoint captureSize:9];
        NSMutableArray* averaged9x9RGBav = [instanceGrab getRGBave:cgImage touchPoint:avPoint captureSize:pixelSize];
        
        //        NSLog(@"%@", averaged9x9RGBav);
        NSLog(@"averaged9x9RGBav\n%@", averaged9x9RGBav);         //fk20151009
        
        //fk20180518
        R9x9 = [[averaged9x9RGBav objectAtIndex:0] intValue];
        G9x9 = [[averaged9x9RGBav objectAtIndex:1] intValue];
        B9x9 = [[averaged9x9RGBav objectAtIndex:2] intValue];
        
        //fk20180822
        targetWidth = acquireRectAV.size.width;
        targetHeight = acquireRectAV.size.height;
        
        //fk20180821 タッチ点の表示
        //　Touch点の中心の表示
        [[UIColor magentaColor] set];
        
        //fk20180521
        //        CGContextStrokeRect(context, CGRectMake(avPoint.x - 4, avPoint.y - 4, 9, 9));
        CGContextStrokeRect(context, CGRectMake(avPoint.x - (pixelSize-1)/2, avPoint.y - (pixelSize-1)/2, pixelSize, pixelSize));
        
        
        
        
        
        //---------------------------------------------------------------//
#pragma mark -- 白黒の色判定で、角の色を参照用に利用する CASE 0 : 1233LF --
        //---------------------------------------------------------------//
        //fk20180726
        if (self.selectionFlag == 0){
            
            //            CGPoint wPoint = {avPoint.x, avPoint.y - pixelSize*6.5*scaleX};
            
            [[UIColor yellowColor] set];
            CGContextStrokeRect(context, CGRectMake(avPoint.x - pixelSize*scaleX, avPoint.y - pixelSize*scaleX, pixelSize*scaleX*2, pixelSize*scaleX*2));
            
            //fk20180821 ミスタッチをした場合でも、ヒストグラム的に、近傍で、明るい色か暗い色のみを、取り出し、測定をするためのルーチン
            //fk20180813
            if (histoSettingFlag == 1){
                
                NSMutableArray* centerDarker = [instanceAcq acquireLB:cgImage touchPoint:avPoint pixelSize:pixelSize*scaleX scale:1.0 refR:100 refG:100 refB:100];
                
                int sRcenterDarker = [[centerDarker objectAtIndex:0] intValue];
                int sGcenterDarker = [[centerDarker objectAtIndex:1] intValue];
                int sBcenterDarker = [[centerDarker objectAtIndex:2] intValue];
                
                //fk20180815 黒い部分がないとき
                //                if (sRcenterDarker == 0 || sGcenterDarker == 0 || sBcenterDarker == 0){
                
                //                    R9x9 = 255;
                //                    G9x9 = 255;
                //                    B9x9 = 255;
                
                //                }else{
                
                R9x9 = sRcenterDarker;
                G9x9 = sGcenterDarker;
                B9x9 = sBcenterDarker;
                
                //            }
                
            }else{
                
            }
            
            CGRect acquiredWhiteRectAV = [instanceAcq acquireLW:cgImage touchPoint:avPoint scale:scaleX maxArea:160*scaleX maxTolerance:(maxToleranceTemp+10) pixelSize:pixelSize];
            
            //            CGContextStrokeRect(context, CGRectMake(avPoint.x - (pixelSize-1)/2, avPoint.y - (pixelSize-1)/2 - pixelSize*6.5*scaleX, pixelSize, pixelSize));
            
            //            NSMutableArray* aveWhite9x9RGBav = [instanceGrab getRGBave:cgImage touchPoint:wPoint captureSize:pixelSize];
            
            //            CGRect acquiredWhiteRectAV = [instanceAcq acquireD:cgImage touchPoint:wPoint scale:1.0 maxArea:240*scaleX maxTolerance:(maxToleranceTemp+30)];
            
            [[UIColor cyanColor] set];
            CGContextStrokeRect(context, CGRectMake(acquiredWhiteRectAV.origin.x,
                                                    acquiredWhiteRectAV.origin.y,
                                                    acquiredWhiteRectAV.size.width,
                                                    acquiredWhiteRectAV.size.height));
            
            //fk20180801 白い上記外接四角形の左下角より少し内側に下記の緑の四角を求める
            //fk20180727
            [[UIColor greenColor] set];
            CGContextStrokeRect(context, CGRectMake(acquiredWhiteRectAV.origin.x + pixelSize*scaleX, acquiredWhiteRectAV.origin.y + acquiredWhiteRectAV.size.height - pixelSize*scaleX*3.5, pixelSize*scaleX*2, pixelSize*scaleX*2));
            
            //CGPoint pointLB = {acquiredWhiteRectAV.origin.x + pixelSize*scaleX + pixelSize*scaleX, acquiredWhiteRectAV.origin.y + acquiredWhiteRectAV.size.height - pixelSize*scaleX*3.5 + pixelSize*scaleX};
            CGPoint pointLB = {acquiredWhiteRectAV.origin.x + pixelSize*scaleX*2, acquiredWhiteRectAV.origin.y + acquiredWhiteRectAV.size.height - pixelSize*scaleX*2.5};
            
            if ((0 < pointLB.x) && (pointLB.x < panelWidth) && (0 < pointLB.y) && (pointLB.y < panelHeight)){
                
                //fk20180801 acquireLB は、上記の緑色の四角形内の色のうちで、始めのタッチ点のRGB値refR,refG,refBより小さい（暗い）色の平均値を求める関数
                NSMutableArray* cornerLB = [instanceAcq acquireLB:cgImage touchPoint:pointLB pixelSize:pixelSize*scaleX scale:1.0 refR:R9x9 refG:G9x9 refB:B9x9];
                
                int sRcornerLB = [[cornerLB objectAtIndex:0] intValue];
                int sGcornerLB = [[cornerLB objectAtIndex:1] intValue];
                int sBcornerLB = [[cornerLB objectAtIndex:2] intValue];
                
                sRLightBottom = sRcornerLB;
                sGLightBottom = sGcornerLB;
                sBLightBottom = sBcornerLB;
                
            }else{
                
            }
            
        }

        //---------------------------------------------------------------//
#pragma mark -- 白黒の色判定で、真下の色を参照用に利用する CASE 3 : 1250 --
        //---------------------------------------------------------------//
        //fk20180727
        if (self.selectionFlag == 1){
            
            //            CGPoint wPoint = {avPoint.x, avPoint.y - pixelSize*6.5*scaleX};
            
            [[UIColor yellowColor] set];
            
            
            //fk20180810
            //            CGContextStrokeRect(context, CGRectMake(avPoint.x - pixelSize*scaleX/2, avPoint.y - pixelSize*scaleX/2, pixelSize*scaleX, pixelSize*scaleX));
            CGContextStrokeRect(context, CGRectMake(avPoint.x - pixelSize*scaleX, avPoint.y - pixelSize*scaleX, pixelSize*scaleX*2, pixelSize*scaleX*2));
            
            
            
            
            
            //fk20180813
            if (histoSettingFlag == 1){
                
                //fk20180816
                //                NSMutableArray* centerDarker = [instanceAcq acquireLB:cgImage touchPoint:avPoint pixelSize:pixelSize*scaleX scale:1.0 refR:100 refG:100 refB:100];
                NSMutableArray* centerDarker = [instanceAcq acquireLB:cgImage touchPoint:avPoint pixelSize:pixelSize*scaleX scale:1.0 refR:170 refG:150 refB:130];
                
                int sRcenterDarker = [[centerDarker objectAtIndex:0] intValue];
                int sGcenterDarker = [[centerDarker objectAtIndex:1] intValue];
                int sBcenterDarker = [[centerDarker objectAtIndex:2] intValue];
                
                R9x9 = sRcenterDarker;
                G9x9 = sGcenterDarker;
                B9x9 = sBcenterDarker;
                
            }else{
                
            }
        
            CGRect acquiredWhiteRectAV = [instanceAcq acquireLW:cgImage touchPoint:avPoint scale:scaleX maxArea:160*scaleX maxTolerance:(maxToleranceTemp+10) pixelSize:pixelSize];
            
            //acquiredWhiteRectAV.size.height = acquiredWhiteRectAV.size.height + 500;
            
            [[UIColor cyanColor] set];
            CGContextStrokeRect(context, CGRectMake(acquiredWhiteRectAV.origin.x,
                                                    acquiredWhiteRectAV.origin.y,
                                                    acquiredWhiteRectAV.size.width,
                                                    acquiredWhiteRectAV.size.height));
            
            //fk20180801 白い上記外接四角形の底辺の真中より少し内側に下記の緑の四角を求める
            //fk20180727
            [[UIColor greenColor] set];
            
            CGContextStrokeRect(context, CGRectMake(acquiredWhiteRectAV.origin.x + acquiredWhiteRectAV.size.width/2 - pixelSize*scaleX/2, acquiredWhiteRectAV.origin.y + acquiredWhiteRectAV.size.height - pixelSize*scaleX*1.5, pixelSize*scaleX, pixelSize*scaleX));
            
            CGPoint pointLB = {acquiredWhiteRectAV.origin.x + acquiredWhiteRectAV.size.width/2, acquiredWhiteRectAV.origin.y + acquiredWhiteRectAV.size.height - pixelSize*scaleX};
            
            if ((0 < pointLB.x) && (pointLB.x < panelWidth) && (0 < pointLB.y) && (pointLB.y < panelHeight)){
                
                NSMutableArray* cornerLB = [instanceAcq acquireLB:cgImage touchPoint:pointLB pixelSize:pixelSize*scaleX scale:1.0 refR:R9x9+50 refG:G9x9+50 refB:B9x9+50];
                
                
                int sRcornerLB = [[cornerLB objectAtIndex:0] intValue];
                int sGcornerLB = [[cornerLB objectAtIndex:1] intValue];
                int sBcornerLB = [[cornerLB objectAtIndex:2] intValue];
                
                sRLightBottom = sRcornerLB;
                sGLightBottom = sGcornerLB;
                sBLightBottom = sBcornerLB;
                
            }else{
                
            }
            
        }
        
#pragma mark -- 測定関連終了 --
        //-------------------------//
        
        //　色をマゼンタに戻して
        [[UIColor magentaColor] set];            //fk20150319
        //        [[UIColor clearColor] set];               //fk20150723 元に戻し
        
        
        
        // データ取得と表示の完了通知
        NSNotification *n = [NSNotification notificationWithName:@"sRGB" object:self];
        [[NSNotificationCenter defaultCenter] postNotification:n];
        
        //--------------------------------------//
#pragma mark -- ルーペ表示とタッチの取り扱い --
        //--------------------------------------//
        //fk20180523
        if (loupeFlag == 1){
            if(sizeFlag == 0){
                
                [[UIColor yellowColor] set];
                CGContextStrokeRect(context, CGRectMake(touchPointForMag.x-1, touchPointForMag.y-1, 3, 3));
                
            }else if(sizeFlag == 1){
                
                [[UIColor yellowColor] set];
                CGContextStrokeRect(context, CGRectMake(touchPointForMag.x-4, touchPointForMag.y-4, 9, 9));
                
            }else{
                
                [[UIColor yellowColor] set];
                CGContextStrokeRect(context, CGRectMake(touchPointForMag.x-13, touchPointForMag.y-13, 27, 27));
                
            }
            
        }else{
            
        }
        
        UIGraphicsEndImageContext();        //　表示の終了
        
    }        //fk20150311 @autoreleasepool
    
}
//drawRect終わり

//-----------------------------------------------//
#pragma mark -- ルーペ表示とタッチの取り扱い用メソッド類 --
//-----------------------------------------------//

//fk20180523
@synthesize touchTimer;


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if(loupeFlagSetting == 0){
        
        printf("\n\nin OFF\n");
        
        return;
        
    }else{
        
    }
    
    printf("\n\nin ON\n");
    
    loupeFlag = 1;
    
    UITouch *touchForMag = [touches anyObject];
    touchPointForMag = [touchForMag locationInView:self];
    [self setNeedsDisplay];
    
    printf("In TouchesBegan\n");
    
    self.touchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                       target:self
                                                     selector:@selector(addLoupe)   //fk20180523
                                                     userInfo:nil
                                                      repeats:NO];
    
    //fk20180523
    // just create one loupe and re-use it.
    
    if(loupe == nil){
        
        loupe = [[MagnifierView alloc] init];
        loupe.viewToMagnify = self.superview;
        
    }
    
    UITouch *touch = [touches anyObject];
    
    loupe.touchPoint = [touch locationInView:self];
    [loupe setNeedsDisplay];
    
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(loupeFlagSetting == 0){
        
        printf("\n\nin OFF\n");
        
        return;
        
        //    }else if (scaleX > 2.5){
        //        printf("\n\nin Scale");
        //        return;
        
    }
    
    printf("\n\nin ON\n");
    
    //fk20140212
    loupeFlag = 1;
    UITouch *touchForMag = [touches anyObject];
    touchPointForMag = [touchForMag locationInView:self];
    [self setNeedsDisplay];
    
    [self handleAction:touches];
    
}


//fk20180523
- (void)addLoupe {
    // add the loop to the superview.  if we add it to the view it magnifies, it'll magnify itself!
    //    [self.superview addSubview:loop];
    [self.superview addSubview:loupe];
    // here, we could do some nice animation instead of just adding the subview...
}


- (void)handleAction:(id)timerObj {
    
    NSSet *touches = timerObj;
    UITouch *touch = [touches anyObject];
    
    loupe.touchPoint = [touch locationInView:self];
    [loupe setNeedsDisplay];
    
}


- (void)dealloc {
    //    [loupe release];
    //    loupe = nil;
    //    [super dealloc];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    loupeFlag = 0;
    
    [self setNeedsDisplay];
    
    [self.touchTimer invalidate];
    self.touchTimer = nil;
    
    [loupe removeFromSuperview];
    
    loupe = nil;
    
    UITouch* touch = [touches anyObject];
    
    //  位置を保存する。
    location = [touch locationInView:self];
    
    //　タッチ位置のViewの座標
    NSLog(@"x = %f, y = %f", location.x, location.y);               //fk20130319
    
    //  描画の必要がある事を自身に通知する。
    [self setNeedsDisplay];
    
}


@end





//---------------------------------------//
#pragma mark -- 画像の取り込みと計算結果の標準 --
//---------------------------------------//

//　ファイルからの画像取得と表示機能
@interface SNScanVC ()<SNConfirmImageDelegate>

@end

@implementation SNScanVC


UIScrollView *sv;

UIImageView* imageView;
//UIView* firstView;

int R1 = 0; int G1 = 0; int B1 = 0;
int R2 = 0; int G2 = 0; int B2 = 0;

//UIImage *originalImage;
//UIImage *shrinkedImage;

float width;
float height;


//--------------------------------------------------------------//
#pragma mark -- UIImagePickerControllerDelegate --
//--------------------------------------------------------------//

- (void) shrinkImage
{
    //fk20180809-1 一旦、640にしたが、640から元に戻す。
    panelWidth = 320 * scaleX;  //暫定パネルの縦横のサイズを変数に記録
    panelHeight = 320 * scaleX;
    //    panelWidth = 320*scaleX;  //暫定パネルの縦横のサイズを変数に記録
    //    panelHeight = 320*scaleX;
    
    NSLog(@"original.width = %.1f, original.height = %.1f", width, height);
    
    // グラフィックスコンテキストを作る
    if ((width > panelWidth) && (height > panelHeight) ){
        if (width >= height){
            w = panelWidth;
            h = height*(panelWidth/width);
            X0 = 0;
            Y0 = 0;
        } else {
            w = width*(panelHeight/height);     //　バグあり（原点を移動できず）
            h = panelHeight;
            X0 = 0;//  (320 - width*(320/height))/2;
            Y0 = 0; //0
        }
    }else if ((width > panelWidth) && (height <= panelHeight)){
        w = panelWidth;
        h = height*(panelWidth/width);
        X0 = 0;
        Y0 = 0;
    }else if ((width <= panelWidth) && (height > panelHeight)){
        w = width*(panelHeight/height);
        h = panelHeight;
        X0 = 0;
        Y0 = 0;
    }else{
        w = width;
        h = height;
        X0 = 0;
        Y0 = 0;
    }
    
    CGSize size = {w, h};
    CGPoint origin = {X0, Y0};
    UIGraphicsBeginImageContext(size);
    
    // 画像を縮小して描画する
    CGRect  rect;
    rect.origin =  origin; //CGPointZero;
    rect.size = size;
    [self.originalImage drawInRect:rect];
    
    [self.viewImage setHidden:true];
    
    // 描画した画像を取得する
    //    UIImage* shrinkedImage;
    self.shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 画像を表示する
    //    _imageView.image = shrinkedImage;
    
    //    CGImageRef  cgImage;
    cgImage = self.shrinkedImage.CGImage;
    
    
    [self scrollImageDraw];
    
    
}


//　拡大／縮小とスクロール可能画像への変換
- (void) scrollImageDraw{
    
    //fk20180810 また元に戻す
    //fk20180809-1
    panelWidth = 320 * scaleX;  //暫定パネルの縦横.のサイズを変数に記録
    panelHeight = 320 * scaleX;
    //    panelWidth = 640.0 * scaleX;  //暫定パネルの縦横のサイズを変数に記録
    //    panelHeight = 640.0 * scaleX;
    
    [sv removeFromSuperview];   //  クラス内だがメソッドの外のオブジェクトなので、前のものを消さないと、次のものが描画されないよ！！！
    
    CGRect backRect = CGRectMake(0.0, 0.0, w, h);
    
    //fk20180809-1
    //    CGRect frontRect = CGRectMake(0.0, 0.0, 320.0, 320.0);
    //fk20180810
    //    CGRect frontRect = CGRectMake(((768-640)/2.0), 0.0, 640.0, 640.0);
    
    // Tu fix
    CGRect frontRect = CGRectMake(0, 104, 768, 1000); //CGRect frontRect = CGRectMake(0, 0, 768, 768);
    
    sv = [[UIScrollView alloc] initWithFrame:frontRect];
    sv.backgroundColor = [UIColor grayColor];
    /*
    [sv setScrollEnabled:NO];
    sv.center = self.view.center;
     */
    if (self.selectionFlag == 1) {
        //sv.center = CGPointMake(self.view.center.x, 510); //y:620
        searchSize = 16;
        pixelSize = 3;
    }else {
        searchSize = 32;
        pixelSize = 9;
    }
    NSLog(@"searchSize: %d pixelSize: %d", searchSize, pixelSize);
    
    UIImageView* uv = [[UIImageView alloc] initWithFrame: backRect];     //背景のビュー
    
    uv.image = self.shrinkedImage;
    
    [sv addSubview:uv];
    sv.contentSize = uv.bounds.size;
    [self.view insertSubview:sv atIndex:0];
    //[self.view addSubview:sv];
    
    NSLog(@"scaleX = %f", scaleX);
    
    self.firstView = [[InteractiveView alloc] initWithFrame:CGRectMake(0, 0, panelWidth, panelHeight)];
    self.firstView.selectionFlag = self.selectionFlag;
    [sv addSubview:self.firstView];
    self.firstView.backgroundColor = [UIColor clearColor];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor grayColor];
    self.typeSelected.hidden = YES;
    self.viewTool.hidden = YES;
    self.btnJudge.hidden = YES;
    
    self.btnJudge.layer.cornerRadius = 30;
    self.btnJudge.layer.masksToBounds = YES;
    
    //fk20180809-1
    //    UIView *viewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 320, 320, 320)];
    //fk20180810
    //    UIView *viewTop = [[UIView alloc] initWithFrame:CGRectMake(((768-320)/2.0), (1024-320), 320, 320)];
    
    
    //fk20180810-1
    //    UIView *viewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 768, 768, 256)];
    
    //    viewTop.backgroundColor = [UIColor lightGrayColor];
    //    [self.view insertSubview:viewTop atIndex:1];
    //[self scrollImageDraw];
    
    NSNotificationCenter *notficationCenter = [NSNotificationCenter defaultCenter];
    [notficationCenter addObserver:self
                          selector:@selector(labelWrite)
                              name:@"sRGB"
                            object:nil];
    
    
    //fk20180523
    //    appDelegate = [[UIApplication sharedApplication] delegate];                                  //fk20150129
    //appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //captureSize = appDelegate.globalCaptureSize;
    captureSize = 9;
    lowerLimit = captureSize/2;
    upperLimit = lowerLimit + 1;
    
    /*
    self.scale.value = (2.0*log10(scaleX*2.0))/log10(2.0);
    //    printf("scaleX:%f, self.scale.value:%f", scaleX, self.scale.value);
    NSLog(@"scaleX:%f, self.scale.value:%f", scaleX, self.scale.value);     //fk20151009
    
    scaleX = 0.5 * pow(sqrt(2), self.scale.value);
    self.currentScale.text = [NSString stringWithFormat: @"x%.1f", scaleX];
     */
    
    //　拡大／縮小率の再設定
    
    /*
    self.scale.value = 6;
    //scaleX = 3.0;
    scaleX = 0.5 * pow(sqrt(2), self.scale.value);
    self.currentScale.text = [NSString stringWithFormat: @"x%.1f", scaleX];
    */
    resetFlag = 1;
    
    // オリジナル画像を取得する
    //    UIImage*    originalImage;
    
    width = self.originalImage.size.width;
    height = self.originalImage.size.height;
    [self shrinkImage];
    
}

#pragma mark -- Show ConfirmImageVC --
- (void)showConfirmImageVC {
    [self.btnJudge setHidden:FALSE];
}

- (IBAction)judgePress:(id)sender {
    [self.btnJudge setHidden:YES];
    UIView *sp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height)];
    sp.backgroundColor = UIColor.clearColor;
    [self.view addSubview:sp];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sp removeFromSuperview];
        SNConfirmImageVC *confirmVC = [[UIStoryboard storyboardWithName:@"Employee" bundle:nil] instantiateViewControllerWithIdentifier:@"SNConfirmImageVC"];
        confirmVC.delegate = self;
        confirmVC.snapShot = self.originalImage;
        confirmVC.selectionFlag = self.selectionFlag;
        confirmVC.arrayImg = self.arrayImg;
        confirmVC.dataWork = self.dataWork;
        confirmVC.indexArray = self.indexArray;
        confirmVC.typeNoti = self.typeNotification;
        [confirmVC showInVCWithVc:self];
    });
}


#pragma mark -- Delegate --
- (void)dismissButtonDidToggle {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)closeUpButtonDidToggleWithArrayData:(NSArray * _Nonnull)arrayData selectionFlag:(NSInteger)selectionFlag dataCheckCondition:(NSArray * _Nonnull)dataCheckCondition {
    [self.delegate reloadDataTestResult:arrayData selectionFlag:selectionFlag dataCheckCondition:dataCheckCondition];
}

//-------------------------------//
#pragma mark -- 測定結果の表示と判定 --
//-------------------------------//
//　ピクセル値表示用のメソッド
- (void)labelWrite
{
    self.lbNoti.hidden = true;
    //fk20180521
    RGB2Lab *instanceRGB2Lab = [[RGB2Lab alloc] init];
    NSMutableArray* Lab = [instanceRGB2Lab getLab:R9x9 sG:G9x9 sB:B9x9];
    
    NSLog(@"Lab\n%@", Lab);
    
    L9x9 = [[Lab objectAtIndex:0] doubleValue];
    a9x9 = [[Lab objectAtIndex:1] doubleValue];
    b9x9 = [[Lab objectAtIndex:2] doubleValue];
    
    self.LabData.text = [NSString stringWithFormat:@"L:%0.3f a:%0.3f b:%0.3f", L9x9, a9x9, b9x9];
    
    //fk20180802
    NSMutableArray* cornerLab = [instanceRGB2Lab getLab:sRLightBottom sG:sGLightBottom sB:sBLightBottom];
    
    NSLog(@"cornerLab\n%@", cornerLab);
    
    LLightBottom = [[cornerLab objectAtIndex:0] doubleValue];
    aLightBottom = [[cornerLab objectAtIndex:1] doubleValue];
    bLightBottom = [[cornerLab objectAtIndex:2] doubleValue];
    
    
    //----------------------//
#pragma mark -- 1233判定 --
    //----------------------//
    switch (self.selectionFlag) {
        case 0:
            if ((L9x9 < 19) && (a9x9 < 100) && (b9x9 < 100)){
                
                self.userCIvalue.textColor = [UIColor greenColor];
                self.userCIvalue.text = [NSString stringWithFormat:@"OK Good"];
                self.typeNotification = 1;
                [self showConfirmImageVC];
            }else if (L9x9 < 41 && a9x9 < 100 && b9x9 < 100){
                
                //fk20180802
                if (L9x9 < LLightBottom*1.25){
                    
                    self.userCIvalue.textColor = [UIColor greenColor];
                    self.userCIvalue.text = [NSString stringWithFormat:@"OK Good acceptable"];
                    self.typeNotification = 1;
                    [self showConfirmImageVC];
                }else{
                    
                    self.userCIvalue.textColor = [UIColor yellowColor];
                    self.userCIvalue.text = [NSString stringWithFormat:@"Marginal - Try Again"];
                    self.typeNotification = 3;
                    [self showConfirmImageVC];
                }
                
            }else{
                
                self.userCIvalue.textColor = [UIColor redColor];
                self.userCIvalue.text = [NSString stringWithFormat:@"Unacceptable"];
                self.typeNotification = 2;
                [self showConfirmImageVC];
            }
            
            //fk20180727
            self.sRGBLightBottom.textColor = [UIColor yellowColor];
            self.sRGBLightBottom.text = [NSString stringWithFormat: @"sR:%d sG:%d sB:%d at LB", sRLightBottom, sGLightBottom, sBLightBottom];
            //FK20180802
            self.LabBottom.textColor = [UIColor yellowColor];
            self.LabBottom.text = [NSString stringWithFormat:@"L:%0.2f a:%0.2f b:%0.2f at LB", LLightBottom, aLightBottom, bLightBottom];
            
            break;
#pragma mark -- 1250判定 --
            //----------------------//
        case 1:
            
            //fk20180522
            //            if ((L9x9 < 19) && (a9x9 < 100) && (b9x9 < 100)){
            
            if ((L9x9 < 17.9) && (a9x9 < 25) && (b9x9 < 50)){
                
                self.userCIvalue.textColor = [UIColor greenColor];
                self.userCIvalue.text = [NSString stringWithFormat:@"OK Good"];
                self.typeNotification = 1;
                [self showConfirmImageVC];
            }else if (L9x9 < 41 && a9x9 < 25 && b9x9 < 50){
                
                //fk20180802 レファレンスの方がうすい場合もあるので
                if (L9x9 < LLightBottom*1.25){
                    
                    self.userCIvalue.textColor = [UIColor greenColor];
                    self.userCIvalue.text = [NSString stringWithFormat:@"OK Good acceptable"];
                    self.typeNotification = 1;
                    [self showConfirmImageVC];
                }else{
                    
                    self.userCIvalue.textColor = [UIColor yellowColor];
                    self.userCIvalue.text = [NSString stringWithFormat:@"Marginal - Try Again"];
                    self.typeNotification = 3;
                    [self showConfirmImageVC];
                }
                
            }else{
                
                self.userCIvalue.textColor = [UIColor redColor];
                self.userCIvalue.text = [NSString stringWithFormat:@"Unacceptable"];
                self.typeNotification = 2;
                [self showConfirmImageVC];
            }
            
            //fk20180727
            self.sRGBLightBottom.textColor = [UIColor yellowColor];
            self.sRGBLightBottom.text = [NSString stringWithFormat: @"sR:%d sG:%d sB:%d at CB", sRLightBottom, sGLightBottom, sBLightBottom];
            
            //fk20180802
            self.LabBottom.textColor = [UIColor yellowColor];
            self.LabBottom.text = [NSString stringWithFormat:@"L:%0.2f a:%0.2f b:%0.2f at CB", LLightBottom, aLightBottom, bLightBottom];
            
            break;
        default:
            
            break;
            
    }
    
    self.userRGBValue.text = [NSString stringWithFormat:@"R:%d G:%d B:%d", R9x9, G9x9, B9x9];
    
}


//-------------------------------//
#pragma mark -- その他のコントロール --
//-------------------------------//


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)scale:(id)sender {
    
    //self.scale.value = 1.0;
    
    scaleX = 0.5 * pow(sqrt(2), self.scale.value);
    self.currentScale.text = [NSString stringWithFormat: @"x%.1f", scaleX];
    
    if(cgImage){                                //fk20140605 ファイルが選ばれていないときの対応
        
        [self shrinkImage];
        
    }
    
    //fk20150316
    scrollOnOff = YES;
    self.scroll.on = YES;
    
}


- (IBAction)viewScroll:(id)sender {
    
    UISwitch *switchStatusTemp = (UISwitch*)sender;
    
    //スイッチがONとOFFの場合で場合分け
    if(switchStatusTemp.on == YES){
        
        scrollOnOff = 1;
        [sv setScrollEnabled:YES];
        
    }else{
        
        scrollOnOff = 0;
        [sv setScrollEnabled:NO];
        
    }
}


- (IBAction)typeSelect:(id)sender {
    
    self.selectionFlag = (int)self.typeSelected.selectedSegmentIndex;
    
    overlapFlag = 0;
    self.overlap.textColor = [UIColor blackColor];
    self.overlap.text = [NSString stringWithFormat: @"Non Overlapped"];
    
}


- (IBAction)sizeSelect:(id)sender {
    
    sizeFlag = (int)self.sizeSelected.selectedSegmentIndex;
    [self sizeSetting];
    
}


- (void)sizeSetting{
    
    switch (sizeFlag) {
        case 0:
            NSLog(@"Sが選択されています");
            pixelSize = 3;
            NSLog(@"pixelSize:%d", pixelSize);
            
            break;
            
        case 1:
            NSLog(@"Mが選択されています");
            pixelSize = 9;
            NSLog(@"pixelSize:%d", pixelSize);
            
            break;
            
        case 2:
            NSLog(@"Lが選択されています");
            pixelSize = 27;
            NSLog(@"pixelSize:%d", pixelSize);
            
            break;
            
        default:
            
            break;
            
    }
    
}

- (IBAction)searchSizeSelect:(id)sender {
    
    searchSizeFlag = (int)self.searchSizeSelected.selectedSegmentIndex;
    [self searchSizeSetting];
}


- (void)searchSizeSetting{
    
    switch (searchSizeFlag) {
        case 0:
            NSLog(@"Nが選択されています");
            searchSize = 16;
            NSLog(@"searchSize:%d", searchSize);
            
            break;
            
        case 1:
            NSLog(@"Mが選択されています");
            searchSize = 32;
            NSLog(@"searchSize:%d", searchSize);
            
            break;
            
        case 2:
            NSLog(@"Wが選択されています");
            //fk20180702
            //            searchSize = 64;
            //fk20180713
            //           searchSize = 256;
            searchSize = 128;
            NSLog(@"searchSize:%d", searchSize);
            
            break;
            
        default:
            
            break;
            
    }
    
}


- (IBAction)loupeSetting:(id)sender {
    
    UISwitch *switchStatusTemp = (UISwitch*)sender;
    
    //スイッチがONとOFFの場合で場合分け
    if(switchStatusTemp.on == YES){
        
        loupeFlagSetting = 1;
        
    }else{
        
        loupeFlagSetting = 0;
        
    }
}

/*
- (IBAction)centerHistoSetting:(id)sender {
    
    UISwitch *switchStatusTemp = (UISwitch*)sender;
    
    //スイッチがONとOFFの場合で場合分け
    if(switchStatusTemp.on == YES){
        
        histoSettingFlag = 1;
        self.histoSetting.text = @"With";
        
    }else{
        
        histoSettingFlag = 0;
        self.histoSetting.text = @"Without";
        
    }
}
 */

- (IBAction)backPress:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}



@end

