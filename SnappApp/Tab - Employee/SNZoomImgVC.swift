//
//  SNZoomImgVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import ImageScrollView

enum ZoomScanType {
    case Doc
    case AO
    case EO
}

class SNZoomImgVC: UIViewController {
    @IBOutlet weak var btnCloseUp: UIButton!
    @IBOutlet weak var imageScrollDoc: ImageScrollView!
    @IBOutlet weak var imageScrollAO: ImageScrollView!
    @IBOutlet weak var imageScrollEO: ImageScrollView!
    
    var nameImg:Any!
    var zoomType:ZoomScanType = .Doc
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("Snapp Album/\(nameImg as! String)")
            let image    = UIImage(contentsOfFile: imageURL.path)
            
            self.imageScrollDoc.display(image: image!)
            self.imageScrollAO.display(image: image!)
            self.imageScrollEO.display(image: image!)
        }
    }
    
    // MARK: - func
    func setupView() {
        self.btnCloseUp.layer.cornerRadius = 30
        self.btnCloseUp.layer.masksToBounds = true
        
        switch zoomType {
        case .Doc:
            imageScrollDoc.isHidden = false
            imageScrollAO.isHidden = true
            imageScrollEO.isHidden = true
            
            imageScrollDoc.imageContentMode = .aspectFit
            imageScrollDoc.initialOffset = .center
        case .AO:
            imageScrollDoc.isHidden = false
            imageScrollAO.isHidden = true
            imageScrollEO.isHidden = true
            
//            imageScrollAO.imageContentMode = .widthFill
//            imageScrollAO.initialOffset = .center
            imageScrollDoc.imageContentMode = .aspectFit
            imageScrollDoc.initialOffset = .center
        case .EO:
            imageScrollDoc.isHidden = false
            imageScrollAO.isHidden = true
            imageScrollEO.isHidden = true
            
//            imageScrollEO.imageContentMode = .heightFill
//            imageScrollEO.initialOffset = .center
            imageScrollDoc.imageContentMode = .aspectFit
            imageScrollDoc.initialOffset = .center
        }
    }
    
    @IBAction func closeUpPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
