//
//  SNDetailWork_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/5/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNDetailWorkVC: SNCameraDocumentDelegate, SNCameraScanDelegate {
    //MARK: - SNCameraDocumentDelegate
    func cameraButtonDidToggle(arrayData: [Any]) {
        dataImgDocument.removeAll()
        print(dataImgDocument.count)
        dataImgDocument = arrayData
        self.collectionDocument.reloadData()
    }
    
    /*
    //MARK: - SNCameraTestResultDelegate
    func reloadDataTestResult(arrayData: [Any], selectionFlag: Int) {
        if selectionFlag == 0 {
            dataImgAC.removeAll()
            dataImgAC = arrayData
            print(dataImgAC.count)
        }else {
            dataImgEO.removeAll()
            dataImgEO = arrayData
            print(dataImgEO.count)
        }
        
        self.collectionTestResult.reloadData()
    }
     */
    
    //MARK: - SNCameraTestResultDelegate
    func reloadDataTestResult(_ array: [Any], selectionFlag: Int, dataCheckCondition: [Any]) {
        if selectionFlag == 0 {
            dataImgAC.removeAll()
            dataImgAC = array
            dataConditionAC.removeAll()
            dataConditionAC = dataCheckCondition
        }else {
            dataImgEO.removeAll()
            dataImgEO = array
            dataConditionEO.removeAll()
            dataConditionEO = dataCheckCondition
        }
        
        self.collectionTestResult.reloadData()
    }
}
