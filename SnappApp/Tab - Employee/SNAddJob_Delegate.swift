//
//  SNAddJob_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/7/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNAddJobVC: DateSettingDelegate {
    //MARK: - DateSettingDelegate
    func dateChangeButtonDidToggle(strDate: String, strYear: Int, strMonth: Int, strDay: Int, strHour: Int, strMinute: String) {
        self.dateSave = "\(strYear)-\(strMonth)-\(strDay)"
        self.lbDate.text = strDate
    }
}
