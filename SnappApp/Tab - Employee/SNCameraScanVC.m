//
//  SNCameraScanVC.m
//  SnappApp
//
//  Created by Tu Ngo on 9/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

#import "SNCameraScanVC.h"

UIImage* originalImage;

float maxZoomFactor;
float zoomFactor;

Target *target;


//fk20180914
//int targetType = 0;     //0:1233LF 1:1250


@interface SNCameraScanVC () <SNScanDelegate>

@end


@implementation SNCameraScanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /*
    //fk20180914
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    targetType = appDelegate.targetFlag;
    */
    
#ifndef __i386__
    self.captureSession = [[AVCaptureSession alloc] init];
    //Optional: self.captureSession.sessionPreset = AVCaptureSessionPresetMedium;
    
    //fk20180815
    //    if ([self.captureSession canSetSessionPreset:AVCaptureSessionPresetPhoto] == YES ){
    if ([self.captureSession canSetSessionPreset:AVCaptureSessionPresetPhoto] == YES ){
        
        [self.captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
        printf("\nin AVCaptureSessionPreset\n");
        
    }
    
    self.videoCaptureDevice =
    [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    
    self.videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoCaptureDevice error:&error];
    
    if (self.videoInput)
    {
        [self.captureSession addInput:self.videoInput];
    }
    else
    {
        NSLog(@"Input Error: %@", error);
    }
    
    
    
    
    
    
    
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *stillImageOutputSettings =
    [[NSDictionary alloc] initWithObjectsAndKeys:
     AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [self.stillImageOutput setOutputSettings:stillImageOutputSettings];
    [self.captureSession addOutput:self.stillImageOutput];
    
    /*
     self.avCaptureOutput = [[AVCapturePhotoOutput alloc]init];
     self.avSettings = [AVCapturePhotoSettings photoSettings];
     
     //    AVCaptureSession* captureSession = [[AVCaptureSession alloc] init];
     //    [captureSession startRunning];
     
     //    [self.avCaptureOutput capturePhotoWithSettings:self.avSettings delegate:self];
     
     AVCaptureConnection *connection = [self.avCaptureOutput connectionWithMediaType:AVMediaTypeVideo];
     
     if (connection.active)
     {
     //connection is active
     NSLog(@"Connection is active");
     
     id previewPixelType = _avSettings.availablePreviewPhotoPixelFormatTypes.firstObject;
     NSDictionary *format = @{(NSString*)kCVPixelBufferPixelFormatTypeKey:previewPixelType,(NSString*)kCVPixelBufferWidthKey:@160,(NSString*)kCVPixelBufferHeightKey:@160};
     
     _avSettings.previewPhotoFormat = format;
     
     [_avCaptureOutput capturePhotoWithSettings:_avSettings delegate:self];
     
     
     }
     else
     {
     NSLog(@"Connection is not active");
     //connection is not active
     //try to change self.captureSession.sessionPreset,
     //or change videoDevice.activeFormat
     }
     
     */
    
    
    
    
    
    AVCaptureVideoPreviewLayer *previewLayer =
    [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    UIView *aView = self.view;
    
    
    //fk20180815
    /*
    if (self.targetType == 0) {
        previewLayer.frame = CGRectMake(85, 200, 600, 500);
    }else {
        previewLayer.frame = CGRectMake(320, 200, 130, 800);
    }
     */
    previewLayer.frame = CGRectMake(0, 104, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 104);
    
    
    //    CGRectMake(0, ((self.view.frame.size.height-self.view.frame.size.height*(1920.0/1080.0))/2.0), self.view.frame.size.width, self.view.frame.size.height*(1920.0/1080.0));
    
    //self.view.bounds;
    
    //fk20180814
    //    CGRectMake((-1) * (self.view.frame.size.width / 2), (-1) * (self.view.frame.size.height / 2) - 45, 2 * self.view.frame.size.width, 2 * self.view.frame.size.height);
    //    CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height-120);
    
    //fk20180815
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    //    previewLayer.videoGravity = AVLayerVideoGravityResize;
    //    previewLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    
    [aView.layer insertSublayer:previewLayer atIndex:0];
    
    
    aView.clipsToBounds = YES;
    
#endif
    
    
    //fk20180914
    [target removeFromSuperview];
    
    
    //fk20180814
    //    target =  [[Target alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-160, ((self.view.frame.size.height-70)/2)-200, 320, 360)];
    target =  [[Target alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    target.targetFlag = self.targetType;
    //    target =  [[Target alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-160, ((self.view.frame.size.height-70)/2)-500, 320, 1000)];
    
    
    //    [self.view addSubview:target];
    [aView.layer insertSublayer:target.layer atIndex:1];
    
    
    //fk20180914
    //fk20180816
    //    if (picsFlag == 1){
    if (self.targetType == 0){
        
        self.pics1233.alpha = 0.5;
        self.pics1250.alpha = 0.0;
        
    }else{
        
        self.pics1233.alpha = 0.0;
        self.pics1250.alpha = 0.5;
        
    }
    //*/
    
    
    
    
    if (self.videoInput){
        
        [self.captureSession startRunning];
        
    }
    
    
    maxZoomFactor = 4.0;
    //    zoomFactor = 2.0;
    //    zoomFactor = 1.0;               //fk20150925
    
    
    //fk20180814
    zoomFactor = 1.4;                   //fk20151021
    //    zoomFactor = 1.0;
    
    [self zoomSet:zoomFactor];
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    [self.captureSession stopRunning];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

int captureFlag = 0;

- (IBAction)capture:(id)sender {
    
    //fk20180815
    [self captureStillImage];
    captureFlag =1;
    //    [self.avCaptureOutput capturePhotoWithSettings:self.avSettings delegate:self];
    
}

- (IBAction)captureTwo:(id)sender {
    [self captureStillImage];
    captureFlag =1;
}



-(void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error
{
    
    
    if (error) {
        NSLog(@"error : %@", error.localizedDescription);
    }
    
    if (photoSampleBuffer) {
        NSData *data = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
        UIImage *image = [UIImage imageWithData:data];
        
        
        if (captureFlag == 1){
            captureFlag = 0;
            
            printf("width:%f height:%f", image.size.width, image.size.height);
            
            
            /*
             //             CGRect cropRect = CGRectMake(0, 0, image.size.height, image.size.width);
             CGRect cropRect = CGRectMake(0, 0, 3024, 4032);
             CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
             UIImage *croppedImage = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation]; // always UIImageOrientationRight
             CGImageRelease(imageRef);
             */
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeImageToSavedPhotosAlbum:[image CGImage]
                                      orientation:(ALAssetOrientation)[image imageOrientation]
                                  completionBlock:^(NSURL *assetURL, NSError *error)
             //        [library writeImageToSavedPhotosAlbum:[croppedImage CGImage]
             //                                  orientation:(ALAssetOrientation)[croppedImage imageOrientation]
             //                              completionBlock:^(NSURL *assetURL, NSError *error)
             
             
             
             
             
             
             
             {
                 UIAlertView *alert;
                 if (!error)
                 {
                     alert = [[UIAlertView alloc] initWithTitle:@"Photo Saved"
                                                        message:@"The photo was successfully saved to your photos library"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
                 }
                 else
                 {
                     alert = [[UIAlertView alloc] initWithTitle:@"Error Saving Photo"
                                                        message:@"The photo was not saved to your photos library"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
                 }
                 
                 [alert show];
             }
             ];
            
        }
        else
        {
            NSLog(@"Error capturing still image: %@", error);
        }
        
    }
    
}



- (void) captureStillImage
{
    
    AVCaptureConnection *stillImageConnection =
    [self.stillImageOutput.connections objectAtIndex:0];
    if ([stillImageConnection isVideoOrientationSupported])
        [stillImageConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    
    [self.stillImageOutput
     captureStillImageAsynchronouslyFromConnection:stillImageConnection
     completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
     {
         if (imageDataSampleBuffer != NULL)
         {
             NSData *imageData = [AVCaptureStillImageOutput
                                  jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
             ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             /*
             image = [self imageWithImage:image convertToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
             printf("width:%f height:%f", image.size.width, image.size.height);
             
             if (self.targetType == 0) {
                 image = [self croppIngimageByImageName:image toRect:CGRectMake(85, 200 - 50, 600, 500)];
             }else {
                 image = [self croppIngimageByImageName:image toRect:CGRectMake(320, 200 - 50, 130, 800)];
             }
             */
             [self goScanVC:image];
             
         }
         else
         {
             NSLog(@"Error capturing still image: %@", error);
         }
         
     }
     
     ];
    
}

- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)goScanVC:(UIImage *)imageCapture {
    SNScanVC *scanVC = [[UIStoryboard storyboardWithName:@"Employee" bundle:nil] instantiateViewControllerWithIdentifier:@"SNScanVC"];
    scanVC.delegate = self;
    scanVC.dataWork = self.dataWork;
    scanVC.arrayImg = self.arrayImg;
    scanVC.indexArray = self.indexArray;
    scanVC.originalImage = imageCapture;
    scanVC.selectionFlag = self.targetType;
    /*
    if (self.targetType == 0) {
        scanVC.widthViewScan = 600;
        scanVC.heightViewScan = 500;
    }else {
        scanVC.widthViewScan = 130;
        scanVC.heightViewScan = 800;
    }
     */
    [self.navigationController pushViewController:scanVC animated:true];
}


- (void)reloadDataTestResult:(NSArray *)array selectionFlag:(NSInteger)selectionFlag dataCheckCondition:(NSArray *)dataCheckCondition {
    [self.delegate reloadDataTestResult:array selectionFlag:selectionFlag dataCheckCondition:dataCheckCondition];
}

/*
- (IBAction)sliderChanged:(id)sender {
    
    float zoomFactorTemp = pow(maxZoomFactor, self.slider.value);
    self.slider.value = log(zoomFactorTemp)/log(maxZoomFactor);
    
    [self zoomSet:zoomFactorTemp];
    
}
*/

- (void)zoomSet:(float)zoomFactorSet{
    
    float zoomFactorTemp = pow(maxZoomFactor, log(zoomFactorSet)/log(maxZoomFactor));
    //self.slider.value = log(zoomFactorTemp)/log(maxZoomFactor);
    
    float zoomLevel = zoomFactorSet;
    
    if ([self.videoCaptureDevice respondsToSelector:@selector(setVideoZoomFactor:)]) {
        
        printf("YES in setVideoZoomFactor\n");
        
        if ([self.videoCaptureDevice lockForConfiguration:nil]) {
            
            //fk20180815
            //            self.videoCaptureDevice.activeFormat = [self.videoCaptureDevice.formats objectAtIndex:10];
            
            NSLog(@"Selected format:\n%@,\nmax zoom factor: %f\n", self.videoCaptureDevice.activeFormat, self.videoCaptureDevice.activeFormat.videoMaxZoomFactor);
            
            float zoomFactor = self.videoCaptureDevice.activeFormat.videoZoomFactorUpscaleThreshold;
            
            printf("Lossless Max Zoom Factor:%.1f\n", zoomFactor);
            
            [self.videoCaptureDevice setVideoZoomFactor:zoomLevel];
            
            printf("VideoZoomFactor:%.2f\n", self.videoCaptureDevice.videoZoomFactor);
            
        }
        
        
        //fk20180814
        //        self.actualZoomFactor.text = [NSString stringWithFormat:@"x%0.1f", zoomLevel*2.0];
        //self.actualZoomFactor.text = [NSString stringWithFormat:@"x%0.1f", zoomLevel];
        
        [self.videoCaptureDevice unlockForConfiguration];
        
    }
    
}

//int picsFlag = 2;
/*
- (IBAction)picsSelected:(id)sender {
    
    UISwitch *switchStatusTemp = (UISwitch*)sender;
    
    //スイッチがONとOFFの場合で場合分け
    if(switchStatusTemp.on == YES){
        
        //ここにスイッチがONになったときにやりたいことを記述
        NSLog(@"スイッチがONになりました");
        //        picsFlag = 1;
        
        self.pics1233.alpha = 0.5;
        self.pics1250.alpha = 0.0;
        
        
        //fk20180914
        self.targetType = 0;
        //appDelegate.targetFlag = 0;
        
        
    }else{
        
        //ここにスイッチがOFFになったときにやりたいことを記述
        NSLog(@"スイッチがOFFになりました");
        //        picsFlag = 0;
        
        self.pics1233.alpha = 0.0;
        self.pics1250.alpha = 0.5;
        
        
        //fk20180914
        self.targetType = 1;
        //appDelegate.targetFlag = 1;
        
        
    }
    
    [self viewDidLoad];         //fk20180914 復活
    
}
 */

- (IBAction)backPress:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}



@end
