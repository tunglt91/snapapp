//
//  LocalData.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/6/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import CoreData

class LocalData: NSObject {
    static let sharedInstance = LocalData()
        
    func dataWork() -> [ModelWork] {
        var arrayData:[ModelWork] = []
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ModelWork")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            //print(result.count)
            arrayData = result as! [ModelWork]
            
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "doc_rec") ?? "")
            }
        } catch {
            print("Failed")
        }
        
        return arrayData
    }
}

class InfoModel: NSObject, NSCoding {
    var entrant:String = ""
    var responsiblePerson:String = ""
    var dateOfWork:String = ""
    var useSterilizer:String = ""
    var sterilizerNumber:String = ""
    var operationFrequency:String = ""
    var startTime:String = ""
    var endTime:String = ""
    var ci_indicator_check:String = ""
    var ci_BDtest:String = ""
    var ci_Card:String = ""
    var bi_indicator_check:String = ""
    var loadNo:String = ""
    
    override init() {
        super.init()
        entrant = ""
        responsiblePerson = ""
        dateOfWork = currentDate()
        useSterilizer = ""
        sterilizerNumber = ""
        operationFrequency = "0回目"
        startTime = currentTime()
        endTime = ""
        ci_indicator_check = ""
        ci_BDtest = ""
        ci_Card = ""
        bi_indicator_check = ""
        loadNo = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.entrant = aDecoder.decodeObject(forKey: "entrant") as? String ?? ""
        self.responsiblePerson = aDecoder.decodeObject(forKey: "responsiblePerson") as? String ?? ""
        self.dateOfWork = aDecoder.decodeObject(forKey: "dateOfWork") as? String ?? ""
        self.useSterilizer = aDecoder.decodeObject(forKey: "useSterilizer") as? String ?? ""
        self.sterilizerNumber = aDecoder.decodeObject(forKey: "sterilizerNumber") as? String ?? ""
        self.operationFrequency = aDecoder.decodeObject(forKey: "operationFrequency") as? String ?? ""
        self.startTime = aDecoder.decodeObject(forKey: "startTime") as? String ?? ""
        self.endTime = aDecoder.decodeObject(forKey: "endTime") as? String ?? ""
        self.ci_indicator_check = aDecoder.decodeObject(forKey: "ci_indicator_check") as? String ?? ""
        self.ci_BDtest = aDecoder.decodeObject(forKey: "ci_BDtest") as? String ?? ""
        self.ci_Card = aDecoder.decodeObject(forKey: "ci_Card") as? String ?? ""
        self.bi_indicator_check = aDecoder.decodeObject(forKey: "bi_indicator_check") as? String ?? ""
        self.loadNo = aDecoder.decodeObject(forKey: "loadNo") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(entrant, forKey: "entrant")
        coder.encode(responsiblePerson, forKey: "responsiblePerson")
        coder.encode(dateOfWork, forKey: "dateOfWork")
        coder.encode(useSterilizer, forKey: "useSterilizer")
        coder.encode(sterilizerNumber, forKey: "sterilizerNumber")
        coder.encode(operationFrequency, forKey: "operationFrequency")
        coder.encode(startTime, forKey: "startTime")
        coder.encode(endTime, forKey: "endTime")
        coder.encode(ci_indicator_check, forKey: "ci_indicator_check")
        coder.encode(ci_BDtest, forKey: "ci_BDtest")
        coder.encode(ci_Card, forKey: "ci_Card")
        coder.encode(bi_indicator_check, forKey: "bi_indicator_check")
        coder.encode(loadNo, forKey: "loadNo")
    }
    
    func currentDate() -> String {
        //Current Date
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return "\(year)年\(month)月\(day)日"
    }
    
    func currentTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let minuteFormat = String(format: "%02d", minute)
        return "\(hour):\(minuteFormat)"
    }
}

class PhysicalModel: NSObject, NSCoding {
    var vapor_pressure:String = ""
    var sterilization_temperature:String = ""
    var sterilization_time:String = ""
    
    override init() {
        super.init()
        vapor_pressure = ""
        sterilization_temperature = ""
        sterilization_time = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.vapor_pressure = aDecoder.decodeObject(forKey: "vapor_pressure") as? String ?? ""
        self.sterilization_temperature = aDecoder.decodeObject(forKey: "sterilization_temperature") as? String ?? ""
        self.sterilization_time = aDecoder.decodeObject(forKey: "sterilization_time") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(vapor_pressure, forKey: "vapor_pressure")
        coder.encode(sterilization_temperature, forKey: "sterilization_temperature")
        coder.encode(sterilization_time, forKey: "sterilization_time")
    }
}

class ChemicalModel: NSObject, NSCoding {
    var bd_test:String = ""
    var indicator:String = ""
    
    override init() {
        super.init()
        bd_test = ""
        indicator = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.bd_test = aDecoder.decodeObject(forKey: "bd_test") as? String ?? ""
        self.indicator = aDecoder.decodeObject(forKey: "indicator") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(bd_test, forKey: "bd_test")
        coder.encode(indicator, forKey: "indicator")
    }
}

class BiologicalModel: NSObject, NSCoding {
    var positive_control:String = ""
    var insertion_position:String = ""
    var judgment:String = ""
    var cultivation_start_time:String = ""
    var result_check_time:String = ""
    
    override init() {
        super.init()
        positive_control = ""
        insertion_position = ""
        judgment = ""
        cultivation_start_time = ""
        result_check_time = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.positive_control = aDecoder.decodeObject(forKey: "positive_control") as? String ?? ""
        self.insertion_position = aDecoder.decodeObject(forKey: "insertion_position") as? String ?? ""
        self.judgment = aDecoder.decodeObject(forKey: "judgment") as? String ?? ""
        self.cultivation_start_time = aDecoder.decodeObject(forKey: "cultivation_start_time") as? String ?? ""
        self.result_check_time = aDecoder.decodeObject(forKey: "result_check_time") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(positive_control, forKey: "positive_control")
        coder.encode(insertion_position, forKey: "insertion_position")
        coder.encode(judgment, forKey: "judgment")
        coder.encode(cultivation_start_time, forKey: "cultivation_start_time")
        coder.encode(result_check_time, forKey: "result_check_time")
    }
    
    func currentTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let minuteFormat = String(format: "%02d", minute)
        return "\(hour):\(minuteFormat)"
    }
}

class SterilizationModel: NSObject, NSCoding {
    var curInfo:InfoModel!
    var curPhysical:PhysicalModel!
    var curChemical:ChemicalModel!
    var curBiological:BiologicalModel!
    var curListDevice:[DeviceModel] = []
    
    override init() {
        super.init()
    }
    
    convenience init(info: InfoModel, physical: PhysicalModel, chemical: ChemicalModel, biological: BiologicalModel, listDevice:[DeviceModel]) {
        self.init()
        self.curInfo = info
        self.curPhysical = physical
        self.curChemical = chemical
        self.curBiological = biological
        self.curListDevice = listDevice
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.curInfo = aDecoder.decodeObject(forKey: "curInfo") as? InfoModel
        self.curPhysical = aDecoder.decodeObject(forKey: "curPhysical") as? PhysicalModel
        self.curChemical = aDecoder.decodeObject(forKey: "curChemical") as? ChemicalModel
        self.curBiological = aDecoder.decodeObject(forKey: "curBiological") as? BiologicalModel
        self.curListDevice = aDecoder.decodeObject(forKey: "curListDevice") as? [DeviceModel] ?? []
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(curInfo, forKey: "curInfo")
        coder.encode(curPhysical, forKey: "curPhysical")
        coder.encode(curChemical, forKey: "curChemical")
        coder.encode(curBiological, forKey: "curBiological")
        coder.encode(curListDevice, forKey: "curListDevice")
    }
}

class DeviceModel: NSObject, NSCoding {
    var part:String = ""
    var technical_equipment:String = ""
    var amount:String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(inputPart: String, inputTechnical_equipment: String, inputAmount: String) {
        self.init()
        self.part = inputPart
        self.technical_equipment = inputTechnical_equipment
        self.amount = inputAmount
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.part = aDecoder.decodeObject(forKey: "part") as? String ?? ""
        self.technical_equipment = aDecoder.decodeObject(forKey: "technical_equipment") as? String ?? ""
        self.amount = aDecoder.decodeObject(forKey: "amount") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(part, forKey: "part")
        coder.encode(technical_equipment, forKey: "technical_equipment")
        coder.encode(amount, forKey: "amount")
    }
}
