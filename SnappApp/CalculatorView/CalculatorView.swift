//
//  CalculatorView.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/5/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol CalculatorDelegate: class {
    func backButtonDidToggle()
}

class CalculatorView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewCalculator: UIView!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var viewBorderPicker: UIView!
    
    var arrayPicker:[String] = ["器具単品の個数", "気管切開セット", "Wルーメンセット", "PTCDセット", "皮膚科縫合セット"]
    
    var delegate:CalculatorDelegate?
    
    // Calculator
    var numberOnScreen:Int = 0
    var previousNumber:Int = 0
    var performingMath = false
    var operation = 0
    
    var firstClick:Bool = true
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("CalculatorView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        self.viewBorderPicker.layer.borderWidth = 1.5
        self.viewBorderPicker.layer.borderColor = UIColor.init(hexString: "707070").cgColor
    }
    
    func updateView() {
        for view in self.viewCalculator.subviews as [UIView] {
            for subview in view.subviews as [UIView] {
                if let btn = subview as? UIButton {
                    btn.layer.borderWidth = 0.5
                    btn.layer.borderColor = UIColor.white.cgColor
                    btn.layer.masksToBounds = true
                }
            }
        }
    }
    
    
    //MARK: - IBAction
    @IBAction func closeUpPress(_ sender: Any) {
        self.delegate?.backButtonDidToggle()
    }
    
    @IBAction func numbers(_ sender: UIButton)
    {
        if firstClick {
            firstClick = false
            lbTotal.text = ""
        }
        
        if performingMath == true
        {
            lbTotal.text = String(sender.tag)
            numberOnScreen = Int(lbTotal.text!)!
            performingMath = false
        }
        else
        {
            lbTotal.text = lbTotal.text! + String(sender.tag)
            numberOnScreen = Int(lbTotal.text!)!
        }
    }
    
    @IBAction func buttons(_ sender: UIButton)
    {
        if lbTotal.text != "" && sender.tag != 11 && sender.tag != 16
        {
            previousNumber = Int(lbTotal.text!)!
            
            if sender.tag == 12 //Divide
            {
                lbTotal.text = "/";
            }
            else if sender.tag == 13 //Multiply
            {
                lbTotal.text = "x";
            }
            else if sender.tag == 14 //Minus
            {
                lbTotal.text = "-";
            }
            else if sender.tag == 15 //Plus
            {
                lbTotal.text = "+";
            }
            
            operation = sender.tag
            performingMath = true;
        }
        else if sender.tag == 16
        {
            if operation == 12
            {
                lbTotal.text = String(previousNumber / numberOnScreen)
            }
            else if operation == 13
            {
                lbTotal.text = String(previousNumber * numberOnScreen)
            }
            else if operation == 14
            {
                lbTotal.text = String(previousNumber - numberOnScreen)
            }
            else if operation == 15
            {
                lbTotal.text = String(previousNumber + numberOnScreen)
            }
        }
        else if sender.tag == 11
        {
            firstClick = true
            lbTotal.text = "0"
            previousNumber = 0;
            numberOnScreen = 0;
            operation = 0;
        }
    }
    

}
