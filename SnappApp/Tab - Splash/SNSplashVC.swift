//
//  SNSplashVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/21/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class SNSplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let storyboard = UIStoryboard(name: "Employee", bundle: nil)
        let listJobVC = storyboard.instantiateViewController(withIdentifier: "SNListJobVC") as? SNListJobVC
        self.navigationController?.pushViewController(listJobVC!, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
