//
//  SNTopMenuAdminVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class SNTopMenuAdminVC: UIViewController {
    @IBOutlet weak var viewToolCount: UIView!
    @IBOutlet weak var viewGeneral: UIView!
    @IBOutlet weak var viewSetting: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }

    //MARK: - IBAction
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Func
    func setupView() {
        self.viewToolCount.layer.cornerRadius = 8
        self.viewToolCount.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewToolCount.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.viewToolCount.layer.shadowOpacity = 1
        self.viewToolCount.layer.shadowRadius = 1
        self.viewToolCount.layer.masksToBounds = false
        
        self.viewGeneral.layer.cornerRadius = 8
        self.viewGeneral.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewGeneral.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.viewGeneral.layer.shadowOpacity = 1
        self.viewGeneral.layer.shadowRadius = 1
        self.viewGeneral.layer.masksToBounds = false
        
        self.viewSetting.layer.cornerRadius = 8
        self.viewSetting.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewSetting.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.viewSetting.layer.shadowOpacity = 1
        self.viewSetting.layer.shadowRadius = 1
        self.viewSetting.layer.masksToBounds = false
        
        let gestureTool = UITapGestureRecognizer(target: self, action:  #selector (self.showToolCountPress (_:)))
        self.viewToolCount.addGestureRecognizer(gestureTool)
        
        let gestureGeneral = UITapGestureRecognizer(target: self, action:  #selector (self.showGeneralPress (_:)))
        self.viewGeneral.addGestureRecognizer(gestureGeneral)
        
        let gestureSetting = UITapGestureRecognizer(target: self, action:  #selector (self.showSettingPress (_:)))
        self.viewSetting.addGestureRecognizer(gestureSetting)
    }
    
    //MARK: -IBAction
    @objc func showToolCountPress(_ sender:UITapGestureRecognizer){
        /*
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let toolCountVC = storyboard.instantiateViewController(withIdentifier: "SNListJobAdminVC") as? SNListJobAdminVC
        self.navigationController?.pushViewController(toolCountVC!, animated: true)
         */
    }
    
    @objc func showGeneralPress(_ sender:UITapGestureRecognizer){
        
    }
    
    @objc func showSettingPress(_ sender:UITapGestureRecognizer){
        // do other task
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
