//
//  TotalCenterCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol TotalCenterCellDelegate: class {
    func selectImageDetail(index: Int)
}

class TotalCenterCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataImgDocument:[Any] = []
    var delegate:TotalCenterCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let nib = UINib(nibName: "DocumentationCell", bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: "DocumentationCell")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.collectionView.collectionViewLayout.invalidateLayout()
        
        self.setupGirdView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupGirdView() {
        //let width: CGFloat = (self.frame.size.width - 5) / 4
        
        let flow = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        flow?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flow?.itemSize = CGSize(width: 150, height: 200)
        flow?.minimumLineSpacing = 30
        flow?.minimumInteritemSpacing = 30
        flow?.scrollDirection = .horizontal
        collectionView?.isPagingEnabled = false
    }
    
}

extension TotalCenterCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataImgDocument.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell:DocumentationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentationCell", for: indexPath) as! DocumentationCell
    
            let data = dataImgDocument[indexPath.row] as! String
            if data.isEmpty {
                cell.imgDocument.isHidden = true
                cell.lbTitle.isHidden = true
            }else {
                cell.imgDocument.isHidden = false
                cell.lbTitle.isHidden = false
            }
        
            if indexPath.row == 0 {
                let dataIndex0 = dataImgDocument[0] as! String
                if dataIndex0 == "noImg" {
                    cell.imgDocument.isHidden = true
                    cell.lbTitle.isHidden = true
                }else {
                    cell.imgDocument.isHidden = false
                    cell.lbTitle.isHidden = false
                }
            }
        
            cell.data = dataImgDocument[indexPath.row]
            cell.lbTitle.text = "器材記録 \(indexPath.row + 1)"
        
            cell.viewCondition.isHidden = true
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.delegate?.selectImageDetail(index: indexPath.row)
    }
}

