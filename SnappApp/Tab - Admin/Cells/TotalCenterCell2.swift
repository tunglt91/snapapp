//
//  TotalCenterCell2.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/5/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class TotalCenterCell2: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lbTitle: UILabel!
    /*
    @IBOutlet weak var imgTest1: UIImageView!
    @IBOutlet weak var imgTest2: UIImageView!
    @IBOutlet weak var imgTest3: UIImageView!
    @IBOutlet weak var lbTest1: UILabel!
    @IBOutlet weak var lbTest2: UILabel!
    @IBOutlet weak var lbTest3: UILabel!
    */
    
    var arrayData:[Any] = []
    var dataCheckCondition:[Any] = []
    var arrayTitle:[String] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let nib = UINib(nibName: "DocumentationCell", bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: "DocumentationCell")
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.collectionView.collectionViewLayout.invalidateLayout()
        
        self.setupGirdView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupGirdView() {
        //let width: CGFloat = (self.frame.size.width - 4) / 5
        
        let flow = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        flow?.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flow?.itemSize = CGSize(width: 150, height: 200)
        flow?.minimumLineSpacing = 30
        flow?.minimumInteritemSpacing = 30
        flow?.scrollDirection = .horizontal
        collectionView?.isPagingEnabled = false
    }
    
}


extension TotalCenterCell2: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:DocumentationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentationCell", for: indexPath) as! DocumentationCell
        
        cell.data = arrayData[indexPath.row]
        cell.lbTitle.text = arrayTitle[indexPath.row]
        
        if dataCheckCondition.count > 0 {
            let checkCondition = dataCheckCondition[indexPath.row] as! Int
            if checkCondition == 0 {
                cell.viewCondition.isHidden = true
            }else {
                cell.viewCondition.isHidden = false
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
