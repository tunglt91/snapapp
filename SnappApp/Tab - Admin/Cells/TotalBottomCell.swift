//
//  TotalBottomCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/5/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol TotalBottomCellDelegate: class {
    func backButtonDidToggle(cell:TotalBottomCell)
}

class TotalBottomCell: UITableViewCell {
    @IBOutlet weak var btnCloseUp: UIButton!
    
    var delegate:TotalBottomCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnCloseUp.layer.cornerRadius = 30
        self.btnCloseUp.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func closeUpPress(_ sender: Any) {
        self.delegate?.backButtonDidToggle(cell: self)
    }
    
}
