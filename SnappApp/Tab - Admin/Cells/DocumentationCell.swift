//
//  DocumentationCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/27/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit



class DocumentationCell: UICollectionViewCell {
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewCondition: UIView!
    
    var data:Any? {
        didSet {
            self.updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgDocument.layer.borderWidth = 0.5
        self.imgDocument.layer.borderColor = UIColor.init(hexString: "707070").cgColor
        self.imgDocument.contentMode = .scaleAspectFit
    }
    
    func updateView() {
        SNCommon.getImageDirectory(imgView: self.imgDocument, nameImg: data!)
    }
}
