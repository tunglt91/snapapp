//
//  TotalHeaderCell.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

protocol TotalHeaderCellDelegate: class {
    //func calculatorButtonDidToggle(cell:TotalHeaderCell)
    func nextButtonDidToggle(cell:TotalHeaderCell)
    func previousButtonDidToggle(cell:TotalHeaderCell)
}

class TotalHeaderCell: UITableViewCell {
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbLotOfNumber: UILabel!
    @IBOutlet weak var lbNameOfSterilizer: UILabel!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var delegate:TotalHeaderCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - IBAction
    @IBAction func nextPress(_ sender: Any) {
        self.delegate?.nextButtonDidToggle(cell: self)
    }
    
    @IBAction func previousPress(_ sender: Any) {
        self.delegate?.previousButtonDidToggle(cell: self)
    }
    
    //MARK: - Func
    func setupView() {
        self.btnNext.layer.cornerRadius = 30
        self.btnNext.layer.masksToBounds = true
        self.btnPrevious.layer.cornerRadius = 30
        self.btnPrevious.layer.masksToBounds = true
    }
    
    
}
