//
//  SNShowItemVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import VisualEffectView

class SNShowItemVC: UIViewController {
    @IBOutlet weak var viewListItem: UIView!
    @IBOutlet weak var btnCloseUp: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }

    //MARK: - IBAction
    @IBAction func closeUpPress(_ sender: Any) {
        self.dismiss()
    }
    
    //MARK: - Func
    func setupView() {
        self.btnCloseUp.layer.cornerRadius = 30
        self.btnCloseUp.layer.masksToBounds = true
        
        self.viewListItem.layer.cornerRadius = 8
        self.viewListItem.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        let visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        
        // Configure the view with tint color, blur radius, etc
        visualEffectView.colorTint = UIColor.white
        visualEffectView.colorTintAlpha = 0.1
        visualEffectView.blurRadius = 10
        visualEffectView.scale = 1
        
        self.view.addSubview(visualEffectView)
        self.view.addSubview(viewContent)
    }
    
    func showInVC(vc: UIViewController){
        vc.addChildViewController(self)
        vc.view.addSubview(self.view)
        self.didMove(toParentViewController: vc)
    }
    
    func dismiss() {
        self.willMove(toParentViewController: nil)
        self.view .removeFromSuperview()
        self.removeFromParentViewController()
    }
    
}
