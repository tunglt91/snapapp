//
//  SNTotalSterilizationVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

class SNTotalSterilizationVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnToolCount: UIButton!
    
    lazy var keyboardSupportView:UIView = {
        let viewX = UIView.init()
        viewX.backgroundColor = UIColor.clear
        viewX.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(viewX)
        viewX.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        viewX.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        viewX.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        viewX.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        /*
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector (self.handleTapViewX(_:)))
        viewX.addGestureRecognizer(tapGesture)
         */
        return viewX
    }()
    
    var arrayTitle:[String] = ["AC", "EO", "過酸化水素", "ホルマリン"]
    var calculatorView:CalculatorView = CalculatorView()
    
    var check1stLoad:Bool = true
    var dataModel:ModelWork = ModelWork()
    // arrayData Image
    var dataImgDocument:[Any] = []
    var dataImgAC:[Any] = []
    var dataImgEO:[Any] = []
    var dataImgHydro:[Any] = []
    var dataImgFormalin:[Any] = []
    
    // arrayData Condition
    var dataCheckConditionAC:[Any] = []
    var dataCheckConditionEO:[Any] = []
    
    var indexDocument:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if check1stLoad {
            check1stLoad = false
            dataImgDocument = dataModel.value(forKey: "doc_rec") as! [Any]
            dataImgAC = dataModel.value(forKey: "results_rec_ac") as! [Any]
            dataImgEO = dataModel.value(forKey: "results_rec_eo") as! [Any]
            dataImgHydro = dataModel.value(forKey: "results_rec_hydro") as! [Any]
            dataImgFormalin = dataModel.value(forKey: "results_rec_formalin") as! [Any]
            dataCheckConditionAC = dataModel.value(forKey: "check_condition_ac") as! [Any]
            dataCheckConditionEO = dataModel.value(forKey: "check_condition_eo") as! [Any]
            
            //Check Data có phải mới khởi tạo không
            if (dataImgDocument[0] as! String).isEmpty{
                dataImgDocument.removeAll()
            }
            if (dataImgAC[0] as! String).isEmpty{
                dataImgAC.removeAll()
            }
            if (dataImgEO[0] as! String).isEmpty{
                dataImgEO.removeAll()
            }
            if (dataImgHydro[0] as! String).isEmpty{
                dataImgHydro.removeAll()
            }
            if (dataImgFormalin[0] as! String).isEmpty{
                dataImgFormalin.removeAll()
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func showItemPress(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SNShowItemVC") as? SNShowItemVC
        vc?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        vc?.showInVC(vc: self)
    }
    
    @IBAction func toolCountPress(_ sender: Any) {
        self.btnToolCount.isHidden = true
        self.view.bringSubview(toFront: self.keyboardSupportView)
        self.keyboardSupportView.isHidden = false
        
        calculatorView = CalculatorView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 190, width: UIScreen.main.bounds.width, height: 190))
        calculatorView.updateView()
        calculatorView.delegate = self
        self.view.addSubview(calculatorView)
    }
    
    
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Func
    func setupView() {
        let headerNib = UINib(nibName: "TotalHeaderCell", bundle: nil)
        self.tableView.register(headerNib, forCellReuseIdentifier: "TotalHeaderCell")
        
        let centerNib = UINib(nibName: "TotalCenterCell", bundle: nil)
        self.tableView.register(centerNib, forCellReuseIdentifier: "TotalCenterCell")
        
        let center2Nib = UINib(nibName: "TotalCenterCell2", bundle: nil)
        self.tableView.register(center2Nib, forCellReuseIdentifier: "TotalCenterCell2")
        
        let bottomNib = UINib(nibName: "TotalBottomCell", bundle: nil)
        self.tableView.register(bottomNib, forCellReuseIdentifier: "TotalBottomCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.keyboardSupportView.isHidden = true
        self.btnToolCount.layer.cornerRadius = 20
        self.btnToolCount.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 8, scale: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
