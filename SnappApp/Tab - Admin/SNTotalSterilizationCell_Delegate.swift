//
//  SNTotalSterilizationCell_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/5/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNTotalSterilizationVC: TotalHeaderCellDelegate, TotalBottomCellDelegate, CalculatorDelegate, TotalCenterCellDelegate{
    //MARK: - TotalHeaderCellDelegate
    /*
    func calculatorButtonDidToggle(cell: TotalHeaderCell) {
        cell.btnToolCount.isHidden = true
        self.view.bringSubview(toFront: self.keyboardSupportView)
        self.keyboardSupportView.isHidden = false
        
        calculatorView = CalculatorView(frame: CGRect(x: 0, y: (UIScreen.main.bounds.height / 2) + 50, width: UIScreen.main.bounds.width, height: 200))
        calculatorView.updateView()
        calculatorView.delegate = self
        self.view.addSubview(calculatorView)
    }
 */
    
    func nextButtonDidToggle(cell: TotalHeaderCell) {        
        indexDocument += 1
        
        if indexDocument == dataImgDocument.count - 1 {
            cell.btnNext.isHidden = true
            cell.btnPrevious.isHidden = false
        }else {
            cell.btnNext.isHidden = false
            cell.btnPrevious.isHidden = false
        }
            
        if dataImgDocument.count > 0 {
            SNCommon.getImageDirectory(imgView: cell.imgDocument, nameImg: dataImgDocument[indexDocument])
        }
    }
    
    func previousButtonDidToggle(cell: TotalHeaderCell) {
        indexDocument -= 1
        
        if indexDocument == 0 {
            cell.btnPrevious.isHidden = true
            cell.btnNext.isHidden = false
        }else {
            cell.btnPrevious.isHidden = false
            cell.btnNext.isHidden = false
        }
        
        if dataImgDocument.count > 0 {
            SNCommon.getImageDirectory(imgView: cell.imgDocument, nameImg: dataImgDocument[indexDocument])
        }
    }
    
    //MARK: - TotalBottomCellDelegate
    func backButtonDidToggle(cell: TotalBottomCell) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - CalculatorDelegate
    func backButtonDidToggle() {
//        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TotalHeaderCell {
//            cell.btnToolCount.isHidden = false
//        }
        self.btnToolCount.isHidden = false
        
        self.keyboardSupportView.isHidden = true
        calculatorView.removeFromSuperview()
    }
    
    //MARK: - TotalCenterCellDelegate
    func selectImageDetail(index: Int) {
        indexDocument = index
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TotalHeaderCell {
            if dataImgDocument.count > 0 {
                cell.btnNext.isHidden = false
                cell.btnPrevious.isHidden = false
                
                if indexDocument == 0 {
                    cell.btnPrevious.isHidden = true
                }else {
                    cell.btnPrevious.isHidden = false
                }
                
                if indexDocument == dataImgDocument.count - 1 {
                    cell.btnNext.isHidden = true
                }else {
                    cell.btnNext.isHidden = false
                }
                
                SNCommon.getImageDirectory(imgView: cell.imgDocument, nameImg: dataImgDocument[indexDocument])
            }else {
                cell.btnNext.isHidden = true
                cell.btnPrevious.isHidden = true
            }
        }
    }
}
