//
//  SNListJobAdminVC.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit

fileprivate func parseDate(_ str : String) -> Date {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "yyyy-MM-dd"
    return dateFormat.date(from: str)!
}

fileprivate func firstDayOfMonth(date : Date) -> Date {
    
    let calendar = Calendar.current
    let components = calendar.dateComponents([.year, .month, .day], from: date)
    return calendar.date(from: components)!
    
}

class SNListJobAdminVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnShowAll: UIButton!
    @IBOutlet weak var btnShowLatestWeek: UIButton!
    @IBOutlet weak var btnShowOmission: UIButton!
    @IBOutlet weak var lineTab1: UIView!
    @IBOutlet weak var lineTab2: UIView!
    @IBOutlet weak var lineTab3: UIView!
    
    let attributeSelect : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 20),
        NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.black,
        NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) : NSUnderlineStyle.styleSingle.rawValue]
    
    let attributeNotSelect : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 20),
        NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.lightGray]
    
    var arrayWork:[ModelWork] = []
    var newWork:[ModelWork] = []
    var sections = [TableSection<Date, ModelWork>]()
    var check1stLoad:Bool = true
    var tabType:TabType = .Tab1
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if check1stLoad {
            check1stLoad = false
            arrayWork.removeAll()
            newWork.removeAll()
            self.sections.removeAll()
            arrayWork = LocalData.sharedInstance.dataWork()
            
            if arrayWork.count > 0 {
                //newWork
                newWork.append(arrayWork[arrayWork.count - 1])
                //xoa công việc mới tạo ở phần ở dưới
                arrayWork.remove(at: arrayWork.count - 1)
            }
            
            self.sections = TableSection.group(rowItems: self.arrayWork, by: { (headline) in
                return firstDayOfMonth(date: parseDate(headline.date!))
            })
            
            self.tableView.reloadData()
        }
    }
    
    //MARK: - IBAction
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: - Action Segment
    @IBAction func segmentedControlValueChanged(_ sender: UIButton) {
        if sender.tag == 0 {
            self.tabType = .Tab1
            
            self.lineTab1.isHidden = false
            self.lineTab2.isHidden = true
            self.lineTab3.isHidden = true
            self.btnShowAll.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.lightGray, for: .normal)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        else if sender.tag == 1 {
            self.tabType = .Tab2
            
            self.lineTab1.isHidden = true
            self.lineTab2.isHidden = false
            self.lineTab3.isHidden = true
            self.btnShowAll.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.lightGray, for: .normal)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeNotSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        else {
            self.tabType = .Tab3
            
            self.lineTab1.isHidden = true
            self.lineTab2.isHidden = true
            self.lineTab3.isHidden = false
            self.btnShowAll.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnShowLatestWeek.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnShowOmission.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
            /*
            let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeNotSelect)
            self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
            
            let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
            self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
            
            let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeSelect)
            self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
             */
        }
        
        self.tableView.reloadData()
    }
    
    //MARK: - Func
    func setupView() {
        let headerNib = UINib(nibName: "ListJobSectionCell", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "ListJobSectionCell")
        
        let nib = UINib(nibName: "ListJobCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ListJobCell")
        
        self.lineTab1.isHidden = false
        self.lineTab2.isHidden = true
        self.lineTab3.isHidden = true
        self.btnShowAll.setTitleColor(UIColor.init(hexString: "848484"), for: .normal)
        self.btnShowLatestWeek.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnShowOmission.setTitleColor(UIColor.lightGray, for: .normal)
        /*
        let attributedStringShowAll = NSAttributedString(string: "全てを表示", attributes: self.attributeSelect)
        self.btnShowAll.setAttributedTitle(attributedStringShowAll, for: .normal)
        
        let attributedStringShowLatestWeek = NSAttributedString(string: "最新の１週間を表示", attributes: self.attributeNotSelect)
        self.btnShowLatestWeek.setAttributedTitle(attributedStringShowLatestWeek, for: .normal)
        
        let attributedStringShowOmission = NSAttributedString(string: "撮影漏れを表示", attributes: self.attributeNotSelect)
        self.btnShowOmission.setAttributedTitle(attributedStringShowOmission, for: .normal)
         */
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
