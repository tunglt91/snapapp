//
//  SNListJobAdminTable_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/23/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNListJobAdminVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tabType == .Tab1 || tabType == .Tab3 {
            return self.sections.count + self.newWork.count
        }
        else {
            if self.sections.count >= 7 {
                return 6 + self.newWork.count
            }else {
                return self.sections.count + self.newWork.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ListJobSectionCell") as! ListJobSectionCell
        
        var date:Date = Date()
        if section == 0 {
            let arrayData = newWork[0]
            let strDate = arrayData.date
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            date = formatter.date(from: strDate!)!
        }else {
            let section = self.sections[(self.sections.count - 1) - (section - 1)]
            date = section.sectionItem
        }
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let weekendStr = calendar.component(.weekday, from: date)
        
        headerView.lbSection.text = "\(year)年\(month)月\(day)日(\(SNCommon.convertWeekdayJapan(weekDay: Int(weekendStr))))"
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return newWork.count
        }else {
            let section = self.sections[(self.sections.count - 1) - (section - 1)]
            return section.rowItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListJobCell") as! ListJobCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if indexPath.section == 0 {
            cell.imgNew.isHidden = false
            cell.data = newWork[0]
            cell.tabType = self.tabType
            cell.collectionView.reloadData()
        }else {
            cell.imgNew.isHidden = true
            let section = self.sections[(self.sections.count - 1) - (indexPath.section - 1)]
            let headline = section.rowItems[(section.rowItems.count - 1) - indexPath.row]
            cell.data = headline
            cell.tabType = self.tabType
            cell.collectionView.reloadData()
            
            /*
             if headline.date_current == arrayWork[arrayWork.count - 1].date_current {
             cell.imgNew.isHidden = false
             }else {
             cell.imgNew.isHidden = true
             }
             */
            
            /*
             cell.data = arrayWork[indexPath.row]
             cell.lbDate.text = headline.date
             cell.lbLotNumber.text = headline.lot_num
             cell.lbNameOfSterilizer.text = headline.name_of_ster
             */
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        var headline:ModelWork = ModelWork()
        if indexPath.section == 0 {
            headline = newWork[0]
        }else {
            let section = self.sections[(self.sections.count - 1) - (indexPath.section - 1)]
            headline = section.rowItems[(section.rowItems.count - 1) - indexPath.row]
        }
        self.showTotalSterilizationVC(dataWork: headline)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 310 * (UIScreen.main.bounds.size.width / 768.0) //330
        let width: CGFloat = ((UIScreen.main.bounds.width - 386) / 5)
        let heightRow = (width * 3) + 54
        return heightRow
    }
    
    func showTotalSterilizationVC(dataWork: ModelWork) {
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let totalSterilizationVC = storyboard.instantiateViewController(withIdentifier: "SNTotalSterilizationVC") as? SNTotalSterilizationVC
        totalSterilizationVC?.dataModel = dataWork
        self.navigationController?.pushViewController(totalSterilizationVC!, animated: true)
    }
}
