//
//  SNTotalSterilizationTable_Delegate.swift
//  SnappApp
//
//  Created by Tu Ngo on 8/24/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import Foundation
import UIKit

extension SNTotalSterilizationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView:UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 30))
        return footerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalHeaderCell") as! TotalHeaderCell
            cell.delegate = self
            
            let date = self.parseDate(dataModel.date!)
            
            let calendar = Calendar.current
            
            let year = calendar.component(.year, from: date)
            let month = calendar.component(.month, from: date)
            let day = calendar.component(.day, from: date)
            
            cell.lbDate.text = "\(year)年\(month)月\(day)日"
            
            cell.lbLotOfNumber.text = dataModel.lot_num
            cell.lbNameOfSterilizer.text = dataModel.name_of_ster
            
            if dataImgDocument.count > 0 {
                cell.btnNext.isHidden = false
                cell.btnPrevious.isHidden = false
                
                let data2 = dataImgDocument[1] as! String
                if data2.isEmpty {
                    cell.btnNext.isHidden = true
                    cell.btnPrevious.isHidden = true
                }else {
                    if indexDocument == 0 {
                        cell.btnPrevious.isHidden = true
                    }else {
                        cell.btnPrevious.isHidden = false
                    }
                    
                    if indexDocument == dataImgDocument.count - 1 {
                        cell.btnNext.isHidden = true
                    }else {
                        cell.btnNext.isHidden = false
                    }
                }
                
                SNCommon.getImageDirectory(imgView: cell.imgDocument, nameImg: dataImgDocument[indexDocument])
            }else {
                cell.btnNext.isHidden = true
                cell.btnPrevious.isHidden = true
            }
            
            return cell
        }
        // data ImgDocument
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCenterCell") as! TotalCenterCell
            cell.delegate = self
            cell.dataImgDocument = self.dataImgDocument
            return cell
        }
        // data ImgAC
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCenterCell2") as! TotalCenterCell2
            
            cell.lbTitle.text = "AC"
            // chinh sua data
            dataImgAC.remove(at: 1)
            dataImgAC.insert(dataImgEO[0], at: 1)
            cell.arrayData = dataImgAC
            dataCheckConditionAC.remove(at: 1)
            dataCheckConditionAC.insert(dataCheckConditionEO[0], at: 1)
            cell.dataCheckCondition = dataCheckConditionAC
            cell.arrayTitle = ["BDテスト","CIテスト","BIテスト"]
            cell.collectionView.reloadData()
            
            return cell
        }
        // data ImgEO
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCenterCell2") as! TotalCenterCell2
            
            cell.lbTitle.text = "EO"
            /*
            cell.arrayData = dataImgEO
            print(dataCheckConditionEO)
            cell.dataCheckCondition = dataCheckConditionEO
             */
            
            cell.arrayData = dataImgHydro
            
            let dataNil:[Any] = [0,0,0]
            cell.dataCheckCondition = dataNil
            
            cell.arrayTitle = ["CIテスト","BIテスト"]
            cell.collectionView.reloadData()
            
            return cell
        }
        // data ImgHydro
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCenterCell2") as! TotalCenterCell2
            
            cell.lbTitle.text = "過酸化水素"
            
            cell.arrayData = dataImgHydro
            
            let dataNil:[Any] = [0,0,0]
            cell.dataCheckCondition = dataNil
            
            cell.arrayTitle = ["CIテスト","BIテスト"]
            cell.collectionView.reloadData()
            
            return cell
        }
        // data ImgFormalin
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCenterCell2") as! TotalCenterCell2
            
            cell.lbTitle.text = "ホルマリン"
            
            cell.arrayData = dataImgFormalin
            
            let dataNil:[Any] = [0,0,0]
            cell.dataCheckCondition = dataNil
            
            cell.arrayTitle = ["CIテスト","BIテスト"]
            cell.collectionView.reloadData()
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalBottomCell") as! TotalBottomCell
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 1050 * (UIScreen.main.bounds.size.width / 768.0)
        }
        if indexPath.section == 1 {
            return 216 //240
        }
        else if indexPath.section == 6 {
            return 100
        }
        else {
            return 216
        }
    }
    
    func parseDate(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.date(from: str)!
    }
}
