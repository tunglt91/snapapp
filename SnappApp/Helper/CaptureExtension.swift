//
//  CaptureExtension.swift
//  SnappApp
//
//  Created by Tu Ngo on 9/10/18.
//  Copyright © 2018 Tu Ngo. All rights reserved.
//

import UIKit
import AVFoundation

class CaptureExtension: NSObject {
    static let sharedInstance = CaptureExtension()
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var image: UIImage?
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupCapture(cameraView: UIView) {
        self.setupCaptureSession()
        self.setupDevice()
        self.setupInputOutput()
        self.setupPreviewLayer(cameraView: cameraView)
        self.startRunningCaptureSession()
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        currentCamera = backCamera
    }
    
    func setupInputOutput() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.removeInput(captureDeviceInput)
            if captureSession.canAddInput(captureDeviceInput) {
                captureSession.addInput(captureDeviceInput)
            }
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            if captureSession.canAddOutput(photoOutput!) {
                captureSession.addOutput(photoOutput!)
            }
        } catch {
            print(error)
        }
    }
    
    func setupPreviewLayer(cameraView: UIView) {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraView.layer.addSublayer(cameraPreviewLayer!)
        //cameraPreviewLayer?.frame = cameraView.frame
        cameraPreviewLayer?.position = CGPoint(x: cameraView.frame.width / 2, y: cameraView.frame.height / 2)
        cameraPreviewLayer?.bounds = cameraView.frame
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
    }
    
    func photoCaptureDelegate(vc: UIViewController) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: vc as! AVCapturePhotoCaptureDelegate)
    }
    
}

