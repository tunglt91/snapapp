//
//  Target.m
//  MyCameraR30
//
//  Created by Fumio KARASAWA on 2015/10/22.
//  Copyright (c) 2015年 Fumio KARASAWA. All rights reserved.
//

#import "Target.h"

@implementation Target




- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //self.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */





- (void)drawRect:(CGRect)rect
{

    
//fk20180914
    //appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    //    float heightShift = 45;
    float heightShift = 0.0;            //fk20150928
    
    //fk20150810
    //    [[UIColor blueColor] setStroke];
    //    [[UIColor magentaColor] setStroke];       //fk20150909
    [[UIColor whiteColor] setStroke];
    
    
    
    // 線を書くUIBezierPathを生成
    UIBezierPath *line = [UIBezierPath bezierPath];
    // 線の幅を指定
    [line setLineWidth:2];
    
    //fk201508190
    //    CGFloat dashPattern[2] = { 1.0f, 2.0f };
    //    CGFloat dashPattern[2] = { 2.0f, 4.0f };
    CGFloat dashPattern[2] = { 2.0f, 3.0f };
    
    [line setLineDash:dashPattern  count:2 phase:0];
    
    
    //    float origin = 110.0;
    float originX = 110.0;
    float originY = 150.0;
    
    
    // 中心横線（C）始点を設定
    //    [line moveToPoint:CGPointMake(0+origin, 50+origin)];
    //    [line moveToPoint:CGPointMake(0, 50+originY)];                //fk20150910
    //    [line moveToPoint:CGPointMake(25+originX, 50+originY+heightShift)];         //fk20151021
    
    // 中心横線（C）を追加
    //    [line addLineToPoint:CGPointMake(18+origin, 50+origin)];
    //    [line addLineToPoint:CGPointMake(46+origin, 50+origin)];
    //    [line addLineToPoint:CGPointMake(320, 53+originY)];           //fk20150910
    //    [line addLineToPoint:CGPointMake(75+originX, 50+originY+heightShift)];       //fk20151021
    
    // 中心垂直線始点を設定
    //    [line moveToPoint:CGPointMake(50+origin, 0+origin)];          //fk20180816
//    [line moveToPoint:CGPointMake(50+originX, 0)];
    
    // 中心垂直線を追加
    //    [line addLineToPoint:CGPointMake(50+origin, 18+origin)];
//    [line addLineToPoint:CGPointMake(50+originX, 360)];               //fk20180816

    
//fk20180816
    [line moveToPoint:CGPointMake(384, 20)];
    [line addLineToPoint:CGPointMake(384, 1125)];
    
    [line moveToPoint:CGPointMake(84, 20)];
    [line addLineToPoint:CGPointMake(84, 1125)];
    
    [line moveToPoint:CGPointMake(184, 20)];
    [line addLineToPoint:CGPointMake(184, 1125)];
    
    [line moveToPoint:CGPointMake(284, 20)];
    [line addLineToPoint:CGPointMake(284, 1125)];
    
    [line moveToPoint:CGPointMake(484, 20)];
    [line addLineToPoint:CGPointMake(484, 1125)];
    
    [line moveToPoint:CGPointMake(584, 20)];
    [line addLineToPoint:CGPointMake(584, 1125)];
    
    [line moveToPoint:CGPointMake(684, 20)];
    [line addLineToPoint:CGPointMake(684, 1125)];
    
    [line moveToPoint:CGPointMake(784, 20)];
    [line addLineToPoint:CGPointMake(784, 1125)];
    
    [line moveToPoint:CGPointMake(884, 20)];
    [line addLineToPoint:CGPointMake(884, 1125)];
    
//fk20180814
//    [line moveToPoint:CGPointMake(0, 1)];
//    [line addLineToPoint:CGPointMake(320, 1)];
    
    [line moveToPoint:CGPointMake(10, 100)];
    [line addLineToPoint:CGPointMake(860, 100)];
    
    [line moveToPoint:CGPointMake(10, 200)];
    [line addLineToPoint:CGPointMake(860, 200)];
    
    [line moveToPoint:CGPointMake(10, 300)];
    [line addLineToPoint:CGPointMake(860, 300)];
    
    [line moveToPoint:CGPointMake(10, 400)];
    [line addLineToPoint:CGPointMake(860, 400)];
    
    [line moveToPoint:CGPointMake(10, 500)];
    [line addLineToPoint:CGPointMake(860, 500)];
    
    [line moveToPoint:CGPointMake(10, 600)];
    [line addLineToPoint:CGPointMake(860, 600)];
    
    [line moveToPoint:CGPointMake(10, 700)];
    [line addLineToPoint:CGPointMake(860, 700)];
    
    [line moveToPoint:CGPointMake(10, 800)];
    [line addLineToPoint:CGPointMake(860, 800)];
    
    [line moveToPoint:CGPointMake(10, 900)];
    [line addLineToPoint:CGPointMake(860, 900)];
    
    [line moveToPoint:CGPointMake(10, 1000)];
    [line addLineToPoint:CGPointMake(860, 1000)];
    
    [line moveToPoint:CGPointMake(10, 1100)];
    [line addLineToPoint:CGPointMake(860, 1100)];

    
//    [line moveToPoint:CGPointMake(0, 950)];
//    [line addLineToPoint:CGPointMake(320, 950)];
    
    // Dバンド横線始点を設定
    //    [line moveToPoint:CGPointMake(100+origin, 50+origin)];
    //    [line moveToPoint:CGPointMake(25+originX, 145+originY)];      //fk20150910
//    [line moveToPoint:CGPointMake(0, 145+originY+heightShift)];           //fk20180816
    
    // Dバンド横線を追加
    //    [line addLineToPoint:CGPointMake(82+origin, 50+origin)];
    //    [line addLineToPoint:CGPointMake(75+originX, 145+originY)];   //fk20150910
//    [line addLineToPoint:CGPointMake(320, 145+originY+heightShift)];      //fk20180816
    
    // Bバンド横線始点を設定
    //    [line moveToPoint:CGPointMake(50+origin, 100+origin)];
    //    [line moveToPoint:CGPointMake(25+originX, -40+originY+heightShift)];      //fk20151021
    
    // Bバンド横線を追加
    //    [line addLineToPoint:CGPointMake(50+origin, 82+origin)];
    //    [line addLineToPoint:CGPointMake(75+originX, -40+originY+heightShift)];   //fk20151021
    
    
    // Aバンド横線始点を設定
    //    [line moveToPoint:CGPointMake(50+origin, 100+origin)];
    //    [line moveToPoint:CGPointMake(25+originX, -130+originY)];     //fk20150910
//    [line moveToPoint:CGPointMake(0, -130+originY+heightShift)];                  //fk20180816
    
    // Aバンド横線を追加
    //    [line addLineToPoint:CGPointMake(50+origin, 82+origin)];
    //    [line addLineToPoint:CGPointMake(75+originX, -130+originY)];  //fk20150910
//    [line addLineToPoint:CGPointMake(320, -130+originY+heightShift)];             //fk20180816
    
    
    
    
    // 線を描画
    [line stroke];
    
    
    
    
//fk20180914
    // 線を書くUIBezierPathを生成
    UIBezierPath *line2 = [UIBezierPath bezierPath];
    // 線の色を決める
    [[UIColor magentaColor] setStroke];
    // 線の幅を指定
    [line2 setLineWidth:4];
    
//    [line2 setLineDash:dashPattern  count:2 phase:0];
    
    if (self.targetFlag == 0){
    
    [line2 moveToPoint:CGPointMake(85, 200)];
    [line2 addLineToPoint:CGPointMake(685, 200)];
    
    [line2 moveToPoint:CGPointMake(85, 700)];
    [line2 addLineToPoint:CGPointMake(685, 700)];
    
    [line2 moveToPoint:CGPointMake(85, 200)];
    [line2 addLineToPoint:CGPointMake(85, 700)];
    
    [line2 moveToPoint:CGPointMake(685, 200)];
    [line2 addLineToPoint:CGPointMake(685, 700)];
    
    }else{

    [line2 moveToPoint:CGPointMake(320, 200)];
    [line2 addLineToPoint:CGPointMake(450, 200)];
    
    [line2 moveToPoint:CGPointMake(320, 1000)];
    [line2 addLineToPoint:CGPointMake(450, 1000)];
    
    [line2 moveToPoint:CGPointMake(320, 200)];
    [line2 addLineToPoint:CGPointMake(320, 1000)];
    
    [line2 moveToPoint:CGPointMake(450, 200)];
    [line2 addLineToPoint:CGPointMake(450, 1000)];
    
    }
    
  
    
    // 線を描画
    [line2 stroke];
    
    
}

@end
