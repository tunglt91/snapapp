//
//  Acquire.h
//  SampleDOM
//
//  Created by Fumio KARASAWA on 2015/10/14.
//  Copyright (c) 2015年 Fumio KARASAWA. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "AppDelegate.h"

@interface Acquire : UIViewController
//{
//    AppDelegate *appDelegate;
//}

-(CGRect)acquire:(CGImageRef)image touchPoint:(CGPoint)location scale:(float)scaleX;
//fk20150319
-(CGRect)acquireD:(CGImageRef)image touchPoint:(CGPoint)location scale:(float)scaleX maxArea:(int)area maxTolerance:(int)level;


//fk20180726 pixelSize: (Initial) search area size of one edge of square by pixel size
-(CGRect)acquireLW:(CGImageRef)image touchPoint:(CGPoint)location scale:(float)scaleX maxArea:(int)area maxTolerance:(int)level pixelSize:(int)pixelSize;

//fk20180727 refR, G and B: Pixels with less than the value of them are picked up, summed up and averaged.
-(NSMutableArray*)acquireLB:(CGImageRef)image touchPoint:(CGPoint)location pixelSize:(int)pixelSize scale:(float)scaleX refR:(int)refR refG:(int)refG refB:(int)refB;

//fk20180802 ratio: Center part of square limited by the ratio which becomes narrower column rectangle is searched.
-(CGRect)acquireDHW:(CGImageRef)image touchPoint:(CGPoint)location scale:(float)scaleX maxArea:(int)area maxTolerance:(int)level ratioW:(int)ratio;


//fk20180731 DOMの白色測定以外には必要なさそうです
@property(assign, nonatomic) int rectangleAveR;
@property(assign, nonatomic) int rectangleAveG;
@property(assign, nonatomic) int rectangleAveB;

@end
