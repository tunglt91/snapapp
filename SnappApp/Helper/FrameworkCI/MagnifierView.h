//
//  MagnifierView.h
//  deltaE
//
//  Created by Fumio KARASAWA on 2015/01/28.
//  Copyright (c) 2015年 Fumio KARASAWA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MagnifierView : UIView{
    UIView *viewToMagnify;
    CGPoint touchPoint;
}
@property (nonatomic, retain) UIView *viewToMagnify;

@property (nonatomic) CGPoint touchPoint;
//@property (assign) CGPoint touchPoint;            //fk20131121


@end
